package com.roughike.bottombar;

import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Administrator on 2016/10/21 0021.
 */

public class CustomBottomBarNavigationBehavior<V extends View> extends BottomNavigationBehavior<V> {

    private int bottomNavHeight;

    CustomBottomBarNavigationBehavior(int bottomNavHeight, int defaultOffset, boolean tablet) {
        super(bottomNavHeight, defaultOffset, tablet);
        this.bottomNavHeight = bottomNavHeight;

    }


    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, V child, View dependency) {
        updateSnackBar(parent, dependency, child);
        return dependency instanceof Snackbar.SnackbarLayout;

    }

    private void updateSnackBar(CoordinatorLayout parent, View dependency, V child) {
        if (dependency instanceof Snackbar.SnackbarLayout && dependency.getTag() == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dependency.setPadding(dependency.getPaddingLeft(),
                        dependency.getPaddingTop(), dependency.getPaddingRight(), bottomNavHeight
                );
            } else {
                ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) dependency.getLayoutParams();
                layoutParams.bottomMargin = bottomNavHeight;
                child.bringToFront();
                child.getParent().requestLayout();
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    ((View) child.getParent()).invalidate();
                }
            }
            dependency.setTag(this);
        }

    }

}