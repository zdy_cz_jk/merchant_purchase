package com.roughike.bottombar;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Created by Administrator on 2016/10/21 0021.
 */

public class CustomBottomBar extends BottomBar {
    public CustomBottomBar(Context context) {
        super(context);
    }

    public CustomBottomBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
//        KLog.d("CustomBottomBar onLayout");
        super.onLayout(changed, left, top, right, bottom);
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams instanceof CoordinatorLayout.LayoutParams){
            if (!(((CoordinatorLayout.LayoutParams)layoutParams).getBehavior() instanceof CustomBottomBarNavigationBehavior)) {
                CustomBottomBarNavigationBehavior behavior = new CustomBottomBarNavigationBehavior(getHeight(), 0, false);
//            KLog.d("add bottom bar : " + behavior);
                ((CoordinatorLayout.LayoutParams)layoutParams).setBehavior(behavior);
            }
        }

    }

}
