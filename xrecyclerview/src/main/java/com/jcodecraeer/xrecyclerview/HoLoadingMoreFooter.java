package com.jcodecraeer.xrecyclerview;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jcodecraeer.xrecyclerview.progressindicator.AVLoadingIndicatorView;

public class HoLoadingMoreFooter extends LinearLayout {

    private SimpleViewSwitcher progressCon;
    public final static int STATE_LOADING = 0;
    public final static int STATE_COMPLETE = 1;
    public final static int STATE_NOMORE = 2;

    private TextView mText;
    private String loadingHint;
    private String noMoreHint;
    private String loadingDoneHint;

	public HoLoadingMoreFooter(Context context) {
		super(context);
		initView();
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public HoLoadingMoreFooter(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

    public void setLoadingHint(String hint) {
        loadingHint = hint;
    }

    public void setNoMoreHint(String hint) {
        noMoreHint = hint;
    }

    public void setLoadingDoneHint(String hint) {
        loadingDoneHint = hint;
    }

    public void initView(){
	    setBackgroundResource(R.drawable.gray_frame_bg);
	    setOrientation(LinearLayout.VERTICAL);
	    setPadding(0,dp2px(getContext(),10),0,dp2px(getContext(),10));
        setGravity(Gravity.CENTER);
        setLayoutParams(new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mText = new TextView(getContext());

        if(loadingHint == null || loadingHint.equals("")){
            loadingHint = (String)getContext().getText(R.string.listview_loading);
        }
        if(noMoreHint == null || noMoreHint.equals("")){
            noMoreHint = (String)getContext().getText(R.string.str_there_no_data_more);
        }
        if(loadingDoneHint == null || loadingDoneHint.equals("")){
            loadingDoneHint = (String)getContext().getText(R.string.str_there_no_data_more);
        }

        LayoutParams layoutParams = new LayoutParams(600, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins( dp2px(getContext(),10),0,dp2px(getContext(),10),0 );
     //   View tv = LayoutInflater.from(getContext()).inflate(R.layout.todo_footer, null);
        //LinearLayout linearLayout = new LinearLayout(getContext());

        mText.setText(getContext().getString(R.string.listview_loading));
        mText.setLayoutParams(layoutParams);
        addView(mText);
        LayoutParams line1Params = new LayoutParams(dp2px(getContext(),50), dp2px(getContext(),1));
        line1 = new View(getContext());
        line1.setLayoutParams(line1Params);
        line1.setBackgroundColor(0xffe4dad6);
        addView(line1);

        progressCon = new SimpleViewSwitcher(getContext());
        progressCon.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        AVLoadingIndicatorView progressView = new  AVLoadingIndicatorView(this.getContext());
        progressView.setIndicatorColor(0xffB5B5B5);
        progressView.setIndicatorId(ProgressStyle.BallSpinFadeLoader);
        progressCon.setView(progressView);

        addView(progressCon);
        LayoutParams line2Params = new LayoutParams(dp2px(getContext(),50), dp2px(getContext(),1));
        line2 = new View(getContext());
        line2.setLayoutParams(line2Params);
        line2.setBackgroundColor(0xffe4dad6);
        addView(line2);

    }

    public int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    private View line1;
    private View line2;

    public void setProgressStyle(int style) {
        if(style == ProgressStyle.SysProgress){
            progressCon.setView(new ProgressBar(getContext(), null, android.R.attr.progressBarStyle));
        }else{
            AVLoadingIndicatorView progressView = new  AVLoadingIndicatorView(this.getContext());
            progressView.setIndicatorColor(0xffB5B5B5);
            progressView.setIndicatorId(style);
            progressCon.setView(progressView);
        }
    }

    public void  setState(int state) {
        switch(state) {
            case STATE_LOADING:
                line1.setVisibility(GONE);
                line2.setVisibility(GONE);
                progressCon.setVisibility(View.VISIBLE);
                mText.setText(loadingHint);
                this.setVisibility(View.VISIBLE);
                    break;
            case STATE_COMPLETE:
                mText.setText(loadingDoneHint);
                this.setVisibility(View.GONE);
                break;
            case STATE_NOMORE:
                mText.setText(noMoreHint);
                line1.setVisibility(VISIBLE);
                line2.setVisibility(VISIBLE);
                progressCon.setVisibility(View.GONE);
                this.setVisibility(View.VISIBLE);
                break;
        }
    }
}
