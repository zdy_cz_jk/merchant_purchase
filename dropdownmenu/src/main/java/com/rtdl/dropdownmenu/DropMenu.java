package com.rtdl.dropdownmenu;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action2;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public class DropMenu implements IDropMenu {
    private Context context;
    private List<DataTypeFilterBean> data;
    private ArrayList<String> dataPrice;
    private Action2<View, Integer> onItemClick;
    private TagSelectAdapter adapter;
    private boolean autoRefresh = true;

    public boolean isAutoRefresh() {
        return autoRefresh;
    }

    public void setAutoRefresh(boolean autoRefresh) {
        this.autoRefresh = autoRefresh;
    }

    public DropMenu(Context context, List<DataTypeFilterBean> data, Action2<View, Integer>
            onItemClick) {
        this.context = context;
        this.data = data;
        this.onItemClick = onItemClick;
    }

    public DropMenu(Context context, List<DataTypeFilterBean> data, ArrayList<String> price, Action2<View, Integer>
            onItemClick) {
        this.context = context;
        this.data = data;
        this.onItemClick = onItemClick;
        this.dataPrice = price;
    }

    public void setDataPrice(ArrayList<String> dataPrice) {
        this.dataPrice = dataPrice;
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void outsideClick(int position) {
        if (position <= data.size() - 1) {
            changeSelect(data, position);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public View getRootView() {
        RecyclerView mRv = new RecyclerView(context);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        mRv.setLayoutManager(manager);
        if (this.dataPrice == null) {
            adapter = new TagSelectAdapter(R.layout.view_dorp_line_item, data, new ArrayList<String>());
        } else {
            adapter = new TagSelectAdapter(R.layout.view_dorp_line_item, data, this.dataPrice);
        }

        mRv.setAdapter(adapter);
        mRv.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (position <= data.size() - 1) {
                    changeSelect(data, position);
                    onItemClick.call(view, position);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        boolean isAnyChecked = false;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getCheck()) {
                isAnyChecked = true;
            }
        }
        if (!isAnyChecked) {
            data.get(0).setCheck(true);
        }
        //如果没有一项选中,设置第一项为(param:  all 或者 "")选中状态
        if (isAutoRefresh()) {
            onItemClick.call(null, 0);
        }
        return mRv;
    }

    private void changeSelect(List<DataTypeFilterBean> data, int i) {
        if (data.size() < 1) {
            return;
        }
        int find = -1;
        for (int j = 0; j < data.size(); j++) {
            if (data.get(j).getCheck()) {
                find = j;
            }
        }
        if (find != i) {
            if (find >= 0) {
                data.get(find).setCheck(false);
            }
            data.get(i).setCheck(true);
        }
    }
}
