package com.rtdl.dropdownmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action2;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public class NormalMenuHolder extends MenuHolder {
    private Context context;
    private View mRootView;
    protected DropDownMenu mDropDownMenu;
    private Action2<View, Integer> onItemClick;
    private ArrayList<String> mMenuListData;
    private DropMenu dropMenu;
    private ArrayList<String> dataPrice;
    private DropDownMenu.OnMenuCloseListerner onMenuCloseListerner;

    public void setOnMenuCloseListerner(DropDownMenu.OnMenuCloseListerner onMenuCloseListerner) {
        this.onMenuCloseListerner = onMenuCloseListerner;
    }

    public void setMenuListData(ArrayList<String> menuListData) {
        this.mMenuListData = menuListData;
        ArrayList<DataTypeFilterBean> mData = new ArrayList<>();
        ArrayList<DataTypeFilterBean> mdata1 = new ArrayList<>();
        if (this.mMenuListData.size() < 1) {
            ArrayList<DataTypeFilterBean> empty = new ArrayList<>();
            DataTypeFilterBean bean11 = new DataTypeFilterBean("", "",
                    empty, false);
            mdata1.add(bean11);
        } else {
            for (int i = 0; i < this.mMenuListData.size(); i++) {
                ArrayList<DataTypeFilterBean> empty = new ArrayList<>();
                DataTypeFilterBean bean11 = new DataTypeFilterBean(this.mMenuListData.get(i), "",
                        empty, false);
                mdata1.add(bean11);
            }
        }


        DataTypeFilterBean bean1 = new DataTypeFilterBean(this.mMenuListData.get(0), "", mdata1,
                false);
        mData.add(bean1);
        initData(mData);
    }

    public boolean isMenuOpen() {
        if (mDropDownMenu == null) {
            return false;
        }
        return mDropDownMenu.isMenuOpen();
    }

    @Override
    public DropDownMenu getDropDownMenu() {
        return mDropDownMenu;
    }

    @Override
    public void outsideMenuClick(int leftPosition, int rightPosition) {
        if (dropMenu != null) {
            dropMenu.outsideClick(leftPosition);
        }
        if (mMenuListData.size() < 1) {
            return;
        }
        mDropDownMenu.setTabText(0, mMenuListData.get(leftPosition));
        mDropDownMenu.closeMenu();
        onItemClick.call(null, leftPosition);
    }

    public NormalMenuHolder(Context context, Action2<View, Integer> onItemClick) {
        this.context = context;
        this.onItemClick = onItemClick;
        initMenu();
    }

    public NormalMenuHolder(Context context, ArrayList<String> price, Action2<View, Integer> onItemClick) {
        this.context = context;
        this.onItemClick = onItemClick;
        this.dataPrice = price;
        initMenu();
    }

    public void setDataPrice(ArrayList<String> dataPrice) {
        this.dataPrice = dataPrice;
        if (dropMenu != null) {
            dropMenu.setDataPrice(dataPrice);
        }
    }

    public void Tab0Click() {
        mDropDownMenu.TopTapClick();

    }

    public void setDifideView(TextView mDifideView) {
        mDropDownMenu.setDifideView(mDifideView);
    }

    public void setTab0Hide() {
        mDropDownMenu.setTopTapHide();
    }

    private void initMenu() {
        mRootView = LayoutInflater.from(context).inflate(R.layout.view_tag_select, null, false);
        mDropDownMenu = (DropDownMenu) mRootView.findViewById(R.id.dropDownMenu);
        mDropDownMenu.setOnMenuCloseListerner(new DropDownMenu.OnMenuCloseListerner() {
            @Override
            public void onMenuClose() {
                if (NormalMenuHolder.this.onMenuCloseListerner!=null){
                    NormalMenuHolder.this.onMenuCloseListerner.onMenuClose();
                }
            }
        });
    }

    @Override
    public IDropMenu getMenu(int type, List<DataTypeFilterBean> data) {
        if (1 == type) {
            final List<DataTypeFilterBean> menu = data.get(0).getMenu();
            dropMenu = new DropMenu(context, menu, this.dataPrice, new Action2<View, Integer>() {
                @Override
                public void call(View view, Integer integer) {
                    if (menu.size() < 1) {
                        return;
                    }
                    mDropDownMenu.setTabText(menu.get(integer).getText());
                    mDropDownMenu.closeMenu();
                    onItemClick.call(view, integer);
                }
            });
            dropMenu.setAutoRefresh(NormalMenuHolder.this.isAutoRefresh());
            return dropMenu;

        } else {
            return null;
        }

    }

    @Override
    public View getDropMenu() {
        return mRootView;
    }

    public interface OnMenuCloseListerner {
        void onMenuClose();
    }
}
