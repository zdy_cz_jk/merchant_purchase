package com.rtdl.dropdownmenu;

import android.support.v4.util.Pair;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public abstract class MenuHolder {
    private  boolean autoRefresh=true;

    public boolean isAutoRefresh() {
        return autoRefresh;
    }

    public void setAutoRefresh(boolean autoRefresh) {
        this.autoRefresh = autoRefresh;
    }

    public ArrayList<Pair<String, IDropMenu>> menusItems = new ArrayList<>();

    abstract public IDropMenu getMenu(int type, List<DataTypeFilterBean> data);

    abstract public View getDropMenu();

    abstract public DropDownMenu getDropDownMenu();

    abstract public void outsideMenuClick(int leftPosition, int rightPosition);

    public void initData(List<DataTypeFilterBean> listBeans) {
        menusItems.clear();
        for (int i = 0; i < listBeans.size(); i++) {
            IDropMenu menu = null;
            boolean itemHasSub = false;
            for (int j = 0; j < listBeans.get(i).getMenu().size(); j++) {
                if (listBeans.get(i).getMenu().get(j).hasSub()) {
                    itemHasSub = true;
                }
            }
            if (listBeans.get(i).hasSub() && itemHasSub) {
                menu = getMenu(2, listBeans.get(i).getMenu());
            } else {
                menu = getMenu(1, listBeans);
            }
            Pair<String, IDropMenu> pair = new Pair<>(listBeans.get(i).getText(), menu);
            menusItems.add(pair);
        }

        ArrayList<String> mTitles = new ArrayList<>();
        ArrayList<View> views = new ArrayList<>();
        for (int i = 0; i < menusItems.size(); i++) {
            mTitles.add(menusItems.get(i).first);
            views.add(menusItems.get(i).second.getRootView());
        }
        getDropDownMenu().setDropDownMenu(mTitles, views);
    }


}
