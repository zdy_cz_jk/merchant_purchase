package com.rtdl.dropdownmenu;

import android.graphics.Color;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public class TagSelectTwoAdapter extends BaseQuickAdapter<DataTypeFilterBean, BaseViewHolder> {

    public TagSelectTwoAdapter(int layoutResId, @Nullable List<DataTypeFilterBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, DataTypeFilterBean item) {
        helper.setText(R.id.tv_drop_detail, item.getText());
        if (item.getCheck()){
          //  helper.setTextColor(R.id.tv_drop_detail, Color.parseColor("#367bb9"));
            helper.setBackgroundColor(R.id.cons_line_item, Color.WHITE);
        }else {
          //  helper.setTextColor(R.id.tv_drop_detail , Color.parseColor("#666666")) ;
            helper.setBackgroundColor(R.id.cons_line_item,   Color.parseColor("#F9F9F9") );

        }
    }
}
