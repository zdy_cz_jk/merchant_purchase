package com.rtdl.dropdownmenu;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public class TagSelectTwoRightAdapter extends BaseQuickAdapter<DataTypeFilterBean, BaseViewHolder> {
    private List<String> mImgsData;

    public TagSelectTwoRightAdapter(int layoutResId, @Nullable List<DataTypeFilterBean> data) {
        super(layoutResId, data);
    }

    public TagSelectTwoRightAdapter(int layoutResId, @Nullable List<DataTypeFilterBean> data, List<String> imgs) {
        super(layoutResId, data);
        this.mImgsData = imgs;
    }

    public void setImgsData(List<String> mImgsData) {
        this.mImgsData = mImgsData;
    }

    @Override
    protected void convert(BaseViewHolder helper, DataTypeFilterBean item) {
        helper.setText(R.id.tv_drop_detail, item.getText());
        if (item.getCheck()) {
            helper.setBackgroundColor(R.id.cons_item_container, Color.parseColor("#999999"));
        } else {
            helper.setBackgroundColor(R.id.cons_item_container, Color.WHITE);
        }
        String url = "";
        if (TagSelectTwoRightAdapter.this.mImgsData == null ||
                TagSelectTwoRightAdapter.this.mImgsData.size() < 1 ||
                TagSelectTwoRightAdapter.this.mImgsData.size() <= helper.getAdapterPosition()) {
            url = "";
        } else {
            url = TagSelectTwoRightAdapter.this.mImgsData.get(helper.getAdapterPosition());
        }
        Glide.with(helper.itemView.getContext())
                .load(url)
                .placeholder(R.drawable.ic_img_error)
                .error(R.drawable.ic_img_error)
                .centerCrop()
                .thumbnail(0.1f)
                .into((ImageView) helper.getView(R.id.iv_drop_icon));
    }
}
