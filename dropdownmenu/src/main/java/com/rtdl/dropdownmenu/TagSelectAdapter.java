package com.rtdl.dropdownmenu;

import android.graphics.Color;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public class TagSelectAdapter extends BaseQuickAdapter<DataTypeFilterBean, BaseViewHolder> {
    private ArrayList<String> dataPrice;

    public TagSelectAdapter(int layoutResId, @Nullable List<DataTypeFilterBean> data) {
        super(layoutResId, data);
    }

    public TagSelectAdapter(int layoutResId, @Nullable List<DataTypeFilterBean> data, ArrayList<String> price) {
        super(layoutResId, data);
        this.dataPrice = price;
    }

    @Override
    protected void convert(BaseViewHolder helper, DataTypeFilterBean item) {
        helper.setText(R.id.tv_drop_detail, item.getText());
        String price = "0.00";
        try {
            if (this.dataPrice == null || this.dataPrice.size() < 1) {
                price = "0.00";
            } else {
                if (helper.getAdapterPosition() < this.dataPrice.size()) {
                    price = this.dataPrice.get(helper.getAdapterPosition());
                } else {
                    price = "0.00";
                }
                double v = Double.parseDouble(price);
                price = String.format("%.3f", v);
                if (price.contains(".")) {
                    for (int i = 2; i >= 0; i--) {
                        if (price.endsWith("0")) {
                            price = price.substring(0, price.length() - 1);
                        }
                    }
                }
                if (price.endsWith(".")) {
                    price = price.substring(0, price.length() - 1);
                }
            }
        } catch (Exception e) {
        }

        helper.setText(R.id.tv_drop_detail_price, price + "€");
        if (item.getCheck()) {
            helper.setTextColor(R.id.tv_drop_detail, Color.parseColor("#ED3009"));
            helper.setTextColor(R.id.tv_drop_detail_price, Color.parseColor("#ED3009"));
            helper.setBackgroundColor(R.id.cons_drop_item, Color.parseColor("#F2F2F2"));
        } else {
            helper.setTextColor(R.id.tv_drop_detail, Color.parseColor("#111111"));
            helper.setTextColor(R.id.tv_drop_detail_price, Color.parseColor("#111111"));
            helper.setBackgroundColor(R.id.cons_drop_item, Color.WHITE);
        }


    }
}
