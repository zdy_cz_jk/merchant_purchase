package com.rtdl.dropdownmenu;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public class DataTypeFilterBean {
    private String text;
    private String param;
    private List<DataTypeFilterBean> menu;
    @SerializedName("select")
    private Boolean isCheck = false;

    public DataTypeFilterBean(String text, String param, List<DataTypeFilterBean> menu, Boolean
            isCheck) {
        this.text = text;
        this.param = param;
        this.menu = menu;
        this.isCheck = isCheck;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public List<DataTypeFilterBean> getMenu() {
        return menu;
    }

    public void setMenu(List<DataTypeFilterBean> menu) {
        this.menu = menu;
    }

    public Boolean getCheck() {
        return isCheck;
    }

    public void setCheck(Boolean check) {
        isCheck = check;
    }

    public boolean hasSub() {
        return this.getMenu() != null && this.getMenu().size() > 0;
    }
}
