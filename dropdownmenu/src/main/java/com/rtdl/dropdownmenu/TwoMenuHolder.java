package com.rtdl.dropdownmenu;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action2;
import rx.functions.Action3;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public class TwoMenuHolder extends MenuHolder {
    private Context context;
    private DropDownMenu mDropDownMenu;
    private View mRootView;
    Action2<View, Integer> onLeftClick;
    Action3<View, Integer, Integer> onRightClick;
    private ArrayList<String> mLeftMenuListData;
    private ArrayList<ArrayList<String>> mRightMenuListData;
    private DropTwoMenu dropTwoMenu;
    private ArrayList<ArrayList<String>> mRightImgs;
    private OnMenuCloseListerner onMenuCloseListerner;

    public void setOnMenuCloseListerner(OnMenuCloseListerner onMenuCloseListerner) {
        this.onMenuCloseListerner = onMenuCloseListerner;
    }

    @Override
    public DropDownMenu getDropDownMenu() {
        return mDropDownMenu;
    }

    @Override
    public void outsideMenuClick(int leftPosition, int rightPosition) {
        if (dropTwoMenu!=null){
            dropTwoMenu.outSideLeftClick(leftPosition);
            dropTwoMenu.outSideRightClick(leftPosition,rightPosition);
        }
        onLeftClick.call(null,leftPosition);
        String tabText = "";
        if (mLeftMenuListData.size() < 1) {
            return;
        }
        if (mRightMenuListData.get(leftPosition).size() < 1) {
            tabText = mLeftMenuListData.get(leftPosition);
        } else if (mLeftMenuListData.size() >= 1 && TextUtils.isEmpty(mRightMenuListData.get(leftPosition).get(rightPosition))) {
            tabText = mLeftMenuListData.get(leftPosition);
        } else {
            tabText = mRightMenuListData.get(leftPosition).get(rightPosition);
        }
        mDropDownMenu.setTabText(tabText);
        mDropDownMenu.closeMenu();
        onRightClick.call(null,leftPosition,rightPosition);
    }

    public void setMenuData(ArrayList<String> leftData, ArrayList<ArrayList<String>> rightData) {
        this.mLeftMenuListData = leftData;
        this.mRightMenuListData = rightData;
        ArrayList<DataTypeFilterBean> mData = new ArrayList<>();
        ArrayList<DataTypeFilterBean> mdata1 = new ArrayList<>();
        for (int i = 0; i < this.mLeftMenuListData.size(); i++) {
            ArrayList<DataTypeFilterBean> mdata11 = new ArrayList<>();
            ArrayList<DataTypeFilterBean> empty = new ArrayList<>();
            if (this.mRightMenuListData.get(i).size() < 1) {
//                DataTypeFilterBean bean111 = new DataTypeFilterBean("", "",
//                        empty, false);
//                mdata11.add(bean111);
            } else {
                for (int k = 0; k < this.mRightMenuListData.get(i).size(); k++) {
                    DataTypeFilterBean bean111 = new DataTypeFilterBean(this.mRightMenuListData.get
                            (i).get(k), "",
                            empty, false);
                    mdata11.add(bean111);
                }
            }

        if (this.mLeftMenuListData.get(i)==null || this.mLeftMenuListData.size()<1 || this.mLeftMenuListData.size()<i){
            DataTypeFilterBean bean11 = new DataTypeFilterBean("", "",
                    mdata11, false);
            mdata1.add(bean11);
        }else {
            DataTypeFilterBean bean11 = new DataTypeFilterBean(this.mLeftMenuListData.get(i), "",
                    mdata11, false);
            mdata1.add(bean11);
        }

        }
        DataTypeFilterBean bean1 = new DataTypeFilterBean(this.mLeftMenuListData.get(0), "", mdata1,
                false);
        mData.add(bean1);
        initData(mData);
    }

    public TwoMenuHolder(Context context, Action2<View, Integer> onLeftClick, Action3<View,
            Integer, Integer>
            onRightClick) {
        this.context = context;
        this.onLeftClick = onLeftClick;
        this.onRightClick = onRightClick;
        initMenu();
    }
    public TwoMenuHolder(Context context,ArrayList<ArrayList<String>> imgs, Action2<View, Integer> onLeftClick, Action3<View,
            Integer, Integer>
            onRightClick) {
        this.context = context;
        this.onLeftClick = onLeftClick;
        this.onRightClick = onRightClick;
        this.mRightImgs = imgs;
        initMenu();
    }

    private void initMenu() {
        mRootView = LayoutInflater.from(context).inflate(R.layout.view_tag_select, null, false);
        mDropDownMenu = (DropDownMenu) mRootView.findViewById(R.id.dropDownMenu);
        mDropDownMenu.setOnMenuCloseListerner(new DropDownMenu.OnMenuCloseListerner() {
            @Override
            public void onMenuClose() {
                if (TwoMenuHolder.this.onMenuCloseListerner!=null){
                    TwoMenuHolder.this.onMenuCloseListerner.onMenuClose();
                }
            }
        });
    }

    @Override
    public IDropMenu getMenu(int type, final List<DataTypeFilterBean> data) {
        if (type == 2) {
            dropTwoMenu = new DropTwoMenu(context, data,TwoMenuHolder.this.mRightImgs, new Action2<View, Integer>() {
                @Override
                public void call(View view, Integer integer) {
                    onLeftClick.call(view, integer);
                }
            }, new
                    Action3<View, Integer, Integer>() {


                        @Override
                        public void call(View view, Integer integer, Integer integer2) {
                            String tabText = "";
                            if (data.size() < 1) {
                                return;
                            }
                            if (data.get(integer).getMenu().size() < 1) {
                                tabText = data.get(integer).getText();
                            } else if (data.get(integer).getMenu().size() >= 1 && TextUtils.isEmpty(data.get(integer).getMenu().get(integer2).getText())) {
                                tabText = data.get(integer).getText();
                            } else {
                                tabText = data.get(integer).getMenu().get(integer2).getText();
                            }
                            mDropDownMenu.setTabText(tabText);
                            mDropDownMenu.closeMenu();
                            onRightClick.call(view, integer, integer2);
                        }
                    });
            dropTwoMenu.setAutoRefresh(TwoMenuHolder.this.isAutoRefresh());
            return dropTwoMenu;
        } else {
            return null;
        }
    }

    public void TopTapClick() {
        mDropDownMenu.TopTapClick();

    }
    public boolean isMenuOpen() {
        if (mDropDownMenu == null) {
            return false;
        }
        return mDropDownMenu.isMenuOpen();
    }
    public void setDifideView(TextView mDifideView) {
        mDropDownMenu.setDifideView(mDifideView);
    }

    public void setTopTapHide() {
        mDropDownMenu.setTopTapHide();
    }

    @Override
    public View getDropMenu() {
        return mRootView;
    }

    public interface OnMenuCloseListerner {
        void onMenuClose();
    }
}
