package com.rtdl.dropdownmenu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action2;
import rx.functions.Action3;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public class DropTwoMenu implements IDropMenu {
    private Context context;
    private List<DataTypeFilterBean> data;
    Action2<View, Integer> onLeftClick;
    Action3<View, Integer, Integer> onRightClick;
    private TagSelectTwoAdapter leftAdapter;
    private TagSelectTwoRightAdapter rightAdapter;
    private View mrootView;
    private ArrayList<ArrayList<String>> mRightImgs;
    private boolean autoRefresh = true;
    public boolean isAutoRefresh() {
        return autoRefresh;
    }



    public void setAutoRefresh(boolean autoRefresh) {
        this.autoRefresh = autoRefresh;
    }

    public DropTwoMenu(Context context, List<DataTypeFilterBean> data,ArrayList<ArrayList<String>> imgs, Action2<View, Integer>
            onLeftClick, Action3<View, Integer, Integer> onRightClick) {
        this.context = context;
        this.data = data;
        this.onLeftClick = onLeftClick;
        this.onRightClick = onRightClick;
        this.mRightImgs = imgs;
        leftAdapter = new TagSelectTwoAdapter(R.layout.view_dorp_left_item, data); //view_dorp_left_item
        //右边的次级菜单初始化默认加载第一行的数据
        if (DropTwoMenu.this.mRightImgs==null || DropTwoMenu.this.mRightImgs.size()<1){
            rightAdapter = new TagSelectTwoRightAdapter(R.layout.view_dorp_line_right_item,
                    data.get(0).getMenu(),new ArrayList<String>());
        }else {
            rightAdapter = new TagSelectTwoRightAdapter(R.layout.view_dorp_line_right_item,
                    data.get(0).getMenu(),DropTwoMenu.this.mRightImgs.get(0));
        }

    }
    public DropTwoMenu(Context context, List<DataTypeFilterBean> data, Action2<View, Integer>
            onLeftClick, Action3<View, Integer, Integer> onRightClick) {
        this.context = context;
        this.data = data;
        this.onLeftClick = onLeftClick;
        this.onRightClick = onRightClick;
        leftAdapter = new TagSelectTwoAdapter(R.layout.view_dorp_left_item, data); //view_dorp_left_item
        //右边的次级菜单初始化默认加载第一行的数据
        rightAdapter = new TagSelectTwoRightAdapter(R.layout.view_dorp_line_right_item,
                data.get(0).getMenu());
    }

    @Override
    public View getRootView() {
        mrootView = LayoutInflater.from(context).inflate(R.layout.layout_double_recycle,
                null,
                false);
//        ViewGroup.LayoutParams layoutParams = mrootView.getLayoutParams();
//        layoutParams.height=$dp2px(context,200);
//        layoutParams.width=$width(context);
//        mrootView.setLayoutParams(layoutParams);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams($dp2px(context,160), $dp2px(context,400));
        RecyclerView mRvLeft = (RecyclerView) mrootView.findViewById(R.id.rv_drop_left);
        mRvLeft.setLayoutParams(params);
        mRvLeft.setMinimumHeight(80);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        mRvLeft.setLayoutManager(manager);
        mRvLeft.setBackgroundColor(Color.parseColor("#F9F9F9"));
        mRvLeft.setAdapter(leftAdapter);

//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, $dp2px(context, 200f));
//        mRvLeft.setLayoutParams(params);
        mRvLeft.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (position <= data.size() - 1) {
                    rightAdapter.setImgsData(DropTwoMenu.this.mRightImgs.get(position));
                    rightAdapter.setNewData(data.get(position).getMenu());
                    changeSelect(data, position);
                    mrootView.findViewById(R.id.rv_drop_right).setTag(R.id.rv_drop_right, position);
                    onLeftClick.call(view, position);
                    adapter.notifyDataSetChanged();
                    rightAdapter.notifyDataSetChanged();
                }
            }
        });
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, $dp2px(context,400));
        RecyclerView mRvRight = (RecyclerView) mrootView.findViewById(R.id.rv_drop_right);
        LinearLayoutManager managerRight = new LinearLayoutManager(context);
        mRvRight.setLayoutManager(managerRight);
        mRvRight.setLayoutParams(params1);
        mRvRight.setAdapter(rightAdapter);
        mRvRight.setBackgroundColor(Color.WHITE);
        if (rightAdapter.getData().size() < 1) {
            mRvRight.setClickable(false);
            mRvRight.setEnabled(false);
        } else {
            mRvRight.setClickable(true);
            mRvRight.setEnabled(true);
        }
        mRvRight.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                List<DataTypeFilterBean> data = rightAdapter.getData();
                if (position <= data.size() - 1) {
                    changeSelect(data, position);
                    int clickPos = -1;
                    try {
                        clickPos = Integer.parseInt(mrootView.findViewById(R.id.rv_drop_right)
                                .getTag(R.id.rv_drop_right).toString());
                    } catch (Exception e) {
                        clickPos = -1;
                    }

                    if (clickPos != -1) {
                        onRightClick.call(view, clickPos, position);
                    } else {
                        for (int i = 0; i < DropTwoMenu.this.data.size(); i++) {
                            if (data == DropTwoMenu.this.data.get(i).getMenu()) {
                                clickPos = i;
                            }
                        }
                        if (clickPos != -1) {
                            onRightClick.call(view, clickPos, position);
                        }
                    }

                    adapter.notifyDataSetChanged();
                }
            }
        });
        changeSelect(leftAdapter.getData(), 0);
        if (isAutoRefresh()){
            onLeftClick.call(null, 0);
        }
        changeSelect(rightAdapter.getData(), 0);
        if (isAutoRefresh()){
            onRightClick.call(null, 0, 0);
        }
        return mrootView;
    }

    private void changeSelect(List<DataTypeFilterBean> data, int i) {
        if (data.size() < 1) {
            return;
        }
        int find = -1;
        for (int j = 0; j < data.size(); j++) {
            if (data.get(j).getCheck()) {
                find = j;
            }
        }
        if (find != i) {
            if (find >= 0) {
                data.get(find).setCheck(false);
            }
            data.get(i).setCheck(true);
        }
    }

    public void outSideLeftClick(int leftPotion) {
        if (leftPotion <= data.size() - 1) {
            rightAdapter.setImgsData(DropTwoMenu.this.mRightImgs.get(leftPotion));
            rightAdapter.setNewData(data.get(leftPotion).getMenu());
            changeSelect(data, leftPotion);
            leftAdapter.notifyDataSetChanged();
            rightAdapter.notifyDataSetChanged();
        }
    }

    public void outSideRightClick(int leftPotion, int rightPosition) {
        List<DataTypeFilterBean> data = rightAdapter.getData();
        if (rightPosition <= data.size() - 1) {
            changeSelect(data, rightPosition);
            rightAdapter.notifyDataSetChanged();
        }
    }

    /**
     * dp转px
     *
     * @param dp
     * @return
     */
    public static int $dp2px(Context context, float dp) {
        return (int) (dp * context.getResources().getSystem().getDisplayMetrics().density);
    }

    /**
     * 屏幕宽度
     *
     * @return
     */
    public static int $width(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

}
