package com.rtdl.dropdownmenu;

import android.view.View;

/**
 * Created by ${赵东阳} on 2018/9/29.
 */

public interface IDropMenu {
    View getRootView();
}
