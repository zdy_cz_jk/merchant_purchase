package com.huanglejing.shanggou.data

import android.content.Context
import android.graphics.Bitmap
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.bean.CompanyListBean
import com.huanglejing.shanggou.bean.CountryBean
import com.huanglejing.shanggou.bean.GoodsListBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.greendao.CacheGoodsBean
import com.huanglejing.shanggou.greendao.CacheGoodsBeanDao
import com.huanglejing.shanggou.greendao.CacheOrderBean
import com.huanglejing.shanggou.greendao.CacheOrderBeanDao
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.CacheLoader
import com.huanglejing.shanggou.utils.checkEmpty
import java.util.*


/**
 * Created by Administrator on 2017/4/18.
 *
 */
//var TYPE_NOW:String="cf"

/*====================================================================================================================================*/
val Locale_Spanish = Locale("ES", "es", "")
val share = AppContext.get().getSharedPreferences(C.Preference.SAHNGOU_DATA, Context.MODE_PRIVATE)

var beanDao: CacheGoodsBeanDao? = null
    get() = AppContext.get()?.daoSession?.cacheGoodsBeanDao

var orderDao: CacheOrderBeanDao? = null
    get() = AppContext.get()?.daoSession?.cacheOrderBeanDao

var CacheShowStock =false
    get() = share.getBoolean(C.Preference.CACHE_STOCK, false) ?:false

var CacheShowGoods =true
    get() = share.getBoolean(C.Preference.CACHE_GOODS_NO, true) ?:true

var CacheName = ""
    get() = share.getString(C.Preference.CACHE_ACCOUNT_NAME, "") ?: ""
var CacheNameNoZero = ""
    get() = share.getString(C.Preference.CACHE_ACCOUNT_NAME_NOZERO, "") ?: ""
var CachePwd = ""
    get() = share.getString(C.Preference.CACHE_ACCOUNT_PWD, "") ?: ""
var CacheCustomerId = ""
    get() = share.getString(C.Preference.CACHE_ACCOUNT_ID, "")

val COMPANYLIST = arrayListOf<CompanyListBean>()
val GOODSLIST = arrayListOf<GoodsListBean>()
var CacheBitmap: Bitmap? = null

fun getCacheCompanyList(): ArrayList<CompanyListBean>? {
    try {
        if (COMPANYLIST == null) {
            val companyData = CacheLoader.acache.getAsObject(C.Cache.COMPANY_LIST) as? ArrayList<CompanyListBean>
            if (companyData != null) {
                return companyData
            } else {
                return null
            }
        } else {
            return COMPANYLIST
        }

    } catch (e: Exception) {
        KLog.e(e?.message ?: "")
    }
    return null
}

fun setShareBitmap(bitmap: Bitmap?) {
    CacheBitmap = bitmap
}


fun getCacheGoodsList(): ArrayList<GoodsListBean>? {
    try {
        if (GOODSLIST == null) {
            val goodsData = CacheLoader.acache.getAsObject(C.Cache.GOODS_LIST) as? ArrayList<GoodsListBean>
            if (goodsData != null) {
                return goodsData
            } else {
                return null
            }
        } else {
            return GOODSLIST
        }

    } catch (e: Exception) {
        KLog.e(e?.message ?: "")
    }
    return null
}

fun getCacheGoodsImgs(): ArrayList<ArrayList<String>>? {
    val mDataImgs = ArrayList<ArrayList<String>>()
    try {
        mDataImgs?.clear()
        getCacheGoodsList()?.forEach {
            var list: ArrayList<String>? = null
            if (it.level == 0) {
                list = arrayListOf()
                mDataImgs?.add(list)
            } else if (it.level == 1) {
                mDataImgs?.getOrNull(mDataImgs?.size?.minus(1) ?: 0)?.add(it?.url ?: "")
            }
        }
        return mDataImgs
    } catch (e: Exception) {
        KLog.e(e?.message ?: "")
    }
    return ArrayList<ArrayList<String>>()
}

fun getAllPrice(): ArrayList<String>? {
    val priceData = arrayListOf<String>()
    try {


        if (getCacheGoods() != null && getCacheGoods()?.size >= 1) {
            getCacheCompanyList()?.forEach {
                var allPrice = 0.00
                getCacheGoods()?.forEachIndexed { index, cacheOrderBean ->
                    if (it.companyId?.equals(cacheOrderBean.companyId ?: "") ?: false) {
                        allPrice = allPrice?.plus(cacheOrderBean?.totalPrice?.toDoubleOrNull()
                                ?: 0.00)
                    }
                }
                priceData?.add(allPrice?.toString())
            }

        } else {
            getCacheCompanyList()?.forEach {
                priceData?.add("0.00")
            }
        }
        return priceData
    } catch (e: Exception) {
        KLog.e(e?.message ?: "")
    }
    return priceData
}


fun getCacheGoods(): List<CacheGoodsBean> {
    val loadAll = beanDao?.loadAll()?.filter {
        it?.customerId?.equals(CacheCustomerId) ?: false
    } ?: emptyList()
    return loadAll
}

fun getCacheOrders(): List<CacheOrderBean> {
    val loadAll = orderDao?.loadAll()?.filter {
        it?.customerId?.equals(CacheCustomerId) ?: false
    } ?: emptyList()
    return loadAll
}

val CountryList
    get() = arrayListOf<CountryBean>(CountryBean(AppContext.get().resources.getString(R.string.str_spain), "34"),
            CountryBean(AppContext.get().resources.getString(R.string.str_italy), "39"),
            CountryBean(AppContext.get().resources.getString(R.string.str_portugal), "351"),
            CountryBean(AppContext.get().resources.getString(R.string.str_france), "33"),
            CountryBean(AppContext.get().resources.getString(R.string.str_slovenia), "38"),
            CountryBean(AppContext.get().resources.getString(R.string.str_croatia), "385"),
            CountryBean(AppContext.get().resources.getString(R.string.str_greece), "30"),
            CountryBean(AppContext.get().resources.getString(R.string.str_hungary), "36"),
            CountryBean(AppContext.get().resources.getString(R.string.str_poland), "48"),
            CountryBean(AppContext.get().resources.getString(R.string.str_chile), "56"),
            CountryBean(AppContext.get().resources.getString(R.string.str_slovakia), "421"))

var mTabRes = intArrayOf()
    get() = intArrayOf(R.drawable.ic_main_tab_home, R.drawable.ic_main_tab_img,
            R.drawable.ic_main_tab_current, R.drawable.ic_main_tab_manage, R.drawable.ic_main_tab_mine)

var mTabResPressed = intArrayOf()
    get() = intArrayOf(R.drawable.ic_main_tab_home_press,
            R.drawable.ic_main_tab_img_press, R.drawable.ic_main_tab_current_press,
            R.drawable.ic_main_tab_manage_press, R.drawable.ic_main_tab_mine_press)
var mTabTitle = arrayOf<String>()
    get() = arrayOf(AppContext.get().resources.getString(R.string.str_home),
            AppContext.get().resources.getString(R.string.str_e_catalog),
            AppContext.get().resources.getString(R.string.str_current_order),
            AppContext.get().resources.getString(R.string.str_order),
            AppContext.get().resources.getString(R.string.str_mine))

/**
 * 获取Tab 显示的内容
 * @param context
 * @param position
 * @return
 */
fun getTabView(context: Context, position: Int): View {
    val view = LayoutInflater.from(context).inflate(R.layout.home_tab_content, null)
    val tabIcon = view.findViewById<ImageView>(R.id.tab_content_image)
    tabIcon.setImageResource(mTabRes[position])
    val tabText = view.findViewById<TextView>(R.id.tab_content_text)
    tabText.setSingleLine(true)
    tabText.ellipsize= TextUtils.TruncateAt.END
    tabText.text = mTabTitle[position]
    return view
}

/*=================================================rent===================================================================================*/


//val LicenceStatus
//    get() = listOf(Licence.getOrNull(0).toDefault("未认证") to SnackBarUtils.Level.BASIC,
//            Licence.getOrNull(1).toDefault("已认证") to SnackBarUtils.Level.CONFIRM,
//            Licence.getOrNull(2).toDefault("认证中") to SnackBarUtils.Level.INFO,
//            Licence.getOrNull(3).toDefault("认证失败") to SnackBarUtils.Level.ERROR)
//val IsLicense
//    get() = listOf("草稿" to SnackBarUtils.Level.INFO,
//            Licence.getOrNull(1).toDefault("已认证") to SnackBarUtils.Level.CONFIRM,
//            Licence.getOrNull(2).toDefault("认证中") to SnackBarUtils.Level.ERROR_V2,
//            Licence.getOrNull(3).toDefault("认证失败") to SnackBarUtils.Level.BASIC)