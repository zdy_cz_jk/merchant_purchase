package com.huanglejing.shanggou.fragment

import android.os.Bundle
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.bean.ShareBean
import com.huanglejing.shanggou.common.BaseBottomSheetFragment
import com.huanglejing.shanggou.utils.FileUtil
import com.huanglejing.shanggou.utils.UIUtils
import com.huanglejing.shanggou.utils.inflate
import com.huanglejing.shanggou.utils.oneKeyShare
import com.umeng.socialize.bean.SHARE_MEDIA
import kotlinx.android.synthetic.main.fragment_share.view.*
import android.graphics.drawable.Drawable
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.provider.MediaStore.Images.Media.getBitmap
import android.graphics.drawable.BitmapDrawable








class ShareFragmentBar : BaseBottomSheetFragment() {


    override val mRootView by lazy {
        activity?.inflate(R.layout.fragment_share)
                ?: error("activity in PayForTaskFragment is null")
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun initWidget() {
        val bean= ShareBean(activity)
        mRootView?.bt_share_cancle?.setOnClickListener {
            dismiss()
        }
        mRootView?.ll_share_qq_zone?.setOnClickListener {
            activity?.oneKeyShare(bean,SHARE_MEDIA.QZONE)
        }
        mRootView?.ll_share_qq?.setOnClickListener {
            activity?.oneKeyShare(bean,SHARE_MEDIA.QQ)
        }
        mRootView?.ll_share_friend_circle?.setOnClickListener {
            activity?.oneKeyShare(bean,SHARE_MEDIA.WEIXIN_CIRCLE)
        }
        mRootView?.ll_share_wechat?.setOnClickListener {
            activity?.oneKeyShare(bean,SHARE_MEDIA.WEIXIN)
        }
    }

    companion object {
        fun newInstance(): ShareFragmentBar {
            val fragment = ShareFragmentBar()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
