package com.huanglejing.shanggou.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.huanglejing.shanggou.presenterimpl.MyFragmentPresenterImpl
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.common.AppBaseFragment
import com.huanglejing.shanggou.interfaces.MyFragmentContract.*
import kotlinx.android.synthetic.main.fragment_my_stub.*

/**
 * Created by Administrator on 2016/8/5 0005.
 */
class MyFragment : AppBaseFragment<MyFragmentPresenter<MyFragmentView>, MyFragmentView>(), MyFragmentView {
    init {
        containerId = R.id.fl_head_container
    }

    var myText: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun createPresenter(): Any? = MyFragmentPresenterImpl()

    override fun getLayoutId(): Int = R.layout.fragment_my_stub

    override fun initWidget(rootView: View?) {

        tv_stub.text = myText ?: ""
        if (myText?.contains(resources.getString(R.string.str_contact)) ?: false) {
            tv_stub.setBackgroundColor(Color.WHITE)
        }
    }

    override fun onResume() {
        super.onResume()
        setIvExitVisible(false)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser){
            setIvExitVisible(false)
        }
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            setAPPTitle(myText)
        }
    }

    override fun initData() {
    }

    companion object {
        @JvmStatic
        fun newInstance(text: String): MyFragment {
            return MyFragment().apply {
                myText = text
            }
        }
    }

}