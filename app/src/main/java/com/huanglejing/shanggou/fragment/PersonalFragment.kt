package com.huanglejing.shanggou.fragment

import android.view.View
import android.widget.TextView
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.activity.*
import com.huanglejing.shanggou.common.AppBaseFragment
import com.huanglejing.shanggou.data.CacheCustomerId
import com.huanglejing.shanggou.data.CacheName
import com.huanglejing.shanggou.data.CacheNameNoZero
import com.huanglejing.shanggou.interfaces.PersonalContract.PersonalPresenter
import com.huanglejing.shanggou.interfaces.PersonalContract.PersonalView
import com.huanglejing.shanggou.presenterimpl.PersonalPresenterImpl
import com.huanglejing.shanggou.utils.UIHelper
import com.huanglejing.shanggou.utils.checkEmpty
import kotlinx.android.synthetic.main.layout_personal_fragment.*
import kotlin.properties.Delegates

/**
 * Created by Administrator on 2017/8/21.
 */
class PersonalFragment : AppBaseFragment<PersonalPresenter<PersonalView>, PersonalView>(), PersonalView {
    init {
        containerId = R.id.fl_head_container
    }

    private var headName: TextView by Delegates.notNull()
    override fun createPresenter(): Any? = PersonalPresenterImpl()

    override fun getLayoutId(): Int = R.layout.layout_personal_fragment


    override fun initWidget(rootView: View?) {
        setHasOptionsMenu(true)
        headName = rootView?.findViewById<TextView>(R.id.tv_head_name) as TextView
        if (CacheCustomerId.checkEmpty()) {
            headName.text =  resources.getString(R.string.str_not_login)
            iv_avatar_icon?.setImageResource(R.drawable.login_head_img)
        } else {
            headName.text =  "+${CacheNameNoZero ?:resources.getString(R.string.str_not_login)}"
            iv_avatar_icon?.setImageResource(R.drawable.login_head_img)
        }

        rl_avatar_show.setOnClickListener {
            if (CacheCustomerId.checkEmpty()) {
                UIHelper.startActivity(LoginActivity::class.java)
                getActivity()?.finish()
            } else {
                UIHelper.startActivity(PersonalCenterActivity::class.java)
            }
        }
        ll_mine_shiping_address.setOnClickListener {
            if (!CacheCustomerId.checkEmpty()) {
                UIHelper.startActivity(MineShippingAddressActivity::class.java)
            } else {
                UIHelper.startActivity(LoginActivity::class.java)
                getActivity()?.finish()
            }
        }
        ll_setting.setOnClickListener {
            if (!CacheCustomerId.checkEmpty()) {
                UIHelper.startActivity(SettingActivity::class.java)
            } else {
                UIHelper.startActivity(LoginActivity::class.java)
                getActivity()?.finish()
            }
        }
/*        ll_feed_back.setOnClickListener {
            if (!CacheCustomerId.checkEmpty()) {
                UIHelper.startActivity(FeedBackActivity::class.java)
            } else {
                UIHelper.startActivity(LoginActivity::class.java)
                getActivity()?.finish()
            }
        }*/
        ll_mine_sellers.setOnClickListener {
            if (!CacheCustomerId.checkEmpty()) {
                UIHelper.startActivity(MineSellersActivity::class.java)
            } else {
                UIHelper.startActivity(LoginActivity::class.java)
                getActivity()?.finish()
            }
        }
        ll_feed_about.setOnClickListener {
            if (!CacheCustomerId.checkEmpty()) {
                UIHelper.startActivity(AboutActivity::class.java)
            } else {
                UIHelper.startActivity(LoginActivity::class.java)
                getActivity()?.finish()
            }
        }
        ll_personal_share.setOnClickListener {
            if (!CacheCustomerId.checkEmpty()) {
                ShareFragmentBar.newInstance().show(getActivity()?.supportFragmentManager, "ShareFragmentBar");
            } else {
                UIHelper.startActivity(LoginActivity::class.java)
                getActivity()?.finish()
            }

        }


    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser) {
            setTitleVisible(false)
            setIvExitVisible(false)
        }
        super.setUserVisibleHint(isVisibleToUser)

    }

    override fun onResume() {
        super.onResume()
    }

    override fun <T> showContent(data: T?) {

    }

    override fun showError(e: Throwable?) {
        KLog.e(e?.message)
    }

    override fun initData() {
    }

    companion object {
        @JvmStatic
        fun newInstance(text: String): PersonalFragment {
            return PersonalFragment()
        }
    }

}