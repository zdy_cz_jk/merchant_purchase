package com.huanglejing.shanggou.fragment

import android.graphics.Color
import android.os.Bundle
import android.support.v4.util.ArrayMap
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.activity.AddSellerActivity
import com.huanglejing.shanggou.adapter.HomeAdapter
import com.huanglejing.shanggou.bean.CompanyListBean
import com.huanglejing.shanggou.common.AppBaseFragment
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.data.COMPANYLIST
import com.huanglejing.shanggou.data.CacheCustomerId
import com.huanglejing.shanggou.data.GOODSLIST
import com.huanglejing.shanggou.interfaces.HomeFragmentContract.HomeFragmentPresenter
import com.huanglejing.shanggou.interfaces.HomeFragmentContract.HomeFragmentView
import com.huanglejing.shanggou.presenterimpl.HomePresenterImpl
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.rx.event.CompanyIdEvent
import com.huanglejing.shanggou.rx.event.CompanyRefreshEvent
import com.huanglejing.shanggou.rx.rxbus.RxBus
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.CacheLoader
import com.huanglejing.shanggou.utils.UIHelper
import com.huanglejing.shanggou.utils.addTo
import com.jcodecraeer.xrecyclerview.ProgressStyle
import com.jcodecraeer.xrecyclerview.XRecyclerView
import rx.Observable

/**
 * Created by Administrator on 2018/9/18.
 */

class HomeFragment : AppBaseFragment<HomeFragmentPresenter<HomeFragmentView>, HomeFragmentView>(), HomeFragmentView, XRecyclerView.LoadingListener {
    private var mAdapter: HomeAdapter? = null;
    val XRview by lazy {
        rootViewWeakRef?.get()?.findViewById<XRecyclerView>(R.id.xr_home_list)
    }
    private val mEmptyView by lazy {
        rootViewWeakRef?.get()?.findViewById<View>(R.id.ll_xr_empty)
    }
    private val mErrorView by lazy {
        rootViewWeakRef?.get()?.findViewById<View>(R.id.ll_xr_error)
    }

    override fun createPresenter(): Any? = HomePresenterImpl()

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun initWidget(rootView: View?) {
        //   initLocalData()
        //   setAPPTitle("我的商家")
        getActivity()?.findViewById<TextView>(R.id.tv_login_language)?.setOnClickListener {
            UIHelper.startActivity(AddSellerActivity::class.java)
        }

        XRview?.setLoadingListener(this@HomeFragment)
        XRview?.setLoadingMoreProgressStyle(ProgressStyle.BallRotate)
        XRview?.setRefreshProgressStyle(ProgressStyle.BallRotate)
        val layoutManager = LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        XRview?.setLayoutManager(layoutManager);
        mAdapter = HomeAdapter(getActivity(), COMPANYLIST)
        XRview?.adapter = mAdapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RxBus.get().toObservable(CompanyRefreshEvent::class.java)
                .doOnNext {
                    mBasePresenter?.getCompanyList()
                }.subscribe(SimpleSubscriber()).addTo(rxManager)
    }
    override fun <T> showContent(data: T?) {
        try {
            val mNetData = data as? ArrayList<CompanyListBean>
            if (COMPANYLIST != null && mNetData != null) {
                COMPANYLIST?.clear()
                COMPANYLIST?.addAll(mNetData)
                CacheLoader.acache.put(C.Cache.COMPANY_LIST, mNetData)
                COMPANYLIST?.clear()
                COMPANYLIST?.addAll(mNetData)
                if (COMPANYLIST?.size ?: 0 < 1 && mEmptyView != null) {
                    XRview?.emptyView = mEmptyView
                }
                mAdapter?.setData(COMPANYLIST ?: arrayListOf())
            }
            loadComPlete()
        } catch (e: Exception) {
            KLog.e(e?.message)
        }

    }

    override fun showError(e: Throwable?) {
        KLog.e(e?.message)
        RxBus.get().postEvent(CompanyIdEvent("", ""))
        COMPANYLIST?.clear()
        if (mErrorView != null) {
            XRview?.emptyView = mErrorView
            mErrorView?.setOnClickListener {
                mBasePresenter?.getCompanyList()
            }
        }


        mAdapter?.setData(arrayListOf())
        loadComPlete()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser) {
            setIvExitVisible(false)
            getActivity()?.findViewById<TextView>(R.id.tv_title)?.setText(resources.getString(R.string.str_home))
        }
        super.setUserVisibleHint(isVisibleToUser)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        setIvExitVisible(false)
        setIvExitShow()
        if (getActivity()?.findViewById<ViewPager>(R.id.main_container)?.currentItem == 0) {
            getActivity()?.findViewById<TextView>(R.id.tv_title)?.setText(resources.getString(R.string.str_home))
            getActivity()?.findViewById<TextView>(R.id.tv_login_language)?.visibility = View.VISIBLE

            getActivity()?.findViewById<TextView>(R.id.tv_login_language)?.setText(resources.getString(R.string.str_add_shops))
        }

    }

    override fun initData() {
    }

    override fun getRequestParams(): Observable<Map<String, Any>> {
        val map = ArrayMap<String, String>().apply {
            put("customerId", CacheCustomerId ?: "")
        }
        return Observable.just(map)
    }

    companion object {
        @JvmStatic
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onRefresh() {
        mBasePresenter?.getCompanyList()

    }

    override fun onLoadMore() {

    }

    fun loadComPlete() {
        XRview?.loadMoreComplete()
        XRview?.refreshComplete()
        XRview?.setNoMore(true)
    }
}
