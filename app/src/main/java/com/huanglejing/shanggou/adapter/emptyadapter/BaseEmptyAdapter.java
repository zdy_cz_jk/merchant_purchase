package com.huanglejing.shanggou.adapter.emptyadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.huanglejing.shanggou.R;

import java.util.List;

/**
 * Created by ${赵东阳} on 2018/10/24.
 */

public abstract class BaseEmptyAdapter<T, M> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected static final int TYPE_EMPTY = 10243;
    protected static final int TYPE_ERROR = 10244;
    protected Context context;
    private boolean isError = false;

    public abstract List<M> getData();
    public abstract RecyclerView.ViewHolder getViewHolder(ViewGroup parent);

    public void setError(boolean error) {
        isError = error;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (TYPE_EMPTY == viewType) {
            View mRootView = LayoutInflater.from(context).inflate(R.layout
                    .layout_empty_xrecyclerview, parent, false);
            return new EmptyViewHolder(mRootView);
        } else if (TYPE_ERROR == viewType) {
            View mRootView = LayoutInflater.from(context).inflate(R.layout
                    .layout_error_xrecyclerview, parent, false);
            return new EmptyViewHolder(mRootView);
        } else {
            return getViewHolder(parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (TYPE_EMPTY == getItemViewType(position) || TYPE_ERROR == getItemViewType(position)) {
            return;
        }
    }


    @Override
    public int getItemCount() {
        if (getData() == null) {
            return 0;
        } else if (getData() != null && getData().size() == 0) {
            return 1;
        } else {
            return getData().size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!isError && getData() != null && getData().size() == 0) {
            return TYPE_EMPTY;
        } else if (isError && getData() != null && getData().size() == 0) {
            return TYPE_ERROR;
        } else {
            return super.getItemViewType(position);
        }

    }

}
