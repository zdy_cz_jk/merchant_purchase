package com.huanglejing.shanggou.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.bean.NewComeProBean;
import com.huanglejing.shanggou.common.AppContext;
import com.huanglejing.shanggou.data.DictKt;
import com.huanglejing.shanggou.greendao.CacheGoodsBean;
import com.huanglejing.shanggou.utils.ExtentionFuncsKt;
import com.huanglejing.shanggou.utils.ToastUtils;
import com.huanglejing.shanggou.view.FloatingTextView.RxHeartLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductImgsViewHolder> {
    private Context context;
    private ArrayList<NewComeProBean> mData;
    private List<CacheGoodsBean> mCacheData;
    private OnItemClickListener onItemClickListener;
    private CacheOrderBeanListener CacheOrderBeanListener;

    public ProductListAdapter(Context context, ArrayList<NewComeProBean> mData) {
        this.context = context;
        this.mData = mData;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setCacheOrderBeanListener(CacheOrderBeanListener CacheOrderBeanListener) {
        this.CacheOrderBeanListener = CacheOrderBeanListener;
    }

    public void setData(ArrayList<NewComeProBean> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    public void setCacheData(List<CacheGoodsBean> mCacheData) {
        this.mCacheData = mCacheData;
    }

    @NonNull
    @Override
    public ProductImgsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_product_list_item, parent, false);
        return new ProductImgsViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductImgsViewHolder holder, final int position) {
        int i1 = 0;
        if (mCacheData != null && mCacheData.size() > 0) {
            for (int i = 0; i < mCacheData.size(); i++) {
                try {
                    if (mData.get(position).getId().equals(mCacheData.get(i).getGoodNO())) {
                        i1 = Integer.parseInt(mCacheData.get(i).getNum());
                    }
                } catch (Exception e) {
                }
            }
        }
        if (i1 == 0) {
            holder.mTvNum.setVisibility(View.GONE);
            holder.mIvMinus.setVisibility(View.GONE);
        } else {
            holder.mTvNum.setVisibility(View.VISIBLE);
            holder.mIvMinus.setVisibility(View.VISIBLE);
        }
        double zk = 0;
        double price = 0;
        int cacheNum = 0;
        try {
            cacheNum=i1*(Integer.parseInt(mData.get(position).getBagCount()));
            zk = Double.parseDouble(mData.get(position).getZk()) / 100;
            Log.d("zk","zk    "+zk);
        } catch (Exception e) {
            zk = 0;
            Log.d("zk","zk  Exception  "+zk);
        }
        try {
            price = Double.parseDouble(mData.get(position).getPrice());
        } catch (Exception e) {
            price = 0;
        }
        holder.mTvOldPrice.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG  );
        if (zk > 0) {
            price = price * (1-zk);
            holder.mTvPrice.setText(ExtentionFuncsKt.formatNum(price) + "€");
            holder.mTvOldPrice.setVisibility(View.VISIBLE);
           // holder.mTvPrice.setTextColor(Color.parseColor("#ED3009"));
        } else {
            holder.mTvPrice.setText(ExtentionFuncsKt.formatNum(price) + "€");
           // holder.mTvPrice.setTextColor(Color.parseColor("#4EABE4"));
            holder.mTvOldPrice.setVisibility(View.INVISIBLE);
        }

        holder.mTvTitle.setText(mData.get(position).getName());
        holder.mTvStock.setText("库存："+mData.get(position).getCount());
        holder.mTvGoodsNum.setText("货号："+mData.get(position).getNo());
        if (DictKt.getCacheShowStock()){
            holder.mTvStock.setVisibility(View.VISIBLE);
        }else {
            holder.mTvStock.setVisibility(View.GONE);
        }
        if (DictKt.getCacheShowGoods()){
            holder.mTvGoodsNum.setVisibility(View.VISIBLE);
        }else {
            holder.mTvGoodsNum.setVisibility(View.GONE);
        }
        holder.mTvContent.setText("包装："+mData.get(position).getBagCount()+"u/"+mData.get(position).getBoxCount()+"c");
        if (zk>0){
            holder.mTvZheKou.setText("-"+(zk*100)+"%");
        }else {
            holder.mTvZheKou.setVisibility(View.GONE);
        }


        holder.mTvOldPrice.setText(mData.get(position).getPrice());
        holder.mTvNum.setText("" +cacheNum);
        holder.mIvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mRxHeart.post(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            String num = holder.mTvNum.getText().toString();
                            if (TextUtils.isEmpty(num)) {
                                num = "0";
                            }
                            int i = Integer.parseInt(num);
                            int i1 = Integer.parseInt(mData.get(position).getCount());
                            if (i == i1) {
                                ToastUtils.toastLong(AppContext.get().getResources().getString(R.string.str_maxmun_to_buy) + i1);
                                return;
                            }
                            int numAdd = i + Integer.parseInt(mData.get(position).getBagCount());
                            int rgb = Color.rgb(255, 0, 0);
                            holder.mTvNum.setVisibility(View.VISIBLE);
                            holder.mIvMinus.setVisibility(View.VISIBLE);
                            holder.mTvNum.setText(numAdd + "");
                            holder.mRxHeart.addHeartAndText(rgb, "+" + Integer.parseInt(mData.get(position).getBagCount()));
                            if (CacheOrderBeanListener != null) {
                                CacheOrderBeanListener.onCacheOrderBeanAdd(position,numAdd, mData.get(position));
                            }
                        } catch (Exception e) {
                        }

                    }
                });
            }
        });
        holder.mIvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mRxHeartMinus.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String num = holder.mTvNum.getText().toString();
                            if (TextUtils.isEmpty(num)) {
                                num = "0";
                            }
                            int i = Integer.parseInt(num);
                            if (i == 0) {
                                return;
                            }

                            int numMinus = i - Integer.parseInt(mData.get(position).getBagCount());
                            int rgb = Color.rgb(255, 0, 0);
                            if (numMinus>=Integer.parseInt(mData.get(position).getBagCount())){
                                holder.mRxHeartMinus.addHeartAndText(rgb, "-" + Integer.parseInt(mData.get(position).getBagCount()));
                            }else if (numMinus>0 && numMinus<Integer.parseInt(mData.get(position).getBagCount())){
                                holder.mRxHeartMinus.addHeartAndText(rgb, "-" + numMinus);
                            }else if (numMinus==0 ){
                                holder.mRxHeartMinus.addHeartAndText(rgb, "-" + Integer.parseInt(mData.get(position).getBagCount()));
                            }
                            /*else {
                                holder.mRxHeartMinus.addHeartAndText(rgb, "" );
                            }*/
                            if (numMinus > 0) {
                                holder.mTvNum.setText(numMinus + "");
                                holder.mTvNum.setVisibility(View.VISIBLE);
                                holder.mIvMinus.setVisibility(View.VISIBLE);

                            } else if (numMinus == 0) {
                                holder.mTvNum.setText(numMinus + "");
                                holder.mTvNum.setVisibility(View.GONE);
                                holder.mIvMinus.setVisibility(View.GONE);
                            }else  if (numMinus<0){
                                return;
                            }

                            if (CacheOrderBeanListener != null) {
                                CacheOrderBeanListener.onCacheOrderBeanMinius(position,numMinus, mData.get(position));
                            }
                        } catch (Exception e) {
                        }

                    }
                });
            }
        });
        Glide.with(context).load(mData.get(position).getUrl())
                .placeholder(R.drawable.ic_img_error)
                .error(R.drawable.ic_img_error)
                .centerCrop()
                .thumbnail(0.1f)
                .into(holder.mIvIcon);
        holder.mIvIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    class ProductImgsViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvTitle;
        private TextView mTvContent;
        private TextView mTvPrice;
        private TextView mTvOldPrice;
        private TextView mTvNum;
        private TextView mTvStock;
        private TextView mTvGoodsNum;
        private TextView mTvZheKou;
        private ImageView mIvAdd;
        private ImageView mIvMinus;
        private ImageView mIvIcon;
        private RxHeartLayout mRxHeart;
        private RxHeartLayout mRxHeartMinus;
        private RelativeLayout mRlContainer;

        public ProductImgsViewHolder(View itemView) {
            super(itemView);
            mRlContainer = itemView.findViewById(R.id.rl_product_list_container);
            mIvIcon = itemView.findViewById(R.id.iv_product_list_icon);
            mTvTitle = itemView.findViewById(R.id.tv_product_list_title);
            mTvContent = itemView.findViewById(R.id.tv_product_list_unit);
            mTvPrice = itemView.findViewById(R.id.tv_product_list_Price);
            mTvNum = itemView.findViewById(R.id.tv_product_list_num);
            mTvStock = itemView.findViewById(R.id.tv_product_list_stock);
            mTvGoodsNum = itemView.findViewById(R.id.tv_product_list_goods_num);
            mTvZheKou = itemView.findViewById(R.id.tv_product_list_zhe_kou);
            mTvOldPrice = itemView.findViewById(R.id.tv_product_list_old_price);
            mIvAdd = itemView.findViewById(R.id.iv_product_list_add);
            mIvMinus = itemView.findViewById(R.id.iv_product_list_minus);
            mRxHeart = itemView.findViewById(R.id.heart_layout);
            mRxHeartMinus = itemView.findViewById(R.id.heart_layout_minus);
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View v, int position);
    }

    public interface CacheOrderBeanListener {
        public void onCacheOrderBeanAdd(int position, int selectNum, NewComeProBean bean);

        public void onCacheOrderBeanMinius(int position, int selectNum, NewComeProBean bean);
    }
}

