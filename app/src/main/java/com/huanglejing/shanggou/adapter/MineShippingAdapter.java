package com.huanglejing.shanggou.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.activity.AddAddresssActivity;
import com.huanglejing.shanggou.bean.AddressListBean;
import com.huanglejing.shanggou.common.AppContext;
import com.huanglejing.shanggou.interfaces.IonSlidingViewClickListener;
import com.huanglejing.shanggou.utils.DisplayUtils;
import com.huanglejing.shanggou.utils.UIHelper;
import com.huanglejing.shanggou.view.LeftSlideView;

import java.util.ArrayList;

public class MineShippingAdapter extends RecyclerView.Adapter<MineShippingAdapter.ProductImgsViewHolder> implements LeftSlideView.IonSlidingButtonListener {
    private Context context;
    private ArrayList<AddressListBean> mData;
    private IonSlidingViewClickListener mIDeleteBtnClickListener;
    private LeftSlideView mMenu = null;


    public MineShippingAdapter(Context context, ArrayList<AddressListBean> mData) {
        this.context = context;
        this.mData = mData;
        mIDeleteBtnClickListener = (IonSlidingViewClickListener) context;
    }

    public void setData(ArrayList<AddressListBean> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductImgsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_shipping_list_item, parent, false);
        return new ProductImgsViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductImgsViewHolder holder, final int position) {
        RelativeLayout rlContainer = holder.itemView.findViewById(R.id.rl_container);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.$width(), LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_VERTICAL;
        rlContainer.setLayoutParams(params);
        holder.mTvName.setText(mData.get(position).getCompanyName());
        holder.mTvAddress.setText(mData.get(position).getCompanyAddress());
        holder.mTvStatus.setText(AppContext.get().getResources().getString(R.string.str_detail));
        holder.mTvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuIsOpen()) {
                    closeMenu();//关闭菜单
                }
                mIDeleteBtnClickListener.onDeleteBtnCilck(v, position);
            }
        });
        rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //判断是否有删除菜单打开
                if (menuIsOpen()) {
                    closeMenu();//关闭菜单
                } else {
                    mIDeleteBtnClickListener.onItemClick(v, position);
                }
            }
        });
        holder.mTvStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //判断是否有删除菜单打开
                if (menuIsOpen()) {
                    closeMenu();//关闭菜单
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("AddressListBean", mData.get(position));
                    UIHelper.startActivity(AddAddresssActivity.class, bundle);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    class ProductImgsViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvName;
        private TextView mTvAddress;
        private TextView mTvStatus;
        private TextView mTvDelete;

        public ProductImgsViewHolder(View itemView) {
            super(itemView);
            mTvName = itemView.findViewById(R.id.tv_shipping_list_title);
            mTvAddress = itemView.findViewById(R.id.tv_shipping_list_content);
            mTvStatus = itemView.findViewById(R.id.tv_shipping_list_status);
            mTvDelete = itemView.findViewById(R.id.tv_delete);
            ((LeftSlideView) itemView).setSlidingButtonListener(MineShippingAdapter.this);
        }
    }

    /**
     * 删除菜单打开信息接收
     */
    @Override
    public void onMenuIsOpen(View view) {
        mMenu = (LeftSlideView) view;
    }

    /**
     * 滑动或者点击了Item监听    *    * @param leftSlideView
     */
    @Override
    public void onDownOrMove(LeftSlideView leftSlideView) {
        if (menuIsOpen()) {
            if (mMenu != leftSlideView) {
                closeMenu();
            }
        }
    }

    /**
     * 关闭菜单
     */
    public void closeMenu() {
        mMenu.closeMenu();
        mMenu = null;
    }

    /**
     * 判断菜单是否打开    *    * @return
     */
    public Boolean menuIsOpen() {
        if (mMenu != null) {
            return true;
        }
        return false;
    }

}

