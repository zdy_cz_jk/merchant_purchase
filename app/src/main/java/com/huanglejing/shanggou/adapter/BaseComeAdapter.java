package com.huanglejing.shanggou.adapter;

import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.bean.NewComeProBean;
import com.huanglejing.shanggou.common.AppContext;
import com.huanglejing.shanggou.data.DictKt;
import com.huanglejing.shanggou.greendao.CacheGoodsBean;
import com.huanglejing.shanggou.utils.ExtentionFuncsKt;
import com.huanglejing.shanggou.utils.ToastUtils;
import com.huanglejing.shanggou.view.FloatingTextView.RxHeartLayout;

import java.util.List;
import java.util.Random;

public class BaseComeAdapter extends BaseQuickAdapter<NewComeProBean, BaseViewHolder> {
    private List<CacheGoodsBean> mCacheData;
    private OnItemClickListener onItemClickListener;
    private CacheOrderBeanListener CacheOrderBeanListener;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setCacheOrderBeanListener(CacheOrderBeanListener CacheOrderBeanListener) {
        this.CacheOrderBeanListener = CacheOrderBeanListener;
    }
    public void setCacheData(List<CacheGoodsBean> mCacheData) {
        this.mCacheData = mCacheData;
    }

    public BaseComeAdapter(@Nullable List<NewComeProBean> data) {
        super(R.layout.layout_product_list_item, data);
    }

    @Override
    protected void convert(final BaseViewHolder helper,final NewComeProBean item) {
        int i1 = 0;
        int cacheNum = 0;
        if (mCacheData != null && mCacheData.size() > 0) {
            for (int i = 0; i < mCacheData.size(); i++) {
                try {
                    if (item.getId().equals(mCacheData.get(i).getGoodNO())) {
                        i1 = Integer.parseInt(mCacheData.get(i).getNum());
                    }
                } catch (Exception e) {
                }
            }
        }
        if (i1 == 0) {
            ((TextView) helper.getView(R.id.tv_product_list_num)).setVisibility(View.GONE);
            ((ImageView) helper.getView(R.id.iv_product_list_minus)).setVisibility(View.GONE);
        } else {
            ((TextView) helper.getView(R.id.tv_product_list_num)).setVisibility(View.VISIBLE);
            ((ImageView) helper.getView(R.id.iv_product_list_minus)).setVisibility(View.VISIBLE);
        }
        double zk = 0;
        double price = 0;
        try {
            cacheNum=i1*(Integer.parseInt(mData.get(helper.getAdapterPosition()).getBagCount()));
            zk = Double.parseDouble(item.getZk()) / 100;
            Log.d("zk", "zk    " + zk);
        } catch (Exception e) {
            zk = 0;
            Log.d("zk", "zk  Exception  " + zk);
        }
        try {
            price = Double.parseDouble(item.getPrice());
        } catch (Exception e) {
            price = 0;
        }
        ((TextView) helper.getView(R.id.tv_product_list_old_price)).getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        if (zk > 0) {
            price = price * (1 - zk);
            ((TextView) helper.getView(R.id.tv_product_list_Price)).setText(ExtentionFuncsKt.formatNum(price) + "€");
            ((TextView) helper.getView(R.id.tv_product_list_old_price)).setVisibility(View.VISIBLE);
         //   ((TextView) helper.getView(R.id.tv_product_list_Price)).setTextColor(Color.parseColor("#ED3009"));
        } else {
            ((TextView) helper.getView(R.id.tv_product_list_Price)).setText(ExtentionFuncsKt.formatNum(price) + "€");
          //  ((TextView) helper.getView(R.id.tv_product_list_Price)).setTextColor(Color.parseColor("#4EABE4"));
            ((TextView) helper.getView(R.id.tv_product_list_old_price)).setVisibility(View.INVISIBLE);
        }

        ((TextView) helper.getView(R.id.tv_product_list_title)).setText(item.getName());
        ((TextView) helper.getView(R.id.tv_product_list_stock)).setText("库存："+mData.get(helper.getAdapterPosition()).getCount());
        ((TextView) helper.getView(R.id.tv_product_list_goods_num)).setText("货号："+mData.get(helper.getAdapterPosition()).getNo());
        if (DictKt.getCacheShowStock()){
            ((TextView) helper.getView(R.id.tv_product_list_stock)).setVisibility(View.VISIBLE);
        }else {
            ((TextView) helper.getView(R.id.tv_product_list_stock)).setVisibility(View.GONE);
        }
        if (DictKt.getCacheShowGoods()){
            ((TextView) helper.getView(R.id.tv_product_list_goods_num)).setVisibility(View.VISIBLE);
        }else {
            ((TextView) helper.getView(R.id.tv_product_list_goods_num)).setVisibility(View.GONE);
        }
        ((TextView) helper.getView(R.id.tv_product_list_unit)).setText("包装："+mData.get(helper.getAdapterPosition()).getBagCount()+"u/"+mData.get(helper.getAdapterPosition()).getBoxCount()+"c");
        if (zk>0){
            ((TextView) helper.getView(R.id.tv_product_list_zhe_kou)).setText("-"+(zk*100)+"%");
        }else {
            ((TextView) helper.getView(R.id.tv_product_list_zhe_kou)).setVisibility(View.GONE);
        }
        ((TextView) helper.getView(R.id.tv_product_list_old_price)).setText(item.getPrice());
        ((TextView) helper.getView(R.id.tv_product_list_num)).setText("" + cacheNum);
        ((ImageView) helper.getView(R.id.iv_product_list_add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RxHeartLayout) helper.getView(R.id.heart_layout)).post(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            String num = ((TextView) helper.getView(R.id.tv_product_list_num)).getText().toString();
                            if (TextUtils.isEmpty(num)) {
                                num = "0";
                            }
                            int i = Integer.parseInt(num);
                            int i1 = Integer.parseInt(item.getCount());
                            if (i == i1) {
                                ToastUtils.toastLong(AppContext.get().getResources().getString(R.string.str_maxmun_to_buy) + i1);
                                return;
                            }
                            int numAdd = i + Integer.parseInt(item.getBagCount());
                            int rgb = Color.rgb(255, 0, 0);
                            ((TextView) helper.getView(R.id.tv_product_list_num)).setVisibility(View.VISIBLE);
                            ((ImageView) helper.getView(R.id.iv_product_list_minus)).setVisibility(View.VISIBLE);
                            ((TextView) helper.getView(R.id.tv_product_list_num)).setText(numAdd + "");
                            ((RxHeartLayout) helper.getView(R.id.heart_layout)).addHeartAndText(rgb, "+" + numAdd);
                            if (CacheOrderBeanListener != null) {
                                CacheOrderBeanListener.onCacheOrderBeanAdd(helper.getAdapterPosition(), numAdd, item);
                            }
                        } catch (Exception e) {
                        }

                    }
                });
            }
        });
        ((ImageView) helper.getView(R.id.iv_product_list_minus)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RxHeartLayout) helper.getView(R.id.heart_layout_minus)).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String num = ((TextView) helper.getView(R.id.tv_product_list_num)).getText().toString();
                            if (TextUtils.isEmpty(num)) {
                                num = "0";
                            }
                            int i = Integer.parseInt(num);
                            if (i == 0) {
                                return;
                            }

                            int numMinus = i - Integer.parseInt(item.getBagCount());

                            if (numMinus > 0) {
                                int rgb = Color.rgb(255, 0, 0);
                                ((TextView) helper.getView(R.id.tv_product_list_num)).setText(numMinus + "");
                                ((RxHeartLayout) helper.getView(R.id.heart_layout_minus)).addHeartAndText(rgb, "-" + numMinus);
                                ((TextView) helper.getView(R.id.tv_product_list_num)).setVisibility(View.VISIBLE);
                                ((ImageView) helper.getView(R.id.iv_product_list_minus)).setVisibility(View.VISIBLE);

                            } else if (numMinus == 0) {
                                ((TextView) helper.getView(R.id.tv_product_list_num)).setText(numMinus + "");
                                ((TextView) helper.getView(R.id.tv_product_list_num)).setVisibility(View.GONE);
                                ((ImageView) helper.getView(R.id.iv_product_list_minus)).setVisibility(View.GONE);
                            } else if (numMinus < 0) {
                                return;
                            }

                            if (CacheOrderBeanListener != null) {
                                CacheOrderBeanListener.onCacheOrderBeanMinius(helper.getAdapterPosition(), numMinus, item);
                            }
                        } catch (Exception e) {
                        }

                    }
                });
            }
        });

        Glide.with(helper.itemView.getContext()).load(item.getUrl())
                .placeholder(R.drawable.ic_img_error)
                .error(R.drawable.ic_img_error)
                .centerCrop()
                .thumbnail(0.1f)
                .into(((ImageView) helper.getView(R.id.iv_product_list_icon)));

        ((RelativeLayout) helper.getView(R.id.rl_product_list_container)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, helper.getAdapterPosition());
                }
            }
        });
    }

    public interface OnItemClickListener {
        public void onItemClick(View v, int position);
    }

    public interface CacheOrderBeanListener {
        public void onCacheOrderBeanAdd(int position, int selectNum, NewComeProBean bean);

        public void onCacheOrderBeanMinius(int position, int selectNum, NewComeProBean bean);
    }
}
