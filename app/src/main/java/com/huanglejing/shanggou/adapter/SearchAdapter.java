package com.huanglejing.shanggou.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.bean.NewComeProBean;
import com.huanglejing.shanggou.view.FloatingTextView.RxHeartLayout;

import java.util.ArrayList;
import java.util.Random;

public class SearchAdapter extends BaseQuickAdapter<NewComeProBean, BaseViewHolder> {


    public SearchAdapter(@Nullable ArrayList<NewComeProBean> data) {
        super(R.layout.layout_product_list_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, NewComeProBean item) {

        helper.setText(R.id.tv_product_list_title, item.getName())
                .setText(R.id.tv_product_list_unit, item.getCount())
                .setText(R.id.tv_product_list_Price, item.getBagCount())
                .setText(R.id.tv_product_list_old_price, item.getPrice())
                .setText(R.id.tv_product_list_num, "" + 0);
        ImageView mIvIcon = helper.getView(R.id.iv_product_list_icon);
        ImageView mIvAdd = helper.getView(R.id.iv_product_list_add);
        final TextView mTvNum = helper.getView(R.id.tv_product_list_num);
        final RxHeartLayout mRxHeart = helper.getView(R.id.heart_layout);
        mIvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRxHeart.post(new Runnable() {
                    @Override
                    public void run() {
                        int rgb = Color.rgb(255, 0, 0);
                        try {
                            String num = mTvNum.getText().toString();
                            int numAdd = Integer.parseInt(num) + 1;
                            mTvNum.setText(numAdd + "");
                            mRxHeart.addHeartAndText(rgb, "+"+numAdd);
                        } catch (Exception e) {
                        }

                    }
                });
            }
        });

        Glide.with(helper.itemView.getContext()).load(item.getUrl())
                .placeholder(R.drawable.ic_img_error)
                .error(R.drawable.ic_img_error)
                .centerCrop()
                .thumbnail(0.1f)
                .into(mIvIcon);
//        helper.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (onItemClickListener != null) {
//                    onItemClickListener.onItemClick(v, position);
//                }
//            }
//        });
    }



}
