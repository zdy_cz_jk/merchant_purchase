package com.huanglejing.shanggou.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.activity.OrderDetailActivity;
import com.huanglejing.shanggou.bean.NowOrderBean;
import com.huanglejing.shanggou.common.AppContext;
import com.huanglejing.shanggou.utils.UIHelper;

import java.util.ArrayList;

public class MineOrderAdapter extends RecyclerView.Adapter<MineOrderAdapter.MineOrderViewHolder> {
    private Context context;
    private ArrayList<NowOrderBean> mData;

    public MineOrderAdapter(Context context, ArrayList<NowOrderBean> mData) {
        this.context = context;
        this.mData = mData;
    }

    public void setData(ArrayList<NowOrderBean> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public MineOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_mine_order_list_item, parent, false);
        MineOrderViewHolder mineOrderViewHolder = new MineOrderViewHolder(inflate);
        return mineOrderViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MineOrderViewHolder holder,final int position) {
        holder.mTvNum.setText(mData.get(position).getBh());
        holder.mTvMoney.setText(mData.get(position).getHj());
        holder.mTvTime.setText(mData.get(position).getCreateTime());
        holder.mTvStatus.setText(mData.get(position).getZt());
        holder.mTvContent.setText(mData.get(position).getBz());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bun = new Bundle();
                bun.putString("orderGuid", mData.get(position).getOrderGuid());
                bun.putString("companyId", AppContext.get().getCompanyId());
                UIHelper.startActivity(OrderDetailActivity.class, bun);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    class MineOrderViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvNum;
        private TextView mTvMoney;
        private TextView mTvTime;
        private TextView mTvStatus;
        private TextView mTvContent;

        public MineOrderViewHolder(View itemView) {
            super(itemView);
            mTvNum = itemView.findViewById(R.id.tv_mine_order_num);
            mTvMoney = itemView.findViewById(R.id.tv_mine_order_money);
            mTvTime = itemView.findViewById(R.id.tv_mine_order_time);
            mTvStatus = itemView.findViewById(R.id.tv_mine_order_status);
            mTvContent = itemView.findViewById(R.id.tv_mine_order_content);
        }

    }
}
