package com.huanglejing.shanggou.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.activity.ProListDetailActivity;
import com.huanglejing.shanggou.bean.NewComeProBean;
import com.huanglejing.shanggou.greendao.CacheGoodsBean;
import com.huanglejing.shanggou.utils.ExtentionFuncsKt;
import com.huanglejing.shanggou.utils.UIHelper;
import com.huanglejing.shanggou.view.FloatingTextView.RxHeartLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ProListDetailAdapter extends RecyclerView.Adapter<ProListDetailAdapter.ProductImgsViewHolder> {
    private Context context;
    private ArrayList<NewComeProBean> mData;
    //  private List<CacheGoodsBean> mCacheData;
    private Map<String, String> mCacheId = new HashMap<>();
    private TextView numView;


    public void setCacheData(List<CacheGoodsBean> mCacheData) {
        //this.mCacheData = mCacheData;
        if (mCacheData == null || mCacheId == null) {
            return;
        }
        mCacheId.clear();
        for (int i = 0; i < mCacheData.size(); i++) {
            mCacheId.put(mCacheData.get(i).getGoodNO(), mCacheData.get(i).getNum());
        }
    }

    public ProListDetailAdapter(Context context, ArrayList<NewComeProBean> mData) {
        this.context = context;
        this.mData = mData;
    }

    public void setData(ArrayList<NewComeProBean> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductImgsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_product_list_detail_item, parent, false);
        return new ProductImgsViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductImgsViewHolder holder, final int position) {
        int i1 = 0;
        String zhekou = "";
        int cacheNum = 0;
        try {

            if (mCacheId != null && mData != null && mData.size() > position
                    && mData.get(position) != null
                    && mData.get(position).getId() != null
                    && mCacheId.get(mData.get(position).getId()) != null) {
                String num = mCacheId.get(mData.get(position).getId());
                i1 = Integer.parseInt(num);
            }
            cacheNum=i1*(Integer.parseInt(mData.get(position).getBagCount()));
        } catch (Exception e) {
        }

        if (i1 == 0) {
            holder.mTvNum.setVisibility(View.GONE);
        } else {
            holder.mTvNum.setVisibility(View.VISIBLE);
        }
        double zk = 0;
        double price = 0;
        try {
            zk = Double.parseDouble(mData.get(position).getZk()) / 100;
            Log.d("zk", "zk    " + zk);
        } catch (Exception e) {
            zk = 0;
            Log.d("zk", "zk  Exception  " + zk);
        }
        try {
            price = Double.parseDouble(mData.get(position).getPrice());
        } catch (Exception e) {
            price = 0;
        }
        holder.mTvOrigin.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        if (zk > 0) {
            price = price * (1 - zk);
            holder.mTvPrice.setText(ExtentionFuncsKt.formatNum(price) + "€");
            holder.mTvOrigin.setVisibility(View.VISIBLE);
            holder.mTvZheKou.setVisibility(View.VISIBLE);
        } else {
            holder.mTvPrice.setText(ExtentionFuncsKt.formatNum(price) + "€");
            holder.mTvOrigin.setVisibility(View.GONE);
            holder.mTvZheKou.setVisibility(View.GONE);
        }
        ProListDetailAdapter.this.numView = holder.mTvNum;
        holder.mTvTitle.setText(mData.get(position).getName());
        holder.mTvContent.setText(mData.get(position).getNo());
        holder.mTvBagCount.setText("包装数:" + mData.get(position).getBagCount() + " / 装箱数:" + mData.get(position).getBoxCount());
        holder.mTvOrigin.setText(mData.get(position).getPrice()+"€");
        holder.mTvZheKou.setText("-"+mData.get(position).getZk() + "%");

        holder.mTvNum.setText("" +cacheNum);

        holder.mIvIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> imgs = new ArrayList<>();
                imgs.add(mData.get(position).getUrl());
                if (context instanceof Activity) {
                    ExtentionFuncsKt.watchLargeImage((Activity) context, imgs, 0);
                }

            }
        });

        Glide.with(context).load(mData.get(position).getUrl())
                .placeholder(R.drawable.ic_img_error)
                .error(R.drawable.ic_img_error)
                .centerCrop()
                .thumbnail(0.1f)
                .into(holder.mIvIcon);

    }


    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    class ProductImgsViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvTitle;
        private TextView mTvContent;
        private TextView mTvBagCount;
        private TextView mTvPrice;
        private TextView mTvNum;
        private TextView mTvZheKou;
        private TextView mTvOrigin;
        private ImageView mIvIcon;

        public ProductImgsViewHolder(View itemView) {
            super(itemView);
            mIvIcon = itemView.findViewById(R.id.iv_product_list_detail_icon);
            mTvTitle = itemView.findViewById(R.id.tv_product_list_detail_title);
            mTvContent = itemView.findViewById(R.id.tv_product_list_detail_stock);
            mTvBagCount = itemView.findViewById(R.id.tv_product_list_detail_bagCount);
            mTvNum = itemView.findViewById(R.id.tv_product_list_detail_num);
            mTvPrice = itemView.findViewById(R.id.tv_product_list_detail_price);
            mTvZheKou = itemView.findViewById(R.id.tv_product_list_detail_zhekou);
            mTvOrigin = itemView.findViewById(R.id.tv_product_list_detail_origin);
        }
    }
}

