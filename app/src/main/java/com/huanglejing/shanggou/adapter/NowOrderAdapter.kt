package com.huanglejing.shanggou.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.greendao.CacheGoodsBean
import com.huanglejing.shanggou.interfaces.IonSlidingViewClickListener
import com.huanglejing.shanggou.utils.DisplayUtils
import com.huanglejing.shanggou.utils.formatNum
import com.huanglejing.shanggou.view.LeftSlideView

class NowOrderAdapter : BaseQuickAdapter<CacheGoodsBean, BaseViewHolder>(R.layout.layout_current_order_list_item), LeftSlideView.IonSlidingButtonListener {


    private var mMenu: LeftSlideView? = null
    private var mIDeleteBtnClickListener: IonSlidingViewClickListener? = null

    public fun setIonSlidingViewClickListener(litener: IonSlidingViewClickListener) {
        this.mIDeleteBtnClickListener = litener
    }

    override fun convert(helper: BaseViewHolder?, item: CacheGoodsBean?) {
        val params = LinearLayout.LayoutParams(DisplayUtils.`$width`(), LinearLayout.LayoutParams.WRAP_CONTENT)
        params.gravity = Gravity.CENTER_VERTICAL
        helper?.getView<RelativeLayout>(R.id.rl_container)?.setLayoutParams(params)
        (helper?.itemView as? LeftSlideView)?.setSlidingButtonListener(this@NowOrderAdapter)
        helper?.setText(R.id.tv_current_order_title, item?.name)
                ?.setText(R.id.tv_current_order_unit, "${item?.price}€")
                ?.setText(R.id.tv_current_order_num, "${item?.num?.toIntOrNull()?.times(item?.bagCount?.toIntOrNull()?:1)?:0}")
                ?.setText(R.id.tv_item_current_order_total, "${formatNum(item?.totalPrice)}€")
        Glide.with(helper?.itemView?.context).load(item?.url)
                .placeholder(R.drawable.ic_img_error)
                .error(R.drawable.ic_img_error)
                .centerCrop()
                .thumbnail(0.1f)
                .into(helper?.getView<ImageView>(R.id.iv_current_order_item))

        helper?.getView<TextView>(R.id.tv_delete)?.setOnClickListener(View.OnClickListener { v ->
            if (menuIsOpen()!!) {
                closeMenu()//关闭菜单
            }
            this.mIDeleteBtnClickListener?.onDeleteBtnCilck(v, helper?.adapterPosition ?: 0)
        })
        helper?.getView<RelativeLayout>(R.id.rl_container)?.setOnClickListener(View.OnClickListener { v ->
            //判断是否有删除菜单打开
            if (menuIsOpen()!!) {
                closeMenu()//关闭菜单
            } else {
                this.mIDeleteBtnClickListener?.onItemClick(v, helper?.adapterPosition ?: 0)
            }
        })

    }

    override fun onMenuIsOpen(view: View?) {
        mMenu = view as LeftSlideView

    }

    override fun onDownOrMove(leftSlideView: LeftSlideView?) {
        if (menuIsOpen()!!) {
            if (mMenu !== leftSlideView) {
                closeMenu()
            }
        }
    }


    /**
     * 关闭菜单
     */
    fun closeMenu() {
        mMenu?.closeMenu()
        mMenu = null
    }

    /**
     * 判断菜单是否打开    *    * @return
     */
    fun menuIsOpen(): Boolean? {
        return if (mMenu != null) {
            true
        } else false
    }
}