package com.huanglejing.shanggou.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.bean.CompanyListBean;
import com.huanglejing.shanggou.interfaces.IonSlidingViewClickListener;
import com.huanglejing.shanggou.utils.DisplayUtils;
import com.huanglejing.shanggou.view.LeftSlideView;

import java.util.ArrayList;

public class SellersAdapter extends RecyclerView.Adapter<SellersAdapter.SellersViewHolder>
        implements LeftSlideView.IonSlidingButtonListener {
    private Context context;
    private ArrayList<CompanyListBean> mData;
    private IonSlidingViewClickListener mIDeleteBtnClickListener;
    private LeftSlideView mMenu = null;
    public SellersAdapter(Context context, ArrayList<CompanyListBean> mData) {
        this.context = context;
        this.mData = mData;
        mIDeleteBtnClickListener = (IonSlidingViewClickListener) context;
    }

    public void setData(ArrayList<CompanyListBean> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public SellersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_sellers_list_item, parent, false);
        return new SellersViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull SellersViewHolder holder, final int position) {
        LinearLayout llContainer = holder.itemView.findViewById(R.id.ll_container);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.$width(), LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_VERTICAL;
        llContainer.setLayoutParams(params);
        holder.mTv.setText(mData.get(position).getCompanyDisplayName());
        Glide.with(context).load(mData.get(position).getCompanyLogo())
                .placeholder(R.drawable.ic_img_error)
                .error(R.drawable.ic_img_error)
                .centerCrop().thumbnail(0.1f)
                .into(holder.mIv);

        holder.mTvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuIsOpen()) {
                    closeMenu();//关闭菜单
                }
                mIDeleteBtnClickListener.onDeleteBtnCilck(v, position);
            }
        });
        llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //判断是否有删除菜单打开
                if (menuIsOpen()) {
                    closeMenu();//关闭菜单
                } else {
                    mIDeleteBtnClickListener.onItemClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }
    /**
     * 删除菜单打开信息接收
     */
    @Override
    public void onMenuIsOpen(View view) {
        mMenu = (LeftSlideView) view;
    }

    /**
     * 滑动或者点击了Item监听    *    * @param leftSlideView
     */
    @Override
    public void onDownOrMove(LeftSlideView leftSlideView) {
        if (menuIsOpen()) {
            if (mMenu != leftSlideView) {
                closeMenu();
            }
        }
    }

    /**
     * 关闭菜单
     */
    public void closeMenu() {
        mMenu.closeMenu();
        mMenu = null;
    }

    /**
     * 判断菜单是否打开    *    * @return
     */
    public Boolean menuIsOpen() {
        if (mMenu != null) {
            return true;
        }
        return false;
    }
    class SellersViewHolder extends RecyclerView.ViewHolder {
        private TextView mTv;
        private ImageView mIv;
        private TextView mTvDelete;
        public SellersViewHolder(View itemView) {
            super(itemView);
            mTv = itemView.findViewById(R.id.tv_sellers_list_name);
            mIv = itemView.findViewById(R.id.iv_sellers_list_icon);
            mTvDelete = itemView.findViewById(R.id.tv_delete);
            ((LeftSlideView) itemView).setSlidingButtonListener(SellersAdapter.this);
        }
    }
}
