package com.huanglejing.shanggou.adapter.emptyadapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import static com.huanglejing.shanggou.adapter.emptyadapter.BaseEmptyAdapter.TYPE_EMPTY;

/**
 * Created by ${赵东阳} on 2018/10/24.
 */

public class EmptyViewHolder extends RecyclerView.ViewHolder {

    public EmptyViewHolder(View itemView) {
        super(itemView);
        if (TYPE_EMPTY == getItemViewType()) {
            return;
        }
    }
}
