package com.huanglejing.shanggou.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.bean.CompanyListBean;
import com.huanglejing.shanggou.common.AppContext;
import com.huanglejing.shanggou.rx.event.CompanyIdEvent;
import com.huanglejing.shanggou.rx.event.JumpToCompanyEvent;
import com.huanglejing.shanggou.rx.rxbus.RxBus;

import java.util.ArrayList;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ProductImgsViewHolder> {
    private Context context;
    private ArrayList<CompanyListBean> mData;

    public HomeAdapter(Context context, ArrayList<CompanyListBean> mData) {
        this.context = context;
        this.mData = mData;
    }

    public void setData(ArrayList<CompanyListBean> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductImgsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_company_list_item, parent, false);
        return new ProductImgsViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductImgsViewHolder holder, final int position) {
        holder.mIvSelect.setVisibility(View.GONE);
        if (TextUtils.isEmpty(AppContext.get().getCompanyId())) {
            AppContext.get().setCompanyId(mData.get(0).getCompanyId() + "");
            RxBus.get().postEvent(new CompanyIdEvent(mData.get(0).getCompanyId(), mData.get(0).getCompanyLogo()));
            holder.mIvSelect.setVisibility(View.VISIBLE);
        } else {
            if (AppContext.get().getCompanyId().equals(mData.get(position).getCompanyId())){
                holder.mIvSelect.setVisibility(View.VISIBLE);
                RxBus.get().postEvent(new CompanyIdEvent(mData.get(position).getCompanyId(), mData.get(position).getCompanyLogo()));
            }

        }

        holder.mTvName.setText(mData.get(position).getCompanyDisplayName());
        holder.mTvPhone.setText(mData.get(position).getPhone());
        holder.mTvContent.setText(mData.get(position).getCompanyScope());
        Glide.with(context).load(mData.get(position).getCompanyLogo()).fitCenter()
                .placeholder(R.drawable.ic_img_error)
                .error(R.drawable.ic_img_error)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f).into(holder.mIvThumb);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppContext.get().setCompanyId(mData.get(position).getCompanyId());
                notifyDataSetChanged();
                RxBus.get().postEvent(new CompanyIdEvent(mData.get(position).getCompanyId(), mData.get(position).getCompanyLogo()));
                RxBus.get().postEvent(new JumpToCompanyEvent());
                //   UIHelper.startActivity(ProductListActivity.class);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    class ProductImgsViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvName;
        private TextView mTvPhone;
        private TextView mTvContent;
        private ImageView mIvThumb;
        private ImageView mIvSelect;

        public ProductImgsViewHolder(View itemView) {
            super(itemView);
            mTvName = itemView.findViewById(R.id.tv_company_list_name);
            mTvPhone = itemView.findViewById(R.id.tv_company_list_phone);
            mTvContent = itemView.findViewById(R.id.tv_company_list_content);
            mIvThumb = itemView.findViewById(R.id.iv_company_list);
            mIvSelect = itemView.findViewById(R.id.iv_company_list_item_selected);

        }
    }
}

