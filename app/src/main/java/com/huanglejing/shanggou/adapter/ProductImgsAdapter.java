package com.huanglejing.shanggou.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.activity.ProductListActivity;
import com.huanglejing.shanggou.bean.GoodsListBean;
import com.huanglejing.shanggou.utils.UIHelper;

import java.util.ArrayList;

public class ProductImgsAdapter extends RecyclerView.Adapter<ProductImgsAdapter.ProductImgsViewHolder> {
    private Context context;
    private ArrayList<GoodsListBean> mData;

    public ProductImgsAdapter(Context context, ArrayList<GoodsListBean> mData) {
        this.context = context;
        this.mData = mData;
    }

    public void setData(ArrayList<GoodsListBean> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductImgsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_pro_imgs_list_item, parent, false);

        return new ProductImgsViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductImgsViewHolder holder, final int position) {
        holder.mTv.setText(mData.get(position).getName());
        Glide.with(context).load(mData.get(position).getUrl()).fitCenter()
                .placeholder(R.drawable.ic_img_error)
                .error(R.drawable.ic_img_error)
                .thumbnail(0.1f).into(holder.mIv);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("categoryId", mData.get(position).getGuid());
                UIHelper.startActivity(ProductListActivity.class, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    class ProductImgsViewHolder extends RecyclerView.ViewHolder {
        private TextView mTv;
        private ImageView mIv;

        public ProductImgsViewHolder(View itemView) {
            super(itemView);
            mTv = itemView.findViewById(R.id.tv_pro_imgs_list_name);
            mIv = itemView.findViewById(R.id.iv_pro_imgs_list);
        }
    }
}

