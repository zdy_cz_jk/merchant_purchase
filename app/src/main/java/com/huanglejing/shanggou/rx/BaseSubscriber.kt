package com.huanglejing.shanggou.rx

import com.huanglejing.shanggou.common.BaseView


/**
 * Created by Administrator on 2016/8/16 0016.
 */
open class BaseSubscriber<T>(private val baseView: BaseView?) : SimpleSubscriber<T>() {


    override fun onStart() {
        super.onStart()
//        AndroidSchedulers.mainThread().createWorker().schedule {
//            baseView?.showLoading()
//        }
    }

    override fun onCompleted() {
        super.onCompleted()
        baseView?.dismissLoading()
    }

    override fun onError(e: Throwable) {
        super.onError(e)
        baseView?.dismissLoading()
    }

    override fun onNext(t: T) {
        super.onNext(t)
    }
}
