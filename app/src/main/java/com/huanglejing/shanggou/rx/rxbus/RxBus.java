package com.huanglejing.shanggou.rx.rxbus;



import com.huanglejing.mylibrary.KLog;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import rx.Observable;
import rx.Subscriber;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;

public class RxBus {
    private static volatile RxBus mDefaultInstance;
    private final SerializedSubject<Object, Object> mBus;
    private final SerializedSubject<Object, Object> rxStickBus;
    private final Map<Class<?>, Object> mStickyEventMap;

    private RxBus() {
        mBus = new SerializedSubject<>(PublishSubject.create());
        rxStickBus = new SerializedSubject(BehaviorSubject.create());
        mStickyEventMap = new ConcurrentHashMap<>();
    }

    public static RxBus get() {
        if (mDefaultInstance == null) {
            synchronized (RxBus.class) {
                if (mDefaultInstance == null) {
                    mDefaultInstance = new RxBus();
                }
            }
        }
        return mDefaultInstance;
    }

    /**
     * 发送事件
     */
    public void postEvent(Object event) {
        KLog.i("postEvent", event);
        mBus.onNext(event);
    }

    /**
     * 根据传递的 eventType 类型返回特定类型(eventType)的 被观察者
     */
    public <T> Observable<T> toObservable(Class<T> eventType) {
        KLog.d("toObservable", eventType.getName());
        return mBus.asObservable().ofType(eventType);
    }

    /**
     * 判断是否有订阅者
     */
    public boolean hasObservers() {
        return mBus.hasObservers();
    }

    public void reset() {
        mDefaultInstance = null;
    }

    /**
     * Stciky 相关
     */

    /**
     * 发送一个新Sticky事件
     */

    public void postStickEvent(Object event) {
        KLog.d("postStickEvent", event);
        synchronized (mStickyEventMap) {
            mStickyEventMap.put(event.getClass(), event);
        }
        rxStickBus.onNext(event);
    }

    /**
     * 根据传递的 eventType 类型返回特定类型(eventType)的 被观察者
     */
    public <T> Observable<T> toStickObservable(final Class<T> eventType) {
        KLog.d("toObservable", eventType.getName());
        synchronized (mStickyEventMap) {
            Observable<T> observable = mBus.asObservable().ofType(eventType);
            final Object event = mStickyEventMap.get(eventType);

            if (event != null) {
                return observable.mergeWith(Observable.create(new Observable.OnSubscribe<T>() {
                    @Override
                    public void call(Subscriber<? super T> subscriber) {
                        subscriber.onNext(eventType.cast(event));
                    }
                })).onBackpressureBuffer();
            } else {
                return observable.onBackpressureBuffer();
            }
        }
    }

    /**
     * 根据eventType获取Sticky事件
     */
    public <T> T getStickyEvent(Class<T> eventType) {
        synchronized (mStickyEventMap) {
            return eventType.cast(mStickyEventMap.get(eventType));
        }
    }

    /**
     * 判断是否有订阅者
     */
    public boolean hasStickObservers() {
        return rxStickBus.hasObservers();
    }


    /**
     * 移除指定eventType的Sticky事件
     */
    public <T> T removeStickyEvent(Class<T> eventType) {
        synchronized (mStickyEventMap) {
            return eventType.cast(mStickyEventMap.remove(eventType));
        }
    }

    /**
     * 移除所有的Sticky事件
     */
    public void removeAllStickyEvents() {
        synchronized (mStickyEventMap) {
            mStickyEventMap.clear();
        }
    }
}