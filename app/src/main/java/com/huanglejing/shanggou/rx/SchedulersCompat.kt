package com.huanglejing.shanggou.rx

import com.huanglejing.shanggou.rx.JobExecutor
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Joker on 2015/8/10.
 */
object SchedulersCompat {
    /**
     * Don't break the chain: use RxJava's compose() operator
     */
    @JvmStatic fun <T> computation(): Observable.Transformer<T, T> {
        return Observable.Transformer { it.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io()) }
    }

    @JvmStatic fun <T> io(): Observable.Transformer<T, T> {
        return Observable.Transformer { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io()) }
    }

    @JvmStatic fun <T> newThread(): Observable.Transformer<T, T> {
        return Observable.Transformer { it.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io()) }
    }

    @JvmStatic fun <T> trampoline(): Observable.Transformer<T, T> {
        return Observable.Transformer { it.subscribeOn(Schedulers.trampoline()).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io()) }
    }

    @JvmStatic fun <T> jobExecutor(): Observable.Transformer<T, T> {
        return Observable.Transformer { it.subscribeOn(Schedulers.from(JobExecutor.eventExecutor)).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io()) }
    }

    @JvmStatic fun <T> mainThread(): Observable.Transformer<T, T> {
        return Observable.Transformer { it.observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io()) }
    }
}
