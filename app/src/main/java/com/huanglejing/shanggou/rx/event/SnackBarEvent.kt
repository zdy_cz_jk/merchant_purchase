package  com.huanglejing.shanggou.rx.event
import android.support.annotation.StringRes
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.utils.SnackBarUtils

/**
 * Created by Administrator on 2016/10/21 0021.
 */

class SnackBarEvent {
    val text: String

    var level: Int = SnackBarUtils.Level.INFO
        private set
        @SnackBarUtils.Level get

    constructor(text: String, level: Int) {
        this.text = text
        this.level = level
    }

    constructor(@StringRes res: Int, level: Int) {
        this.text = AppContext.get().getString(res)
        this.level = level
    }
}
