package com.huanglejing.shanggou.rx

import android.content.Context
import android.text.TextUtils
import android.view.View
import com.huanglejing.shanggou.rx.event.OnResultEvent
import com.huanglejing.shanggou.rx.oepration.LoginOperation
import com.huanglejing.shanggou.common.BaseActivity
import rx.Observable
import rx.functions.Action1
import rx.functions.Func1
import java.util.concurrent.TimeUnit

/**
 * Created by Administrator on 2016/8/9 0009.
 */

object RxUtils {

    @JvmStatic val NoNull: Func1<Any, Boolean> = Func1 { o -> o != null }
    @JvmStatic var SNoNull: Func1<String, Boolean> = Func1 { s -> !TextUtils.isEmpty(s) }

    /**
     * 每秒倒计时

     * @param time
     * *
     * @return
     */
    @JvmStatic fun countdown(time: Int): Observable<Int> {
        val countTime = if (time < 0) 0 else time
        return Observable.interval(0, 1, TimeUnit.SECONDS).map { countTime - it.toInt() }.take(countTime + 1)
    }

    @JvmStatic fun makeClickObservable(v: View): Observable<View> = Observable.create<View> { it.onNext(v) }.throttleFirst(800, TimeUnit.MILLISECONDS) // 缓存0.8s内的点击

    @JvmStatic fun isContextFinish(context: Context): Boolean = (context as? BaseActivity)?.isDestroyedCompatible ?: false
    @JvmStatic fun startLoginCall(tag: String, action1: Action1<OnResultEvent.OnLoginResultEvent>):
            Observable<String> = Observable.just(tag).flatMap(LoginOperation<String>(tag, action1))
}




