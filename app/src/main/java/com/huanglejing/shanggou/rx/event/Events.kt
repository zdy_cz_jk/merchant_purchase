package  com.huanglejing.shanggou.rx.event



/**
 * Created by Administrator on 2017/4/26 0026.
 */
interface RxBusEvent

//厂房经纬度
data class PassLatLng(val content: String) : RxBusEvent

//region account event
class AuthResultEvent(val map: Map<String, String>,val platform: String) : RxBusEvent
//region account event
class JumpToCompanyEvent() : RxBusEvent
class JumpToHomeEvent() : RxBusEvent

class CompanyIdEvent(val companyId:String,var url:String) : RxBusEvent
class CompanyRefreshEvent() : RxBusEvent




