package  com.huanglejing.shanggou.rx.event

/**
 * Created by Administrator on 2016/9/9 0009.
 */

open class OnResultEvent<T>(val tag: String, val data: T) {

    /**
     * 登录结果，
     */
    class OnLoginResultEvent(tag: String, data: Boolean) : OnResultEvent<Boolean>(tag, data)
}
