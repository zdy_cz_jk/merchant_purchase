package com.huanglejing.shanggou.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;

import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.weixin.view.WXCallbackActivity;

import java.util.Map;

public class WXEntryActivity extends WXCallbackActivity {
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void onReq(BaseReq req) {
        super.onReq(req);
    }

    //微信回调
    @Override
    public void onResp(BaseResp resp) {   //分享之后的回调
        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK: //正确返回
                //Toast.makeText(this, "微信分享成功回调了111", Toast.LENGTH_SHORT).show();
                break;
        }
        super.onResp(resp);
    }

    @Override
    protected void handleIntent(Intent intent) {

        mWxHandler.setAuthListener(new UMAuthListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {

            }

            @Override
            public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
            }

            @Override
            public void onError(SHARE_MEDIA platform, int action, Throwable t) {

            }

            @Override
            public void onCancel(SHARE_MEDIA platform, int action) {

            }
        });
        super.handleIntent(intent);
    }
}
