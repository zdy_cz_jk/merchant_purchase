package com.huanglejing.shanggou.bean

import java.io.Serializable

class GoodsListBean :Serializable{

    /**
     * guid : {FE5D2CFC-8CE9-4E19-A685-357F05BD44FC}
     * id : 57270
     * level : 0
     * name : 清洁系列
     * url : http://dadisoft.cn/PMBOX/dadisoft/40363phone.jpg
     */

    var guid: String? = null
    var id: Int = 0
    var level: Int = 0
    var name: String? = null
    var url: String? = null
}
