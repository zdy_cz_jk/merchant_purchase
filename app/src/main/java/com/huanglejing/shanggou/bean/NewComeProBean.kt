package com.huanglejing.shanggou.bean

import java.io.Serializable

class NewComeProBean  :Serializable{

    /**
     * bagCount : 1
     * boxCount : 1
     * count : 50
     * id : 15649077
     * name : LED车灯一套
     * no : 10003
     * parentGuid : {D5A1F15E-CE20-4E4D-B82A-7FA79B6D2812}
     * price : 2000
     * url : http://dadisoft.cn/PMBOX/dadisoft/10003phone.jpg
     * xname :
     * zk :
     */

    var bagCount: String? = null
    var boxCount: String? = null
    var count: String? = null
    var id: String? = null
    var name: String? = null
    var no: String? = null
    var parentGuid: String? = null
    var price: String? = null
    var url: String? = null
    var xname: String? = null
    var zk: String? = null
}
