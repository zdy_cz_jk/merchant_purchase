package com.huanglejing.shanggou.bean

class CompanyListBean {

    /**
     * companyDisplayName : 大地贸易
     * companyId : 15
     * companyLogo : http://dadisoft.cn/PMBOX/dadisoft_logo.jpg
     * companyName : dadisoft
     * companyScope : 汽车配件、自行车配件、五金等
     */

    var companyDisplayName: String? = null
    var companyId:  String? = null
    var companyLogo: String? = null
    var companyName: String? = null
    var companyScope: String? = null
    var phone: String? = null
}
