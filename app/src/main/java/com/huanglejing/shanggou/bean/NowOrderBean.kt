package com.huanglejing.shanggou.bean

class NowOrderBean {

    /**
     * bh : 20180529154839374
     * bz :
     * count : 8
     * createTime : 2018-05-29 15:48:39
     * hj : 12.02
     * orderGuid : 2df0c767-4ef4-412c-b817-37111c6e7233
     * orderId : 5750
     */

    var bh: String? = null
    var bz: String? = null
    var count: String? = null
    var createTime: String? = null
    var zt: String? = null
    var hj: String? = null
    var orderGuid: String? = null
    var orderId: Int = 0
}
