package com.huanglejing.shanggou.bean

import java.io.Serializable

class UpdateInfo:Serializable {

    /**
     * app : zj
     * id : 1
     * needupdate : false
     * url : http://dadisoft.cn:8080/PMBOX/HLJZJ.exe
     * ver : 111245
     * zipmd5 : 121221
     * zipurl : 1212
     */

    var app: String? = null
    var id: Int = 0
    var needupdate: Boolean? = false
    var url: String? = null
    var ver: Int = 0
}
