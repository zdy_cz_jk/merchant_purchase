package com.huanglejing.shanggou.bean

/**
 * Created by ${赵东阳} on 2018/9/21.
 */

class AddressBean {
    var companyName = ""
    var peopleName = ""
    var dutyCode = "" //税号
    var address = ""
    var city = ""
    var province = ""
    var country = ""
    var aipCode = "" //邮编
    var phone = ""
    var email = ""
}
