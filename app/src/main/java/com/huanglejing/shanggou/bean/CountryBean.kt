package com.huanglejing.shanggou.bean

import android.os.Parcel
import android.os.Parcelable

 data class CountryBean (
    val name: String? = null,
    val code: String? = null
): Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<CountryBean> = object : Parcelable.Creator<CountryBean> {
            override fun createFromParcel(source: Parcel): CountryBean = CountryBean(source)
            override fun newArray(size: Int): Array<CountryBean?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(), source.readString())
    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(name)
        dest?.writeString(code)
    }

}


