package com.huanglejing.shanggou.bean

class OrderDetailBean {

    /**
     * count : 1
     * name : 电压力锅
     * no : 99999
     * price : 50
     * totalPrice : 50.00
     * url : http://dadisoft.cn/PMBOX/dadisoft/99999phone.jpg
     * xname : OLLA A PRESION ELECTRICA
     */

    var count: String? = null
    var name: String? = null
    var no: String? = null
    var price: String? = null
    var totalPrice: String? = null
    var url: String? = null
    var xname: String? = null
}
