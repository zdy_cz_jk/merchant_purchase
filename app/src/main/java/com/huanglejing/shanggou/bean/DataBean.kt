package com.huanglejing.shanggou.bean

import java.io.Serializable

/**
 * Created by Administrator on 2017/4/12 0012.
 */
data class PageJson(var page: Int? = null, val pageNow: Int? = null, val pageSize: Int = 0, val count: Int = 0, val pageCount: Int = 0)

data class LanguageBean(val img:String,val name: String) : Serializable {
}


