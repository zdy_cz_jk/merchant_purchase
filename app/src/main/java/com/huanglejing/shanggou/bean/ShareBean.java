package com.huanglejing.shanggou.bean;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;

import com.huanglejing.shanggou.utils.ImageLoaderKit;
import com.umeng.socialize.media.UMEmoji;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMVideo;
import com.umeng.socialize.media.UMusic;

import java.io.File;

/**
 * Created by Administrator on 2016/11/29.
 */

public class ShareBean {
    Context context;
    private String text = "";
    private String title = "";
    private String TargetUrl = "";
    private String follow = "";
    private UMImage image;
    private UMVideo video;
    private UMEmoji emoji;
    private UMusic uMusic;
    private File appfile;
    private UMImage extra;


    public ShareBean(Context context) {
        this.context = context;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTargetUrl() {
        return TargetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        TargetUrl = targetUrl;
    }

    public String getFollow() {
        return follow;
    }

    public void setFollow(String follow) {
        this.follow = follow;
    }

    public UMImage getImage() {
        if (image == null) {
            return new UMImage(context, "");
        } else {
            return image;
        }
    }

    public void setImage(String imageUrl) {
        if (ImageLoaderKit.isImageUriValid(imageUrl)) {
            this.image = new UMImage(context, imageUrl);
        }
    }
    public void setImage(Bitmap bitmap) {
        if (bitmap!=null){
            this.image = new UMImage(context, bitmap);
        }
    }



    public UMVideo getVideo() {
        return video;
    }

    public void setVideo(String videoUrl) {
        UMVideo video1 = new UMVideo(videoUrl);
        this.video = video1;
    }

    public UMEmoji getEmoji() {
        return emoji;
    }

    public void setEmoji(String emojiUrl) {
        UMEmoji emoji1 = new UMEmoji(context, emojiUrl);
        this.emoji = emoji1;
    }

    public UMusic getuMusic() {
        return uMusic;
    }

    public void setuMusic(String uMusicUrl) {
        UMusic uMusic1 = new UMusic(uMusicUrl);
        this.uMusic = uMusic1;
    }



    public File getAppfile() {
        return appfile;
    }

    public void setAppfile(String appFilePath) {
        File appFile = new File(appFilePath);
        this.appfile = appFile;
    }

    public UMImage getExtra() {
        return extra;
    }

    public void setExtra(String extraUrl) {
        UMImage extra1 = new UMImage(context, extraUrl);
        this.extra = extra1;
    }
}
