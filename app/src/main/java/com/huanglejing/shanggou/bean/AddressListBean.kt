package com.huanglejing.shanggou.bean

import java.io.Serializable

class AddressListBean :Serializable{

    /**
     * companyAddress :
     * companyName : 电子科技
     * country :
     * customerId : 4
     * id : 11
     * linkEmail : 123
     * linkMan :
     * linkTelephone : 123
     * postalCode : 123
     * province :
     */

    var companyAddress: String? = null
    var companyName: String? = null
    var country: String? = null
    var customerId: String? = null
    var id: String? = null
    var linkEmail: String? = null
    var linkMan: String? = null
    var linkTelephone: String? = null
    var postalCode: String? = null
    var province: String? = null
    var companySh: String? = null
    var city: String? = null

}
