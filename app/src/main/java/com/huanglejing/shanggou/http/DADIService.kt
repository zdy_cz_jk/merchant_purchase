package com.huanglejing.shanggou.http

import com.google.gson.JsonObject
import retrofit2.http.*
import rx.Observable

/**
 * Created by Administrator on 2016/7/22 0022.
 */

interface DADIService {

    @Deprecated("new api instead")
    @FormUrlEncoded
    @POST("{operation}")
    fun postOperation(@Path("operation") operation: String, @Field("json") param: String): Observable<JsonObject>

    @Deprecated("new api instead")
    @FormUrlEncoded
    @POST("{operation}")
    fun postOperation(@Path("operation") operation: String,
                      @Field("json") param: String,
                      @FieldMap userLogin: Map<String, String>): Observable<JsonObject>

    @Deprecated("new api instead")
    @FormUrlEncoded
    @POST("{operation}")
    fun postOperation(@Path("operation") operation: String,
                      @FieldMap fields: Map<String, String>): Observable<JsonObject>

    /*==========================================new api===================================================*/
    @POST("{operation}")
    fun post(@Path("operation", encoded = true) operation: String, @Body map: Map<String, @JvmSuppressWildcards Any>
             , @HeaderMap headers: Map<String, String> = emptyMap()): Observable<JsonObject>

    @FormUrlEncoded
    @POST("{operation}")
    fun postEncode(@Path("operation", encoded = true) operation: String, @FieldMap map: Map<String, @JvmSuppressWildcards Any>
             , @HeaderMap headers: Map<String, String> = emptyMap()): Observable<JsonObject>

    @GET("{operation}")
    fun get(@Path("operation", encoded = true) operation: String, @QueryMap map: Map<String, @JvmSuppressWildcards Any>
             , @HeaderMap headers: Map<String, String> = emptyMap()): Observable<JsonObject>
}
