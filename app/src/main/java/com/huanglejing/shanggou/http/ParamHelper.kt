package com.cfzx.http

import android.support.v4.util.ArrayMap
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.checkEmpty

/**
 * Created by 75176 on 2016/7/24 0024.
 */
class ParamHelper private constructor() {

//    private val instance by lazy(LazyThreadSafetyMode.PUBLICATION) { ParamHelper() }

    fun add(key: String, value: Any?): ParamHelper {
        if (key.checkEmpty() || value == null) return this
        params.put(key, value)
        return this
    }

    fun addMap(values: Map<String, Any>?): ParamHelper {
        if (values == null) return this
        params.putAll(values)
        return this
    }

    fun addPage(page: Int): ParamHelper {
        val map = ArrayMap<String, Int>()
        map.put("goToPage", page)
        map.put("rowDisplay", C.Page.PAGE_ROW)
        params.put("page", map)
        return this
    }

    /**
     * 获取当前参数集合
     * @return
     */
    val currentParams: Map<String, Any>
        get() = params


    override fun toString(): String {
        val s = AppContext.gson().toJson(params)
        KLog.i("params :$params => to string : $s")
        return s
    }

    private val params by lazy { ArrayMap<String, Any>().apply { KLog.i("params map :$this , code = ${this.hashCode()}") } }

    companion object {
        //        private val increment: AtomicInteger = AtomicInteger(0)
//        private val paramsMap = ArrayMap<Int, ParamHelper>()
        private val userMap by lazy { ArrayMap<String, String>(2) }

        @JvmStatic fun start(): ParamHelper {
//            val id = increment.addAndGet(Int.MIN_VALUE)
            return ParamHelper()/*.apply {
                paramsMap.put(id, this)
            }*/
        }

        /**
         * 获取登录用户参数
         * @return
         */
        //add account
        @Deprecated("may not use ")
        val userLoginMap: Map<String, String>
            get() = emptyMap()/*{
                return if (AppContext.account().isLogin) {
                    if (AppContext.account().account == userMap["USERNAME"]) userMap
                    else userMap.apply {
                        clear()
                        put("USERNAME", AppContext.account().account)
                        put("PASSWORD", AppContext.account().password)
                        KLog.i("add account success")
                    }
                } else emptyMap()
            }*/
    }
}
