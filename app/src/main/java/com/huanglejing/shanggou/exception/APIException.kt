package com.huanglejing.shanggou.exception

import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.common.AppContext

/**
 * Created by 75176 on 2016/7/24 0024.
 */
open class APIException : CFZXException {

    var code = -1
        private set

    constructor(message: String = "cause some error ", code: Int = -1, exception: Throwable = CFZXException()) : super(message, exception) {
        this.code = code
    }

}

class JOSNEmptyException(message: String = AppContext.get().resources.getString(R.string.str_no_return)) : APIException(message = message, code = 404)

class JsonParseException(message: String = AppContext.get().resources.getString(R.string.str_json_parse_incorrect)) : APIException(message = message, code = 1021)

class TokenExpireException(message: String, code: Int) : APIException(message = message, code = code)
