package com.huanglejing.shanggou.exception

/**
 * Created by Administrator on 2016/7/29 0029.
 */
open class CFZXException : RuntimeException {


    constructor() {
    }

    constructor(message: String = "some error", exception: Throwable = NotImplementedError()) : super(message, exception) {
    }

}
