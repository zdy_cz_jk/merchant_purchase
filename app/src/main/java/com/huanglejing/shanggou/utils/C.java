package com.huanglejing.shanggou.utils;

import android.os.Environment;


import com.huanglejing.shanggou.common.AppContext;

import java.io.File;

/**
 * Created by Administrator on 2017/8/16.
 */

public class C {
    public static class Cache {
        public static final String COMPANY_ID = "companyId";
        public static final String COMPANY_LIST = "companyList";
        public static final String GOODS_LIST = "goodsList";
        //SharedPreferences

        //cache path
        public static final String EX_APP_DIR = Environment
                .getExternalStorageDirectory()
                + File.separator
                + AppContext.get().getPackageName()
                + File.separator;

        public static final String EX_DOWNLOAD_DIR = EX_APP_DIR + "download" + File.separator;
        public static final String EX_TEMP_DIR = EX_APP_DIR + "temp" + File.separator;
        public static final String EX_CACHE_DIR = EX_APP_DIR + "cache" + File.separator;
        public static final String EX_IMAGE_DIR = EX_APP_DIR + "image" + File.separator;
        public static final String EX_IMAGE_DICM = Environment.getExternalStorageDirectory() + File.separator + "DCIM" + File.separator + "Camera" + File.separator + "cfzx" + File.separator;

        // 外存sdcard存放路径
        public static final String FILE_PATH = Environment.getExternalStorageDirectory() + "/" + "AutoUpdate" + "/";
        public static final String FILE_NAME = FILE_PATH + "AutoUpdate.apk";
    }

    public class SavedInstanceState {
        public final static String RECREATION_SAVED_STATE = "base_recreation_state";
        public final static String LOADER_ID_SAVED_STATE = "base_loader_id_state";
    }

    public static class Constants {
        public static final String EX_APP_DIR = Environment
                .getExternalStorageDirectory()
                + File.separator
                + AppContext.get().getPackageName()
                + File.separator;
        public final static String ENCOUNTER_SHARE = "to_meet_you_share";
        public final static String IS_FIRST = "is_first_in";
        public final static String EX_IMAGE_DIR = EX_APP_DIR + "image" + File.separator;
        public final static String SETTING_LOGIN_CHECK = "setting_login_check";
        public final static String MEET_DATA = "meet_data";
        public final static String ACCOUNT_BEAN = "account_bean";
        public static final String LOGIN_NEED_BACK = "login_need_back";
        public static final String USER_AVATAR_LOGIN_CHECK = "user_avatar_login_check";//更换头像
        public static final String OPERATION_LOGIN_CHECK = "operation_login_check";

        public static final String BASE_JUHE = "http://apis.juhe.cn/";
    }

    public class BundleKey {
        public final static String KEY1 = "meet_bundle_key1";
        public final static String KEY2 = "meet_bundle_key2";
        public final static String KEY3 = "meet_bundle_key3";
    }

    public class Page {
        public static final int PAGE_ROW = 10;
    }

    public class APIV1 {
        public static final String FINAL_URL = "";
        public static final String BASE_URL_OLD = "http://old.dadisoft.cn/";
        public static final String BASE_URL_NEW = "http://dadisoft.cn/";
        public static final String LOGIN = "dadi/customer/login";  //登陆
        public static final String COMPANY_LIST = "dadi/companyinfo/compnayList";  //可用公司列表
        public static final String GOODS_CATEGORY = "dadi/goods/category";  //获取商品分类
        public static final String GOODS_LIST = "dadi/goods/list";  //获取商品列表
        public static final String ORDER_ALL = "dadi/order/all";  //获取商品分类
        public static final String ADDRESS_LIST = "dadi/customer/address/list";  //收货地址列表
        public static final String ADD_ADDRESS = "dadi/customer/address/add";  //添加收获地址
        public static final String UPDATE_ADDRESS = "dadi/customer/address/update";  //添加收获地址
        public static final String DELETE_ADDRESS = "dadi/customer/address/delete";  //删除收获地址
        public static final String REGISTER = "dadi/customer/register";  //5.5.7	用户注册
        public static final String VALILD_YZM = "dadi/customer/vaildyzm";  //5.5.9	校验验证码
        public static final String SEND_YZM = "dadi/customer/sendyzm";  //5.5.7	用户注册
        public static final String LOGOUT = "dadi/customer/logout";  //5.5.7	用户注册
        public static final String GOODS_NEW = "dadi/goods/new";  //5.4.4	获取新到商品列表
        public static final String GOODS_HISTORY = "dadi/goods/histroy";  //5.4.4	获取历史订购商品列表
        public static final String GOODS_RECOMEND = "dadi/goods/recommend";  //5.4.5	获取推荐商品列表
        public static final String GOODS_PROMOTION = "dadi/goods/promotion";  //5.4.6	获取促销商品列表
        public static final String GOODS_SEARCH = "dadi/goods/search";  //5.4.3	搜索商品列表
        public static final String ORDER_GOODS = "dadi/order/goods";  //5.4.3	搜索商品列表
        public static final String ORDER_COMMIT = "dadi/order/commit";  //5.4.3	生成订单
        public static final String UPDATE = "dadi/soft/pc";  //获取PC端软件更新信息
        public static final String UPDATE_PWD = "dadi/customer/updatepassword";  //修改密码
        public static final String RESET_PWD = "dadi/customer/resetpassword";  //修改密码
        public static final String DELETE_SHOP = "dadi/companyinfo/deleteCompany";  //删除商家
        public static final String BIND_CUSTOMER = "dadi/companyinfo/bindCustomer";  //添加商家
    }

    public class Account {
        public static final String LOGIN_ACCOUNT = "login_account"; //缓存登录账户
    }

    public class Preference {
        public static final String SAHNGOU_DATA = "shanggouData"; //缓存登录账户
        public static final String CACHE_ACCOUNT_NAME = "accountName"; //缓存登录账户
        public static final String CACHE_ACCOUNT_NAME_NOZERO = "accountNamenozera"; //缓存登录账户
        public static final String CACHE_ACCOUNT_PWD = "acconutPwd"; //缓存登录账户密码
        public static final String CACHE_ACCOUNT_ID = "acconutId"; //缓存登录账户id
        public static final String CACHE_LANGUAGE = "language"; //缓存选中的语言
        public static final String CACHE_STOCK = "cacheStock"; //缓存是否显示库存
        public static final String CACHE_GOODS_NO = "cacheGoodsNo"; //缓存是否显示货号
    }
}
