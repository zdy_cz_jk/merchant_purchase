package com.huanglejing.shanggou.utils;

import android.app.Activity;
import android.text.TextUtils;


import com.huanglejing.mylibrary.KLog;
import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.bean.ShareBean;
import com.huanglejing.shanggou.common.AppContext;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMWeb;

import java.util.Map;

/**
 * Created by Administrator on 2016/11/29.
 */

public class UMHelper {
    private UMShareListener umShareListener;


    /**
     * 私有的构造函数
     */
    private UMHelper() {

    }

    public static UMHelper getInstance() {
        return SingletonHolder.instance;
    }

    /**
     * 友盟授权
     *
     * @param
     */
    public UMAuthListener getUMAuthListener() {
        UMAuthListener umAuthListener = new UMAuthListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {

            }

            @Override
            public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
                ToastUtils.toastLong("Authorize succeed");
            }

            @Override
            public void onError(SHARE_MEDIA platform, int action, Throwable t) {
                ToastUtils.toastLong("Authorize fail");
            }

            @Override
            public void onCancel(SHARE_MEDIA platform, int action) {
                ToastUtils.toastLong("Authorize cancel");
            }
        };

        return umAuthListener;
    }


    public void startShare(final Activity activity, SHARE_MEDIA platform, ShareBean bean) {
        umShareListener = new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {

            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                ToastUtils.toastLong(AppContext.get().getResources().getString(R.string.str_share_success));
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                KLog.d(throwable);
                ToastUtils.toastLong(AppContext.get().getResources().getString(R.string.str_share_failure));
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
                ToastUtils.toastLong(AppContext.get().getResources().getString(R.string.str_share_cancle));
            }
        };

        ShareAction shareAction = new ShareAction(activity);
        if (platform != null) {
            shareAction.setPlatform(platform);
        }
        UMWeb web = null;
        if (!TextUtils.isEmpty(bean.getTargetUrl())) {
            web = new UMWeb(bean.getTargetUrl()); //切记切记 这里分享的链接必须是http开头
        }

        if (!TextUtils.isEmpty(bean.getText())) {
            if (web != null) {
                web.setDescription(bean.getText());//描述
            } else {
                shareAction.withText(bean.getText());
            }
        }

        if (!TextUtils.isEmpty(bean.getTitle()) && web != null) {
            if (web != null) {
                web.setTitle(bean.getTitle());//标题
            } else {
                shareAction.withText(bean.getTitle());
            }
        }

        if (bean.getImage() != null && web != null) {
            if (web != null) {
                web.setThumb(bean.getImage());  //缩略图
            } else {
                shareAction.withMedia(bean.getImage());
            }
        }
        if (web != null) {
            shareAction.withMedia(web);
        }
        shareAction.setCallback(umShareListener).share();
    }


    private static class SingletonHolder {
        private static UMHelper instance = new UMHelper();
    }


}