package com.huanglejing.shanggou.utils;

import android.content.pm.PackageInfo;

import com.huanglejing.shanggou.common.AppContext;


/**
 * Created by Administrator on 2017/3/10.
 */

public class PackageHelper {

    public static PackageInfo getPackageInfo() {
        try {
            return AppContext.get().getPackageManager().getPackageInfo(
                    AppContext.get().getPackageName(), 0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getPackageName() {
        return AppContext.get().getPackageName();
    }


}
