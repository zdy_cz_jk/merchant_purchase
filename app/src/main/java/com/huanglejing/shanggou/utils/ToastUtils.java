package com.huanglejing.shanggou.utils;

import android.support.annotation.StringRes;
import android.widget.Toast;


import com.huanglejing.shanggou.common.AppContext;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;

/**
 * Created by Administrator on 2016/7/22 0022.
 */
public class ToastUtils {

    private static Toast toast;


    public static Toast get() {
        if (toast == null) {
            synchronized (ToastUtils.class) {
                toast = Toast.makeText(AppContext.get(), "", Toast.LENGTH_LONG);
            }
        } else {
        }
        return toast;
    }

    public static void toastLong(final String msg) {
        if (!ExtentionFuncsKt.checkEmpty(msg)) {
            get();
            AndroidSchedulers.mainThread().createWorker().schedule(new Action0() {
                @Override
                public void call() {
                    toast.setText(msg);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.show();
                }
            });

        }
    }

    public static void toastLong(@StringRes int msg) {
        get();
        toast.setText(msg);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }


}
