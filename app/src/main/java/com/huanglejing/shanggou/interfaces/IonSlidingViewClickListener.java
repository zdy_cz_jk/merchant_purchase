package com.huanglejing.shanggou.interfaces;

import android.view.View;

/**
 * 注册接口的方法：点击事件。在Mactivity.java实现这些方法。
 */
public interface IonSlidingViewClickListener {
    void onItemClick(View view, int position);//点击item正文

    void onDeleteBtnCilck(View view, int position);//点击“删除”

    void onSetBtnCilck(View view, int position);//点击“设置”
}


