package com.huanglejing.shanggou.interfaces

import com.google.gson.JsonObject
import com.huanglejing.shanggou.bean.CompanyListBean
import com.huanglejing.shanggou.bean.GoodsListBean
import com.huanglejing.shanggou.common.BasePresenter
import com.huanglejing.shanggou.common.BaseView
import com.huanglejing.shanggou.presenterimpl.BaseCacheRequestImpl
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.arrayMapOf
import rx.Observable

/**
 * Created by Administrator on 2016/11/4 0004.
 */

interface MainContract {
    interface MainView : BaseView {
    }

    interface MainPresenter<V> : BasePresenter<V> {
    }

}

interface RegisterContract {
    interface RegisterView : BaseView.BaseNetView {

    }

    interface RegisterPresenter<V> : BasePresenter<V> {

    }

}

interface ConfirmValiContract {
    interface ConfirmValiView : BaseView.BaseNetView {
      //  fun getYZMParams(): Observable<Map<String, Any>>
    }

    interface ConfirmValiPresenter<V> : BasePresenter<V> {
        fun requestValidateNumber(phone: String): Observable<JsonObject> = BaseCacheRequestImpl.getInstance(C.APIV1.SEND_YZM).requestForGet(arrayMapOf("username" to phone))
      //  fun register()
        fun confirmCode()
    }

}

interface PersonalCenterContract {
    interface PersonalCenterView : BaseView.BaseNetView {
    }

    interface PersonalCenterPresenter<V> : BasePresenter<V> {
        fun logout()
    }

}

interface ProductDetailContract {
    interface ProductDetailView : BaseView {
    }

    interface ProductDetailPresenter<V> : BasePresenter<V> {
    }

}

interface OrderDetailContract {
    interface OrderDetailView : BaseView.BaseNetView {
    }

    interface OrderDetailPresenter<V> : BasePresenter<V> {
        fun getOrderDetail()
    }

}

interface MineShipAddressContract {
    interface MineShipAddressView : BaseView.BaseNetView {
        fun getDeleteParams(): Observable<Map<String, Any>>
        fun showDeleteSuccess(t: Any)
    }

    interface MineShipAddressPresenter<V> : BasePresenter<V> {
        fun getAddressList()
        fun deleteAddress()
    }

}

interface AddAddressContract {
    interface AddAddressView : BaseView.BaseNetView {
    }

    interface AddAddressPresenter<V> : BasePresenter<V> {
        fun addAddress()
        fun updateAddress()
    }

}

interface AddSellerContract {
    interface AddSellerView : BaseView.BaseNetView{
    }

    interface AddSellerPresenter<V> : BasePresenter<V> {
        fun addSeller()
    }

}

interface SaveOrderContract {
    interface SaveOrderView : BaseView.BaseNetView {
        fun getOrderParams(): Observable<Map<String, Any>>

        fun showSaveResult(data: JsonObject)
    }

    interface SaveOrderPresenter<V> : BasePresenter<V> {
        fun getAddressList()
        fun saveOrder()
    }

}

interface MineSellersContract {
    interface MineSellersView : BaseView.BaseNetView {
        fun getDeleteParams(): Observable<Map<String, Any>>
        fun showDeleteSuccess(t: Any)
    }

    interface MineSellersPresenter<V> : BasePresenter<V> {
        fun getCompanyList()
        fun deleteSellers()
    }

}

interface NewComeProContract {
    interface NewComeProView : BaseView.BaseNetView {
    }

    interface NewComeProPresenter<V> : BasePresenter<V> {
        fun getNewComProList()
    }

}


interface RecomendProContract {
    interface RecomendProView : BaseView.BaseNetView {
    }

    interface RecomendProPresenter<V> : BasePresenter<V> {
        fun getRecomendProList()
    }

}

interface PromotionProContract {
    interface PromotionProView : BaseView.BaseNetView {
    }

    interface PromotionProPresenter<V> : BasePresenter<V> {
        fun getPromotionProList()
    }

}

interface HistoryContract {
    interface HistoryView : BaseView.BaseNetView {
    }

    interface HistoryPresenter<V> : BasePresenter<V> {
        fun getNewComProList()
    }

}

interface SearchContract {
    interface SearchView : BaseView.BaseNetView {
    }

    interface SearchPresenter<V> : BasePresenter<V> {
        fun getSearchList()
    }

}
interface SetPwdContract {
    interface SetPwdView : BaseView.BaseNetView {
        /**
         * 请求参数
         * @param page
         * *
         * @return
         */
        fun getRegisterParams(): Observable<Map<String, Any>>
    }

    interface SetPwdPresenter<V> : BasePresenter<V> {
        fun resetPwd()
        fun register()
    }

}

interface ModifyPwdContract {
    interface ModifyPwdView : BaseView.BaseNetView {
    }

    interface ModifyPwdPresenter<V> : BasePresenter<V> {
        fun updatePwd()
    }

}

interface FeedBackContract {
    interface FeedBackView : BaseView {
    }

    interface FeedBackPresenter<V> : BasePresenter<V> {
    }

}

interface ProductListContract {
    interface ProductListView : BaseView.BaseNetView {
        fun getProductListParams(): Observable<Map<String, Any>>
        fun showGoodsList(data: ArrayList<GoodsListBean>)
    }

    interface ProductListPresenter<V> : BasePresenter<V> {
        //   fun getGoodsCagory()
        fun getProductList()

    }

}

interface ProListDetailContract {
    interface ProListDetailView : BaseView.BaseNetView {
        fun getProListDetailParams(): Observable<Map<String, Any>>
        fun showGoodsList(data: ArrayList<GoodsListBean>)
    }

    interface ProListDetailPresenter<V> : BasePresenter<V> {
        //  fun getCompanyList()
        fun getProListDetail()

    }

}

interface MyFragmentContract {
    interface MyFragmentView : BaseView {

    }

    interface MyFragmentPresenter<V> : BasePresenter<V> {

    }

}

interface NowOrderContract {
    interface NowOrderView : BaseView {
        //    fun getCompanyListParams(): Observable<Map<String, Any>>
        fun showCompanyList(data: ArrayList<CompanyListBean>)
    }

    interface NowOrderPresenter<V> : BasePresenter<V> {
        //  fun getCompanyList()
        //  fun getNowOrder()
    }

}
interface SettingContract {
    interface SettingView : BaseView {
    }

    interface SettingPresenter<V> : BasePresenter<V> {
    }

}
interface MineOrderContract {
    interface MineOrderView : BaseView.BaseNetView {
        var isShowLoading: Boolean
    }

    interface MineOrderPresenter<V> : BasePresenter<V> {
        fun getNowOrder()
    }

}

interface ProductImgsContract {
    interface ProductImgsView : BaseView.BaseNetView {
        var isShowLoading: Boolean
    }

    interface ProductImgsPresenter<V> : BasePresenter<V> {
        fun getNetData()
    }

}

interface HomeFragmentContract {
    interface HomeFragmentView : BaseView.BaseNetView {

    }

    interface HomeFragmentPresenter<V> : BasePresenter<V> {
        fun getCompanyList()
    }

}


interface PersonalContract {
    interface PersonalView : BaseView {
        //   fun iniSettingItems(values: Array<SettingValues>)
    }

    interface PersonalPresenter<V> : BasePresenter<V> {

    }

}


interface LoginContract {
    interface LoginView : BaseView.BaseNetView {

    }

    interface LoginPresenter<V> : BasePresenter<V> {
        fun login()
    }
}

interface CountryListContract {
    interface CountryListView : BaseView {

    }

    interface CountryListPresenter<V> : BasePresenter<V> {
    }
}

interface LanguageListContract {
    interface LanguageListView : BaseView {

    }

    interface LanguageListPresenter<V> : BasePresenter<V> {
    }
}