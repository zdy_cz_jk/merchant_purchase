package com.huanglejing.shanggou.anim;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by Administrator on 2016/7/29 0029.
 */
public class FadeInOutPageTransformer implements ViewPager.PageTransformer {
    @Override
    public void transformPage(View page, float position) {
        if (position < -1) {//页码完全不可见
            page.setAlpha(0);
        } else if (position < 0) {
            page.setAlpha(1 + position);
        } else if (position < 1) {
            page.setAlpha(1 - position);
        } else {
            page.setAlpha(0);
        }
    }
}
