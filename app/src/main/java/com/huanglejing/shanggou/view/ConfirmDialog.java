package com.huanglejing.shanggou.view;

import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.huanglejing.shanggou.R;


/**
 * Created by chunhuiwang on 2016/8/3.
 */
public class ConfirmDialog extends Dialog {
    private Context context;
    private String title;
    private boolean isTwo;
    private ClickListenerInterface clickListenerInterface;

    public interface ClickListenerInterface {

        void doConfirm();

        void doCancel();
    }

    public ConfirmDialog(Context context, String msg, boolean isTwo, ClickListenerInterface clickListenerInterface) {
        super(context, R.style.customDialog);
        this.context = context;
        this.title = msg;
        this.clickListenerInterface = clickListenerInterface;
        this.isTwo = isTwo;
        init();
    }

    public ConfirmDialog setLeftText(String str){
        tvConfirm.setText(str);
        return this;
    }
    public ConfirmDialog setRightText(String str){
        tvCancel.setText(str);
        return this;
    }

    private TextView tvConfirm;
    private TextView tvCancel;

    public void init() {
        setCancelable(false);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.fragment_xiangce_dialog, null);
        setContentView(view);

        TextView text = (TextView) view.findViewById(R.id.text);
        tvConfirm = (TextView) view.findViewById(R.id.queren);
        tvCancel = (TextView) view.findViewById(R.id.quxiao);

        text.setText(title);

        if(!isTwo){
            tvCancel.setVisibility(View.GONE);
            view.findViewById(R.id.line).setVisibility(View.GONE);
        }

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListenerInterface != null){
                    clickListenerInterface.doConfirm();
                    dismiss();
                }
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListenerInterface != null){
                    clickListenerInterface.doCancel();
                    dismiss();
                }
            }
        });
        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.8); // 高度设置为屏幕的0.6
        dialogWindow.setAttributes(lp);

    }

    public void setClicklistener(ClickListenerInterface clickListenerInterface) {
        this.clickListenerInterface = clickListenerInterface;
    }

}
