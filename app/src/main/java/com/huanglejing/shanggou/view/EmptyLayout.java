package com.huanglejing.shanggou.view;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.activity.LoginActivity;
import com.huanglejing.shanggou.common.AppContext;
import com.huanglejing.shanggou.utils.NetworkUtil;
import com.huanglejing.shanggou.utils.UIHelper;


public class EmptyLayout extends LinearLayout implements
        View.OnClickListener {// , ISkinUIObserver {

    public static final int HIDE_LAYOUT = 4;
    public static final int NETWORK_ERROR = 1;
    public static final int NETWORK_LOADING = 2;
    public static final int NODATA = 3;
    public static final int NODATA_ENABLE_CLICK = 5;
    public static final int NO_LOGIN = 6;
    public static final int COMPLETE = 7;
    public ImageView img;
    public TextView tv;
    @DrawableRes
    private int imgId = R.drawable.empty_no_data;
    private int errorImgId = R.drawable.empty_error;
    private ProgressBar mLoading;
    private boolean clickEnable = true;
    private Context context;
    private OnClickListener listener;
    private int mErrorState;
    private String strNoDataContent = AppContext.get().getString(R.string.str_there_no_data_more);
    private LinearLayout linearLayout;
    private int orientation;

    public EmptyLayout(Context context) {
        this(context, LinearLayout.VERTICAL);
    }


    public EmptyLayout(Context context, int orientation) {
        super(context);
        this.context = context;
        this.orientation = orientation;
        init();
    }

    public EmptyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.orientation = LinearLayout.VERTICAL;
        init();
    }

    public static EmptyLayout getEmptyLayout(int state, ViewGroup parent) {
        return getEmptyLayout(state, VERTICAL, parent);
    }

    public static EmptyLayout getEmptyLayout(int state, int orientation, ViewGroup parent) {
        EmptyLayout emptyLayout = (EmptyLayout) LayoutInflater.from(AppContext.get()).inflate(R.layout.layout_empty, parent, false);
        emptyLayout.setViewOriention(orientation);
        emptyLayout.setErrorType(state);
        if (orientation == LinearLayout.HORIZONTAL && state == NETWORK_LOADING && emptyLayout.getLayoutParams() != null) {
            emptyLayout.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        }
        return emptyLayout;
    }

    private void init() {
        View view = LayoutInflater.from(context).inflate(R.layout.view_error_layout, null);
        linearLayout = view.findViewById(R.id.ll_orientation);
        linearLayout.setOrientation(orientation);
        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        img = view.findViewById(R.id.img_error_layout);
        tv = view.findViewById(R.id.tv_error_layout);
        mLoading = view.findViewById(R.id.animProgress);
//        setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
        setOnClickListener(this);
        img.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (clickEnable) {
                    // setErrorType(NETWORK_LOADING);
                    if (listener != null)
                        listener.onClick(v);
                }
            }
        });
        addView(view);
        changeErrorLayoutBgMode(context);
    }

    public void changeErrorLayoutBgMode(Context context1) {
        // mLayout.setBackgroundColor(SkinsUtil.getColor(context1,
        // "bgcolor01"));
        // tv.setTextColor(SkinsUtil.getColor(context1, "textcolor05"));
    }

    public void dismiss() {
        mErrorState = HIDE_LAYOUT;
        setVisibility(View.GONE);
    }

    public int getErrorState() {
        return mErrorState;
    }

    public boolean isLoadError() {
        return mErrorState == NETWORK_ERROR;
    }

    public boolean isLoading() {
        return mErrorState == NETWORK_LOADING;
    }

    @Override
    public void onClick(View v) {
        if (clickEnable) {
            // setErrorType(NETWORK_LOADING);
            if (listener != null)
                listener.onClick(v);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        // MyApplication.getInstance().getAtSkinObserable().registered(this);
        onSkinChanged();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        // MyApplication.getInstance().getAtSkinObserable().unregistered(this);
    }

    public void onSkinChanged() {
        // mLayout.setBackgroundColor(SkinsUtil
        // .getColor(getContext(), "bgcolor01"));
        // tv.setTextColor(SkinsUtil.getColor(getContext(), "textcolor05"));
    }

    public void setErrorMessage(String msg) {
        tv.setText(msg);
    }

    /**
     * 新添设置背景
     *
     * @author 火蚁 2015-1-27 下午2:14:00
     */
    public void setErrorImag(@DrawableRes int imgResource) {
        imgId = imgResource;
        img.setImageResource(imgResource);
    }

    public void setErrorType(int i) {
        setVisibility(View.VISIBLE);
        linearLayout.setOrientation(orientation);
        switch (i) {
            case NETWORK_ERROR:
                mErrorState = NETWORK_ERROR;
                // img.setBackgroundDrawable(SkinsUtil.getDrawable(context,"pagefailed_bg"));
                if (NetworkUtil.isNetAvailable(AppContext.get())) {
                    tv.setText(R.string.error_view_load_error_click_to_refresh);
                    img.setImageResource(errorImgId);
                } else {
                    tv.setText(R.string.error_view_network_error_click_to_refresh);
                    img.setImageResource(R.mipmap.ic_network_error);
                }
                img.setVisibility(View.VISIBLE);
                mLoading.setVisibility(View.GONE);
                clickEnable = true;
                break;
            case NETWORK_LOADING:
                mErrorState = NETWORK_LOADING;
                // mLoading.setBackgroundDrawable(SkinsUtil.getDrawable(context,"loadingpage_bg"));
                mLoading.setVisibility(View.VISIBLE);
                img.setVisibility(View.GONE);
                tv.setText(R.string.error_view_loading);
                clickEnable = false;
                break;
            case NODATA:
                mErrorState = NODATA;
                // img.setBackgroundDrawable(SkinsUtil.getDrawable(context,"page_icon_empty"));
                img.setImageResource(imgId);
                img.setVisibility(View.VISIBLE);
                mLoading.setVisibility(View.GONE);
                setTvNoDataContent();
                clickEnable = true;
                break;
            case HIDE_LAYOUT:
                setVisibility(View.GONE);
                break;
            case NODATA_ENABLE_CLICK:
                mErrorState = NODATA_ENABLE_CLICK;
                img.setImageResource(imgId);
                // img.setBackgroundDrawable(SkinsUtil.getDrawable(context,"page_icon_empty"));
                img.setVisibility(View.VISIBLE);
                mLoading.setVisibility(View.GONE);
                setTvNoDataContent();
                tv.append(AppContext.get().getResources().getString(R.string.str_do_again));
                clickEnable = true;
                break;
            case COMPLETE:
                mErrorState = COMPLETE;
                img.setImageResource(imgId);
                img.setVisibility(View.GONE);
                mLoading.setVisibility(View.GONE);
                tv.setText(R.string.str_there_no_data_more);
                clickEnable = false;
                break;

            case NO_LOGIN:
                mErrorState = NO_LOGIN;
                // img.setBackgroundDrawable(SkinsUtil.getDrawable(context,"page_icon_empty"));
                img.setImageResource(imgId);
                img.setVisibility(View.VISIBLE);
                mLoading.setVisibility(View.GONE);
                tv.setText(R.string.not_login);
                listener = new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UIHelper.Companion.startActivity(LoginActivity.class);
                    }
                };
                clickEnable = true;
                break;
            default:
                break;
        }
    }

    public void setNoDataContent(String noDataContent) {
        strNoDataContent = noDataContent;
    }

    public void setOnLayoutClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public void setTvNoDataContent() {
        tv.setText(strNoDataContent);
    }

    @Override
    public void setVisibility(int visibility) {
        if (visibility == View.GONE)
            mErrorState = HIDE_LAYOUT;
        super.setVisibility(visibility);
    }

    public void setViewOriention(int orientation) {
        this.orientation = orientation;
        linearLayout.setOrientation(orientation);
    }

}
