/*
 * Copyright (C) 2015 tyrantgit
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.huanglejing.shanggou.view.FloatingTextView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;


public class RxTextView extends android.support.v7.widget.AppCompatTextView {

    private static final Paint sPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
    private static final Canvas sCanvas = new Canvas();
//    private static Bitmap sHeartBorder;
//    private int mHeartBorderResId = R.drawable.anim_heart_border;

    public RxTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RxTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public RxTextView(Context context) {
        super(context);
    }

    private static Bitmap createBitmapSafely(int width, int height) {
        try {
            return Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError error) {
            error.printStackTrace();
        }
        return null;
    }

    public void setColor(int color) {
        createHeart(color);
        setTextColor(color);
        //    setImageDrawable(new BitmapDrawable(getResources(), heart));
    }

    public void setColorAndText(int color, String num) {
        createHeartAndText(color,num);
        setTextColor(color);
        //    setImageDrawable(new BitmapDrawable(getResources(), heart));
    }

    public void setColorAndDrawables(int color, int heartResId, int heartBorderResId) {

//        if (heartBorderResId != mHeartBorderResId) {
//            sHeartBorder = null;
//        }
//        mHeartBorderResId = heartBorderResId;
        setColor(color);
    }

    public Bitmap createHeart(int color) {
//        if (sHeartBorder == null) {
//            sHeartBorder = BitmapFactory.decodeResource(getResources(), mHeartBorderResId);
//        }
        setText("+ 10");
        setDrawingCacheEnabled(true);
        measure(80, 80);
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(getDrawingCache());       //千万别忘最后一步
        //    tv.destroyDrawingCache();

        Bitmap heartBorder = bitmap;
        Bitmap bm = createBitmapSafely(heartBorder.getWidth(), heartBorder.getHeight());
        if (bm == null) {
            return null;
        }
        Canvas canvas = sCanvas;
        canvas.setBitmap(bm);
        Paint p = sPaint;
        p.setColor(Color.parseColor("#ff4455"));
        //  p.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
        canvas.drawBitmap(heartBorder, 0, 0, p);
        p.setColorFilter(null);
        canvas.setBitmap(null);
        return bm;
    }

    public Bitmap createHeartAndText(int color, String num) {
//        if (sHeartBorder == null) {
//            sHeartBorder = BitmapFactory.decodeResource(getResources(), mHeartBorderResId);
//        }
        setText(num);
        setTextSize(TypedValue.COMPLEX_UNIT_SP,10);
        setDrawingCacheEnabled(true);
        measure(80, 80);
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(getDrawingCache());       //千万别忘最后一步
        //    tv.destroyDrawingCache();

        Bitmap heartBorder = bitmap;
        Bitmap bm = createBitmapSafely(heartBorder.getWidth(), heartBorder.getHeight());
        if (bm == null) {
            return null;
        }
        Canvas canvas = sCanvas;
        canvas.setBitmap(bm);
        Paint p = sPaint;
        p.setColor(Color.parseColor("#ff4455"));
        //  p.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
        canvas.drawBitmap(heartBorder, 0, 0, p);
        p.setColorFilter(null);
        canvas.setBitmap(null);
        return bm;
    }

}
