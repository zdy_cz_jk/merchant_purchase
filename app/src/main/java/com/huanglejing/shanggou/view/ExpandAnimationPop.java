package com.huanglejing.shanggou.view;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.PopupWindow;

import com.huanglejing.shanggou.R;


/**
 * Created by zhudong on 2018/8/6.
 */

/***
 * 带扩展和收缩动画的popwindow
 */
public class ExpandAnimationPop extends PopupWindow {
    private Context mContext;

    public ExpandAnimationPop(Context context) {
        super(context);
        this.mContext = context;
    }


    @Override
    public void dismiss() {
        if(ExpandAnimationPop.this.isShowing()){
            Animation downAnim = AnimationUtils.loadAnimation(mContext, R.anim.bottom_tran_to_down);
            downAnim.setInterpolator(new DecelerateInterpolator());
            downAnim.setRepeatCount(0);
            downAnim.setRepeatMode(Animation.RELATIVE_TO_SELF);
            downAnim.setFillAfter(true);
            downAnim.setFillBefore(false);
            downAnim.setFillEnabled(true);
            ExpandAnimationPop.this.getContentView().startAnimation(downAnim);
            downAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    doDismiss();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }


    }


    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff, int gravity) {
//        Animation upAnim = AnimationUtils.loadAnimation(mContext, R.anim.bottom_tran_to_up);
//        upAnim.setInterpolator(new DecelerateInterpolator());
//        upAnim.setFillAfter(true);
//        upAnim.setFillBefore(false);
//        upAnim.setFillEnabled(true);
//        ExpandAnimationPop.this.getContentView().startAnimation(upAnim);
        super.showAsDropDown(anchor, xoff, yoff, gravity);
    }

    private void doDismiss() {
        super.dismiss();
    }
}
