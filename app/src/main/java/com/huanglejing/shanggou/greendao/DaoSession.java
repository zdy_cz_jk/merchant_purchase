package com.huanglejing.shanggou.greendao;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.huanglejing.shanggou.greendao.CacheGoodsBean;
import com.huanglejing.shanggou.greendao.CacheOrderBean;

import com.huanglejing.shanggou.greendao.CacheGoodsBeanDao;
import com.huanglejing.shanggou.greendao.CacheOrderBeanDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig cacheGoodsBeanDaoConfig;
    private final DaoConfig cacheOrderBeanDaoConfig;

    private final CacheGoodsBeanDao cacheGoodsBeanDao;
    private final CacheOrderBeanDao cacheOrderBeanDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        cacheGoodsBeanDaoConfig = daoConfigMap.get(CacheGoodsBeanDao.class).clone();
        cacheGoodsBeanDaoConfig.initIdentityScope(type);

        cacheOrderBeanDaoConfig = daoConfigMap.get(CacheOrderBeanDao.class).clone();
        cacheOrderBeanDaoConfig.initIdentityScope(type);

        cacheGoodsBeanDao = new CacheGoodsBeanDao(cacheGoodsBeanDaoConfig, this);
        cacheOrderBeanDao = new CacheOrderBeanDao(cacheOrderBeanDaoConfig, this);

        registerDao(CacheGoodsBean.class, cacheGoodsBeanDao);
        registerDao(CacheOrderBean.class, cacheOrderBeanDao);
    }
    
    public void clear() {
        cacheGoodsBeanDaoConfig.clearIdentityScope();
        cacheOrderBeanDaoConfig.clearIdentityScope();
    }

    public CacheGoodsBeanDao getCacheGoodsBeanDao() {
        return cacheGoodsBeanDao;
    }

    public CacheOrderBeanDao getCacheOrderBeanDao() {
        return cacheOrderBeanDao;
    }

}
