package com.huanglejing.shanggou.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class CacheGoodsBean {
    @Id
    private Long id;
    @Property(nameInDb = "companyId")
    private String companyId;
    @Property(nameInDb = "customerId")
    private String customerId;
    @Property(nameInDb = "bz")
    private String bz;
    @Property(nameInDb = "categoryGUID")
    private String categoryGUID;
    @Property(nameInDb = "bagCount")
    private String bagCount;
    @Property(nameInDb = "boxCount")
    private String boxCount;
    @Property(nameInDb = "count")
    private String count;
    @Property(nameInDb = "goodNO")
    private String goodNO;
    @Property(nameInDb = "name")
    private String name;
    @Property(nameInDb = "no")
    private String no;
    @Property(nameInDb = "parentGuid")
    private String parentGuid;
    @Property(nameInDb = "price")
    private String price;
    @Property(nameInDb = "url")
    private String url;
    @Property(nameInDb = "xname")
    private String xname;
    @Property(nameInDb = "zk")
    private String zk;
    @Property(nameInDb = "num")
    private String num;
    @Property(nameInDb = "totalPrice")
    private String totalPrice;


    @Generated(hash = 482784393)
    public CacheGoodsBean(Long id, String companyId, String customerId, String bz,
            String categoryGUID, String bagCount, String boxCount, String count,
            String goodNO, String name, String no, String parentGuid, String price,
            String url, String xname, String zk, String num, String totalPrice) {
        this.id = id;
        this.companyId = companyId;
        this.customerId = customerId;
        this.bz = bz;
        this.categoryGUID = categoryGUID;
        this.bagCount = bagCount;
        this.boxCount = boxCount;
        this.count = count;
        this.goodNO = goodNO;
        this.name = name;
        this.no = no;
        this.parentGuid = parentGuid;
        this.price = price;
        this.url = url;
        this.xname = xname;
        this.zk = zk;
        this.num = num;
        this.totalPrice = totalPrice;
    }
    @Generated(hash = 974082114)
    public CacheGoodsBean() {
    }


    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCompanyId() {
        return this.companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    public String getBz() {
        return this.bz;
    }
    public void setBz(String bz) {
        this.bz = bz;
    }
    public String getCategoryGUID() {
        return this.categoryGUID;
    }
    public void setCategoryGUID(String categoryGUID) {
        this.categoryGUID = categoryGUID;
    }
    public String getBagCount() {
        return this.bagCount;
    }
    public void setBagCount(String bagCount) {
        this.bagCount = bagCount;
    }
    public String getBoxCount() {
        return this.boxCount;
    }
    public void setBoxCount(String boxCount) {
        this.boxCount = boxCount;
    }
    public String getCount() {
        return this.count;
    }
    public void setCount(String count) {
        this.count = count;
    }
    public String getGoodNO() {
        return this.goodNO;
    }
    public void setGoodNO(String goodNO) {
        this.goodNO = goodNO;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNo() {
        return this.no;
    }
    public void setNo(String no) {
        this.no = no;
    }
    public String getParentGuid() {
        return this.parentGuid;
    }
    public void setParentGuid(String parentGuid) {
        this.parentGuid = parentGuid;
    }
    public String getPrice() {
        return this.price;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getXname() {
        return this.xname;
    }
    public void setXname(String xname) {
        this.xname = xname;
    }
    public String getZk() {
        return this.zk;
    }
    public void setZk(String zk) {
        this.zk = zk;
    }
    public String getNum() {
        return this.num;
    }
    public void setNum(String num) {
        this.num = num;
    }
    public String getTotalPrice() {
        return this.totalPrice;
    }
    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
}
