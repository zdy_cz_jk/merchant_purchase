package com.huanglejing.shanggou.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.io.Serializable;
import java.util.ArrayList;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class CacheOrderBean {
    @Id
    private Long id;
    @Property(nameInDb = "companyId")
    private String companyId;
    @Property(nameInDb = "customerId")
    private String customerId;
    @Property(nameInDb = "bz")
    private String bz;
    @Property(nameInDb = "goods")
    //"goods":[
    // {"categoryGUID":"设置为空字符","goodNO":"产品ID","num":数量字符型,"price":"单价","totalPrice":"合价"},
    // {"categoryGUID":"设置为空字符","goodNO":"产品ID","num":数量字符型,"price":"单价","totalPrice":"合价"}
    // ]
    //此处需将子条目的数据库的id以，连接存储
    private String goods;

    @Property(nameInDb = "companyName")
    private String companyName;
    @Property(nameInDb = "companyAddress")
    private String companyAddress;
    @Property(nameInDb = "postalCode")
    private String postalCode;
    @Property(nameInDb = "city")
    private String city;
    @Property(nameInDb = "province")
    private String province;
    @Property(nameInDb = "linkTelephone")
    private String linkTelephone;



    @Generated(hash = 2079107916)
    public CacheOrderBean(Long id, String companyId, String customerId, String bz,
            String goods, String companyName, String companyAddress, String postalCode,
            String city, String province, String linkTelephone) {
        this.id = id;
        this.companyId = companyId;
        this.customerId = customerId;
        this.bz = bz;
        this.goods = goods;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.postalCode = postalCode;
        this.city = city;
        this.province = province;
        this.linkTelephone = linkTelephone;
    }
    @Generated(hash = 547761444)
    public CacheOrderBean() {
    }



    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCompanyId() {
        return this.companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    public String getBz() {
        return this.bz;
    }
    public void setBz(String bz) {
        this.bz = bz;
    }
    public String getGoods() {
        return this.goods;
    }
    public void setGoods(String goods) {
        this.goods = goods;
    }
    public String getCompanyName() {
        return this.companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getCompanyAddress() {
        return this.companyAddress;
    }
    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }
    public String getPostalCode() {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    public String getCity() {
        return this.city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getProvince() {
        return this.province;
    }
    public void setProvince(String province) {
        this.province = province;
    }
    public String getLinkTelephone() {
        return this.linkTelephone;
    }
    public void setLinkTelephone(String linkTelephone) {
        this.linkTelephone = linkTelephone;
    }



}
