package com.huanglejing.shanggou.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.huanglejing.mylibrary.KLog;
import com.huanglejing.shanggou.rx.RxManager;


/**
 * Created by Administrator on 2016/8/11 0011.
 */
public class BaseFragment extends Fragment {
    protected String TAG;
    protected BaseActivity activity;
    protected RxManager rxManager;
    private boolean isTitleVisible =true;
    private boolean isIvExitVisible =true;
    private Integer containerId;

    public Integer getContainerId() {
        return containerId;
    }

    public void setContainerId(int containerId) {
        this.containerId = containerId;
    }
    public boolean isTitleVisible() {
        return isTitleVisible;
    }

    public void setTitleVisible(boolean titleVisible) {
        isTitleVisible = titleVisible;
    }

    public boolean isIvExitVisible() {
        return this.isIvExitVisible;
    }

    public void setIvExitVisible(boolean ivExitVisible) {
        this.isIvExitVisible = ivExitVisible;
    }

    @Override
    public void onAttach(Activity activity) {
        TAG = this.getClass().getSimpleName();
        KLog.w(TAG, TAG + " : execute ...activity =" + activity);
        super.onAttach(activity);
        this.activity = (BaseActivity) activity;
    }

    @Override
    public void onAttach(Context context) {
        KLog.w(TAG, TAG + " : execute ... context :" + context);
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        KLog.w(TAG, TAG + " : execute ...");
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        KLog.w(TAG, TAG + " : execute ...");
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        KLog.w(TAG, TAG + " : execute ...");
        rxManager = new RxManager();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        KLog.w(TAG, TAG + " : execute ...");
        super.onViewCreated(view, savedInstanceState);
    }

    protected void setAPPTitle(String title) {
        try {
            ;
            if (getActivity() == null) {
                KLog.d("getActivity is null");
                return;
            }
            if ((BaseActivity) getActivity() == null) {
                KLog.d("getActivity  is not BaseActivity");
                return;
            }
            if (((BaseActivity) getActivity()).getmTvTitle() == null) {
                KLog.d("title view is null ");
                return;
            }
            if (!TextUtils.isEmpty(title)) {
                ((BaseActivity) getActivity()).getmTvTitle().setText(title);
            }
        } catch (Exception e) {

        }

    }


    protected void setTitleVisible() {
        if (getActivity()==null){
            return;
        }
        if ((BaseActivity) getActivity()==null){
            return;
        }
        if (((BaseActivity) getActivity()).getmTvTitle() == null) {
            return;
        }
        if (isTitleVisible){
            ((BaseActivity) getActivity()).getmTvTitle().setVisibility(View.VISIBLE);
        }else {
            ((BaseActivity) getActivity()).getmTvTitle().setVisibility(View.GONE);
        }

    }
    protected void setIvExitShow() {
        if (getActivity()==null){
            return;
        }
        if ((BaseActivity) getActivity()==null){
            return;
        }
        if (((BaseActivity) getActivity()).getmIvExit() == null) {
            return;
        }
        if (isIvExitVisible()){
            ((BaseActivity) getActivity()).getmIvExit().setVisibility(View.VISIBLE);
        }else {
            ((BaseActivity) getActivity()).getmIvExit().setVisibility(View.GONE);
        }
    }
    @Override
    public void onStart() {
        KLog.w(TAG, TAG + " : execute ...");
        super.onStart();
    }

    @Override
    public void onResume() {
        KLog.w(TAG, TAG + " : execute ...");
        super.onResume();
      //  MobclickAgent.onPageStart(TAG);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        KLog.w(TAG, TAG + " : execute ... onHiddenChanged =" + hidden);
        super.onHiddenChanged(hidden);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        KLog.w(TAG, TAG + " : execute ... isVisibleToUser =" + isVisibleToUser);
        super.setUserVisibleHint(isVisibleToUser);
     if (isVisibleToUser){
         setTitleVisible();
         setIvExitShow();
     }
    }

    @Override
    public void onPause() {
        KLog.w(TAG, TAG + " : execute ...");
        super.onPause();
        //  MobclickAgent.onPageEnd(TAG);
    }

    @Override
    public void onStop() {
        KLog.w(TAG, TAG + " : execute ...");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        KLog.w(TAG, TAG + " : execute ...");
        super.onDestroyView();
        AppContext.getRefWatcher(getViewContext()).watch(this);
    }

    @Override
    public void onDestroy() {
        KLog.w(TAG, TAG + " : execute ...");
        //  AppContext.get().getRefWatcher().watch(this);
        super.onDestroy();
        rxManager.clear();
        rxManager.destory();
    }

    @Override
    public void onDetach() {
        KLog.w(TAG, TAG + " : execute ...");
        super.onDetach();
        this.activity = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        KLog.w(TAG, TAG + " : execute ...");
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        KLog.w(TAG, TAG + " : execute ...");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onLowMemory() {
        KLog.w(TAG, TAG + " : execute ...");
        super.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        KLog.w(TAG, TAG + " : execute ...");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        KLog.w(TAG, TAG + " : execute ...");
        super.onViewStateRestored(savedInstanceState);
    }

    //region other
    @NonNull
    public Context getViewContext() {
        if (activity == null) return AppContext.get();
        return activity;
    }
    //endregion
}
