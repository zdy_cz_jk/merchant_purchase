package com.huanglejing.shanggou.common


import android.content.Context
import android.text.TextUtils
import com.google.gson.JsonObject
import rx.Observable
import java.util.*

/**
 * Created by Administrator on 2016/7/21 0021.
 */
interface BaseModel<in T, R> {
    //默认请求
    fun requestFor(t: T): Observable<R> {
        return Observable.empty()
    }
    //默认请求
    fun requestForGet(t: T): Observable<R> {
        return Observable.empty()
    }


    //默认请求
    fun requestFor(): Observable<R> {
        return Observable.empty()
    }

    //默认请求
    fun requestForGet(): Observable<R> {
        return Observable.empty()
    }


    fun requestWithHeaders(t: T, headers: Map<String, String>): Observable<R> = Observable.empty()

    fun abort() {
        //废弃请求,如果是Retrofit请求，只需要unsubscribe即可，无需实现该方法
        TODO()
    }


    interface BaseCacheModel<in T, R> : BaseModel<T, R> {
        fun requestForCache(t: T): Observable<R> //默认请求
        fun requestForGetCache(t: T): Observable<R> //默认请求
        fun abortLruCache() {
            TODO()
        }
    }


    interface LoginModel<in T, R> : BaseModel<T, R> {
        fun backGroundLogin(t: Map<String, String>): Observable<JsonObject>
    }







}
