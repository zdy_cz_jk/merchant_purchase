package com.huanglejing.shanggou.common

import android.content.Context
import android.content.DialogInterface
import com.afollestad.materialdialogs.MaterialDialog
import com.huanglejing.shanggou.R

import rx.Observable
import java.lang.ref.WeakReference

/**
 * Created by Administrator on 2016/7/21 0021.
 */
interface BaseView {

    companion object {
        var place_holder_loading: WeakReference<MaterialDialog?>? = null
    }

    fun getViewContext(): Context

    fun showLoading(): Unit {
        place_holder_loading = WeakReference(MaterialDialog.Builder(getViewContext()).content(R.string.loading).cancelable(true).progress(true, 0).show())
    }

    fun dismissLoading(): Unit {
        place_holder_loading?.let {
            it?.get()?.dismiss()
            it?.clear()
        }
        place_holder_loading = null
    }

    fun <T> showContent(data: T?): Unit = TODO(" no impl")

    fun showError(e: Throwable?): Unit = TODO(" no impl")

    interface BaseListView : BaseView {
        fun <T> showContents(datas: List<T>?)

        /**
         * @param end 是否結束
         */
        fun loadComplete(end: Boolean = false)

    }

    interface BaseNetView : BaseView {


        /**
         * 请求参数
         * @param page
         * *
         * @return
         */
        fun getRequestParams(): Observable<Map<String, Any>>





    }


}
