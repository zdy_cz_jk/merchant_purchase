package com.huanglejing.shanggou.common

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.BottomSheetDialogFragment
import android.view.View
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.rx.RxManager


/**
 * Created by Administrator on 2017/6/2 0002.
 */
abstract class BaseBottomSheetFragment : BottomSheetDialogFragment() {

    abstract val mRootView: View
    protected val mBehavior by lazy { BottomSheetBehavior.from(mRootView.parent as View) }
    protected val rxManager by lazy { RxManager() }

    final override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(activity!!).apply {
            setContentView(mRootView)
            initWidget()
            KLog.d("behavior :$mBehavior")
            mRootView.post {
                /**
                 * PeekHeight默认高度256dp 会在该高度上悬浮
                 * 设置等于view的高 就不会卡住
                 */
                /**
                 * PeekHeight默认高度256dp 会在该高度上悬浮
                 * 设置等于view的高 就不会卡住
                 */
                mBehavior.peekHeight = mRootView.height
            }
        }
    }

    abstract fun initWidget()
    override fun onDestroy() {
        super.onDestroy()
        rxManager.clear()
    }
}