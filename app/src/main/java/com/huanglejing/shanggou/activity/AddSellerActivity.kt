package com.huanglejing.shanggou.activity

import android.os.Bundle
import android.support.v4.util.ArrayMap
import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.data.CacheCustomerId
import com.huanglejing.shanggou.interfaces.AddSellerContract.AddSellerPresenter
import com.huanglejing.shanggou.interfaces.AddSellerContract.AddSellerView
import com.huanglejing.shanggou.presenterimpl.AddSellersImpl
import com.huanglejing.shanggou.rx.event.CompanyRefreshEvent
import com.huanglejing.shanggou.rx.rxbus.RxBus
import com.huanglejing.shanggou.utils.ToastUtils
import com.huanglejing.shanggou.utils.checkEmpty
import kotlinx.android.synthetic.main.activity_add_seller.*
import rx.Observable

class AddSellerActivity : AppBaseActivity<AddSellerPresenter<AddSellerView>, AddSellerView>(), AddSellerView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initWidget()
    }

    override fun getRequestParams(): Observable<Map<String, Any>> {
        return Observable.just(ArrayMap<String, Any>().apply {
            put("customerId", CacheCustomerId)
            put("companyCode", et_companyCode?.text?.toString() ?: "")
            put("customerCode", et_customerCode?.text?.toString() ?: "")
        })
    }

    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_add_shops))
        bt_add_seller?.setOnClickListener {
            if (et_companyCode.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_code_not_null))
                return@setOnClickListener
            }
            if (et_customerCode.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_pin_not_null))
                return@setOnClickListener
            }
            mBasePresenter?.addSeller()
        }

    }

    override fun <T> showContent(data: T?) {
        if (data is JsonObject) {

            if (data?.get("status")?.asString?.equals("00") ?: false) {
                ToastUtils.toastLong(data?.get("message")?.asString
                        ?: resources.getString(R.string.str_add_seller_success))
                RxBus.get().postEvent(CompanyRefreshEvent())
                finish()
            } else {
                ToastUtils.toastLong(data?.get("message")?.asString
                        ?: resources.getString(R.string.str_add_seller_fail))
            }
        }

    }

    override fun showError(e: Throwable?) {
        KLog.e(e?.message)
    }

    override fun createPresenter(): AddSellerPresenter<AddSellerView> = AddSellersImpl()

    override fun getLayoutId(): Int = R.layout.activity_add_seller

    override fun requestPermission() {
    }
}
