package com.huanglejing.shanggou.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.adapter.NowOrderAdapter
import com.huanglejing.shanggou.bean.CompanyListBean
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.data.*
import com.huanglejing.shanggou.greendao.CacheGoodsBean
import com.huanglejing.shanggou.greendao.CacheGoodsBeanDao
import com.huanglejing.shanggou.greendao.CacheOrderBean
import com.huanglejing.shanggou.greendao.CacheOrderBeanDao
import com.huanglejing.shanggou.interfaces.IonSlidingViewClickListener
import com.huanglejing.shanggou.interfaces.NowOrderContract
import com.huanglejing.shanggou.presenterimpl.NowOrderImpl
import com.huanglejing.shanggou.utils.UIHelper
import com.huanglejing.shanggou.utils.checkEmpty
import com.huanglejing.shanggou.utils.formatNum
import com.huanglejing.shanggou.view.EmptyLayout
import com.rtdl.dropdownmenu.NormalMenuHolder
import kotlinx.android.synthetic.main.activity_current_order.*
import kotlinx.android.synthetic.main.activity_new_com_pro.*
import kotlinx.android.synthetic.main.simple_recycle_view.*

class CurrentOrderActivity : AppBaseActivity<NowOrderContract.NowOrderPresenter<NowOrderContract.NowOrderView>, NowOrderContract.NowOrderView>()
        , NowOrderContract.NowOrderView, IonSlidingViewClickListener {
    var requestCode = 0
    var mData = arrayListOf<CacheGoodsBean>()
    var companyId = ""
    var allPrice = 0.00
    var leftPosition = 0
    var myAdapter: NowOrderAdapter? = null
    val menusData = arrayListOf<CompanyListBean>()
    protected var mEmptyLayout: EmptyLayout? = null
    private var loadAll: List<CacheGoodsBean>? = null
    private val menu by lazy {
        NormalMenuHolder(this@CurrentOrderActivity, getAllPrice()) { view, i ->
            leftPosition = i
            allPrice = getAllPrice()?.getOrNull(i)?.toDoubleOrNull() ?: 0.00
            companyId = menusData?.getOrNull(i)?.companyId?.toString() ?: ""
            loadAll = getCacheGoods()
            showContent(loadAll)
            //   myAdapter?.notifyDataSetChanged()
        }
    }
//
//    override fun getCompanyListParams(): Observable<Map<String, Any>> {
//        val map = ArrayMap<String, String>().apply {
//            put("customerId", CacheCustomerId ?: "")
//        }
//        return Observable.just(map)
//    }

    override fun showCompanyList(data: ArrayList<CompanyListBean>) {
        if (data.checkEmpty()) {
            return
        }
        menusData?.clear()
        menusData?.addAll(data)
        data?.forEachIndexed { index, companyListBean ->
            if (companyListBean?.companyId?.equals(companyId) ?: false) {
                leftPosition = index
            }
        }

        if (companyId.checkEmpty()) {
            companyId = data?.getOrNull(0)?.companyId?.toString() ?: ""
        }
        ll_current_order_container?.removeAllViews()
        ll_current_order_container?.addView(menu.dropMenu)
        menu.isAutoRefresh = false
        menu.setMenuListData((data?.map { it?.companyDisplayName ?: "" } as? ArrayList)
                ?: arrayListOf<String>())
        menu?.outsideMenuClick(leftPosition, 0)
        allPrice = getAllPrice()?.get(leftPosition)?.toDoubleOrNull() ?: 0.00
        //    mBasePresenter?.getNowOrder()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        companyId = AppContext.get().companyId ?: ""
        requestCode = intent?.getIntExtra("", 300) ?: 300
        initWidget()
    }

    fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_current_order))
        val manager = LinearLayoutManager(this@CurrentOrderActivity, LinearLayoutManager.VERTICAL, false)
        simple_recycle_view?.layoutManager = manager

        myAdapter = NowOrderAdapter()
        myAdapter?.setIonSlidingViewClickListener(this@CurrentOrderActivity)
        simple_recycle_view?.adapter = myAdapter
//        var inflate = LayoutInflater.from(this@CurrentOrderActivity).inflate(R.layout.layout_space_footer, simple_recycle_view, false)
//        myAdapter?.addFooterView(inflate)

        tv_save_order?.setOnClickListener {
            if (allPrice <= 0.0) {
                return@setOnClickListener
            }
            val mNetData = loadAll?.filter {
                it?.companyId?.equals(companyId) ?: false
            } ?: emptyList()
            val goods = StringBuffer()
            if (mNetData != null) {
                mNetData?.forEachIndexed { index, cacheGoodsBean ->
                    goods.append(cacheGoodsBean.id).append("#")
                }
            }
            var allOrder = getCacheOrders()?.filter {
                it.companyId?.equals(AppContext.get().companyId ?: "") ?: false
            }
            var isHasOrder = false
            var orderBean: CacheOrderBean? = null
            allOrder?.forEachIndexed { index, cacheOrderBean ->
                if (companyId.equals(cacheOrderBean.companyId)) {
                    isHasOrder = true
                    orderBean = cacheOrderBean
                }

            }
            if (isHasOrder && orderBean != null) {
                orderBean?.goods = goods?.toString() ?: ""
                orderDao?.update(orderBean)
                val bun = Bundle()
                bun?.putSerializable("companyId", companyId)
                UIHelper.startActivity(SaveOrderActivity::class.java, bun)
            } else if (!isHasOrder && orderBean == null) {
                orderBean = CacheOrderBean()
                orderBean?.companyId = companyId
                orderBean?.customerId = CacheCustomerId ?: ""
                orderBean?.bz = ""
                orderBean?.goods = goods?.toString() ?: ""
                orderDao?.insert(orderBean)
                val bun = Bundle()
                bun?.putSerializable("companyId", companyId)
                UIHelper.startActivity(SaveOrderActivity::class.java, bun)
            }

        }
    }

    override fun onResume() {
        super.onResume()
        showCompanyList(getCacheCompanyList() ?: arrayListOf())

    }

    override fun createPresenter(): NowOrderContract.NowOrderPresenter<NowOrderContract.NowOrderView> = NowOrderImpl()


    override fun requestPermission() {
    }

    override fun getLayoutId(): Int = R.layout.activity_current_order

    override fun <T> showContent(data: T?) {
        try {
            menu?.setDataPrice(getAllPrice())
            val mNetData = (data as? List<CacheGoodsBean>)?.filter {
                it?.companyId?.equals(companyId) ?: false
            } ?: emptyList()
            mData?.clear()
            if (mNetData != null) {
                mData?.addAll(mNetData)
                allPrice = getAllPrice()?.get(leftPosition)?.toDoubleOrNull() ?: 0.00
                tv_current_order_total?.setText("${resources.getString(R.string.str_total)}${formatNum(allPrice)}€")
//                if (allPrice > 100) {
//                    tv_save_order?.setText(resources.getString(R.string.str_save_order))
//                    tv_save_order?.isClickable = true
//                    tv_save_order?.isEnabled = true
//                    tv_save_order?.setBackgroundColor(Color.parseColor("#367BB9"))
//                    tv_save_order?.setTextColor(Color.WHITE)
//                } else {
//                    resources.getString(R.string.str_save_order)
//                    tv_save_order?.setText("${resources.getString(R.string.str_lack)}${formatNum(100.minus(allPrice))}")
//                    tv_save_order?.isClickable = false
//                    tv_save_order?.isEnabled = false
//                    tv_save_order?.setBackgroundColor(Color.parseColor("#777777"))
//                    tv_save_order?.setTextColor(Color.WHITE)
//                }

            } else {
                tv_current_order_total?.setText("${resources.getString(R.string.str_total)}${formatNum(allPrice)}€")
//                tv_save_order?.setText("${resources.getString(R.string.str_lack)}${formatNum(100.minus(allPrice))}")
//                tv_save_order?.isClickable = false
//                tv_save_order?.isEnabled = false
//                tv_save_order?.setBackgroundColor(Color.parseColor("#777777"))
//                tv_save_order?.setTextColor(Color.WHITE)
            }
            myAdapter?.setNewData(mData ?: arrayListOf())
            myAdapter?.notifyDataSetChanged()
            loadComplete(true)
        } catch (e: Exception) {
            KLog.e(e?.message)
        }

    }

    override fun onItemClick(view: View?, position: Int) {
        (mData?.get(position) as? CacheGoodsBean)?.let {
            val bun = Bundle()
            bun.putLong("id", it?.id)
            UIHelper.startActivity(ProductDetailActivity::class.java, bun)
        }
    }

    override fun onDeleteBtnCilck(view: View?, position: Int) {
        val deleteBean = mData?.get(position) as? CacheGoodsBean
        beanDao?.delete(deleteBean)
        loadAll = getCacheGoods()?.filter {
            it.companyId?.equals(AppContext.get().companyId ?: "") ?: false
        }
        showContent(loadAll)
    }

    override fun onSetBtnCilck(view: View?, position: Int) {
    }

    fun loadComplete(end: Boolean = false) {
        this.mEmptyLayout = EmptyLayout.getEmptyLayout(EmptyLayout.NODATA, simple_recycle_view)
        myAdapter?.setHeaderAndEmpty(myAdapter?.headerLayoutCount ?: 0 > 0)
        //  xr_toolbar_list.post {
        myAdapter?.loadMoreComplete()
        if (end) {
            myAdapter?.loadMoreEnd()
            if (myAdapter?.data?.size ?: 0 <= 0) {
                mEmptyLayout?.setErrorType(EmptyLayout.NODATA)
                myAdapter?.setEmptyView(mEmptyLayout)
            } else {
                val foot = EmptyLayout.getEmptyLayout(EmptyLayout.COMPLETE, xr_toolbar_list)
                myAdapter?.removeAllFooterView()
                myAdapter?.setFooterView(foot)
            }
        }
        //   }
    }

    override fun showError(e: Throwable?) {
        KLog.e(e?.message)
        this.mEmptyLayout = EmptyLayout.getEmptyLayout(EmptyLayout.NETWORK_ERROR, simple_recycle_view)
        myAdapter?.setHeaderAndEmpty(myAdapter?.headerLayoutCount ?: 0 > 0)
        myAdapter?.loadMoreComplete()
        myAdapter?.loadMoreEnd()
        myAdapter?.setEmptyView(mEmptyLayout)
        myAdapter?.setNewData(arrayListOf())
    }

    override fun finish() {
        if (requestCode == 300) {
            val intent = Intent(this@CurrentOrderActivity, ProductListActivity::class.java)
            setResult(500, intent)
        }else if (requestCode == 400){
            val intent = Intent(this@CurrentOrderActivity, ProductListActivity::class.java)
            setResult(500, intent)
        }
        super.finish()
    }
}
