package com.huanglejing.shanggou.activity

import android.os.Bundle
import android.util.Log
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.data.beanDao
import com.huanglejing.shanggou.greendao.CacheGoodsBean
import com.huanglejing.shanggou.greendao.CacheGoodsBeanDao
import com.huanglejing.shanggou.interfaces.ProductDetailContract.ProductDetailPresenter
import com.huanglejing.shanggou.interfaces.ProductDetailContract.ProductDetailView
import com.huanglejing.shanggou.presenterimpl.ProductDetailImpl
import com.huanglejing.shanggou.utils.formatNum
import kotlinx.android.synthetic.main.activity_product_detail.*

class ProductDetailActivity : AppBaseActivity<ProductDetailPresenter<ProductDetailView>, ProductDetailView>(), ProductDetailView {
    private var bean: CacheGoodsBean? = null
    private var boxNum = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var id = intent?.extras?.getLong("id") ?: 0L
        bean = beanDao?.queryBuilder()?.where(CacheGoodsBeanDao.Properties.Id.eq(id))?.build()?.unique()
        initWidget()
    }

    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_product_detail1))
        tv_detail_zhekou?.text="0%"
        tv_detail_tiaoma?.text = bean?.goodNO?.toString()
        tv_detail_number?.text = bean?.no
        tv_detail_number?.text = bean?.name
        tv_detail_name?.text = bean?.name
        tv_detail_bagcount?.text = "${bean?.bagCount} / ${bean?.boxCount}"
        tv_detail_bagCount?.text = bean?.num
        tv_detail_num?.text = bean?.num?.toIntOrNull()?.times(bean?.bagCount?.toIntOrNull()?:0)?.toString()?:"0"
        tv_detail_price?.text = "${formatNum(bean?.price)}€"
        tv_detail_total?.text = "${formatNum(bean?.totalPrice)}€"
        et_detail_content?.setText(bean?.bz)

        iv_box_minus?.setOnClickListener {
        //    Log.d("boxNum","boxNum count      ${bean?.bagCount}")
            boxNum = tv_detail_bagCount?.text?.toString()?.toIntOrNull() ?: 0
        //    Log.d("boxNum","boxNum      ${boxNum}")
            if (boxNum > 0) {
                boxNum = boxNum?.minus(1)
            }else{
                boxNum=0
            }
//            Log.d("boxNum","boxNum --     ${boxNum}")
//            Log.d("boxNum","boxNum --all     ${tv_detail_num?.text?.toString()?.toIntOrNull()?.minus(boxNum?.times(bean?.bagCount?.toIntOrNull() ?: 0))?.toString()?:"0"}")
            tv_detail_bagCount?.text = boxNum?.toString()
            Log.d("boxNum","boxNum   num00   ${boxNum?.times(bean?.bagCount?.toIntOrNull() ?: 0)?.toString()?:"0"}")
            tv_detail_num?.text = boxNum?.times(bean?.bagCount?.toIntOrNull() ?: 0)?.toString()?:"0"
            val price=bean?.price?.toDoubleOrNull()?:0.00
            tv_detail_total?.text = "${formatNum(price?.times(boxNum?.times(bean?.bagCount?.toIntOrNull() ?: 0)) ?: 0.00)}€"
        }

        iv_box_add?.setOnClickListener {
         //   Log.d("boxNum","boxNum count      ${bean?.bagCount}")
            boxNum = tv_detail_bagCount?.text?.toString()?.toIntOrNull()?.plus(1) ?: 0
//            Log.d("boxNum","boxNum      ${boxNum}")
//            Log.d("boxNum","boxNum ++     ${boxNum}")
//            Log.d("boxNum","boxNum ++all     ${tv_detail_num?.text?.toString()?.toIntOrNull()?.plus(boxNum?.times(bean?.bagCount?.toIntOrNull() ?: 0))?.toString()?:"0"}")
            tv_detail_bagCount?.text = boxNum?.toString()
            Log.d("boxNum","boxNum   num0   ${boxNum?.times(bean?.bagCount?.toIntOrNull() ?: 0)?.toString()?:"0"}")
            tv_detail_num?.text = boxNum?.times(bean?.bagCount?.toIntOrNull() ?: 0)?.toString()?:"0"
            val price=bean?.price?.toDoubleOrNull()?:0.00
            tv_detail_total?.text = "${formatNum(price?.times(boxNum?.times(bean?.bagCount?.toIntOrNull() ?: 0)) ?: 0.00)}€"

        }
    }

    override fun createPresenter(): ProductDetailPresenter<ProductDetailView> = ProductDetailImpl()

    override fun getLayoutId(): Int = R.layout.activity_product_detail

    override fun requestPermission() {
    }

    override fun finish() {
//        bean?.companyId = bean?.companyId
//        bean?.customerId = bean?.companyId
//        bean?.categoryGUID = bean?.categoryGUID
//        bean?.bagCount = bean?.bagCount
//        bean?.count = bean?.count
//        bean?.goodNO = bean?.goodNO
//        bean?.name = bean?.name
//        bean?.parentGuid = bean?.parentGuid
//        bean?.url = bean?.url
//        bean?.xname = bean?.xname
//        bean?.zk = bean?.zk
//        bean?.price = bean?.zk
//        bean?.no = tv_detail_number?.text?.toString()
//        bean?.bagCount = tv_detail_bagCount?.text?.toString()
        bean?.num = tv_detail_bagCount?.text?.toString()
        bean?.totalPrice = tv_detail_total?.text?.toString()?.replace("€","")
        bean?.bz = et_detail_content?.text?.toString()
        beanDao?.update(bean)
        super.finish()

    }
}
