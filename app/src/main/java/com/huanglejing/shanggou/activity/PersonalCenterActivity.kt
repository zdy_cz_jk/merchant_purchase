package com.huanglejing.shanggou.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.util.ArrayMap
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.data.CacheName
import com.huanglejing.shanggou.data.CacheNameNoZero
import com.huanglejing.shanggou.interfaces.PersonalCenterContract.PersonalCenterPresenter
import com.huanglejing.shanggou.interfaces.PersonalCenterContract.PersonalCenterView
import com.huanglejing.shanggou.presenterimpl.PersonCenterImpl
import com.huanglejing.shanggou.utils.AppManager
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.UIHelper
import com.huanglejing.shanggou.utils.cacheLoginInfo
import kotlinx.android.synthetic.main.activity_personal_center.*
import rx.Observable

class PersonalCenterActivity : AppBaseActivity<PersonalCenterPresenter<PersonalCenterView>, PersonalCenterView>(), PersonalCenterView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initWidget()
    }

    override fun getRequestParams(): Observable<Map<String, Any>> {
        val map = ArrayMap<String, String>()
        return Observable.just(map)
    }

    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_about_me))
        bt_logout?.setOnClickListener {
            mBasePresenter?.logout()
        }
        rl_update_container?.setOnClickListener {
            UIHelper.startActivity(ModifyPwdActivity::class.java)
        }

        tv_personal_center_name?.text = "+${CacheNameNoZero}"
        ll_personal_center_language?.setOnClickListener {
            //  AppManager.finishAllActivity()
            startActivity(Intent(this@PersonalCenterActivity, LanguageListActivity::class.java))
        }

    }

    override fun <T> showContent(data: T?) {
        cacheLoginInfo("", "")
        AppManager.finishAllActivity()
        UIHelper.startActivity(LoginActivity::class.java)

    }

    override fun createPresenter(): PersonalCenterPresenter<PersonalCenterView> = PersonCenterImpl()

    override fun getLayoutId(): Int = R.layout.activity_personal_center

    override fun requestPermission() {
    }
}
