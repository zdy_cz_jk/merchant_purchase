package com.huanglejing.shanggou.activity

import android.os.Bundle
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.data.CacheShowGoods
import com.huanglejing.shanggou.data.CacheShowStock
import com.huanglejing.shanggou.data.share
import com.huanglejing.shanggou.interfaces.SettingContract.SettingPresenter
import com.huanglejing.shanggou.interfaces.SettingContract.SettingView
import com.huanglejing.shanggou.presenterimpl.SettingImpl
import com.huanglejing.shanggou.utils.C
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppBaseActivity<SettingPresenter<SettingView>, SettingView>(), SettingView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        setAPPTitle(AppContext.get().resources.getString(R.string.str_setting))
       /* iv_login_exit?.setOnClickListener {
            finish()
        }*/
        sb_show_stock?.check = CacheShowStock
        sb_show_goods_no?.check = CacheShowGoods
        sb_show_stock?.setOnChangedListener { v, checkState ->
            CacheShowStock = checkState
            share?.edit()?.putBoolean(C.Preference.CACHE_STOCK, checkState ?: false)?.commit()
        }
        sb_show_goods_no?.setOnChangedListener { v, checkState ->
            CacheShowGoods = checkState
            share?.edit()?.putBoolean(C.Preference.CACHE_GOODS_NO, checkState ?: true)?.commit()
        }

    }

    override fun createPresenter(): SettingPresenter<SettingView> = SettingImpl()

    override fun getLayoutId(): Int = R.layout.activity_setting

    override fun requestPermission() {
    }
}
