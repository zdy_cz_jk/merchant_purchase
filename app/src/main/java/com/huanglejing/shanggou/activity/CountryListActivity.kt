package com.huanglejing.shanggou.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.listener.OnItemChildClickListener
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.bean.CountryBean
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.data.CountryList
import com.huanglejing.shanggou.interfaces.CountryListContract.CountryListPresenter
import com.huanglejing.shanggou.interfaces.CountryListContract.CountryListView
import com.huanglejing.shanggou.presenterimpl.CountryListImpl
import com.huanglejing.shanggou.utils.ToastUtils
import kotlinx.android.synthetic.main.activity_country_list.*

class CountryListActivity : AppBaseActivity<CountryListPresenter<CountryListView>, CountryListView>(), CountryListView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initWidget()
    }


    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_native))
      //  getmIvExit()?.visibility = View.GONE
        rv_country_list.layoutManager = LinearLayoutManager(this@CountryListActivity)

        rv_country_list.adapter = object : BaseQuickAdapter<CountryBean, BaseViewHolder>(R.layout.layout_country_list_item, CountryList) {
            override fun convert(helper: BaseViewHolder?, item: CountryBean?) {
                helper?.setText(R.id.tv_country_list_name, item?.name)
                        ?.setText(R.id.tv_country_list_code, "+${item?.code}")
            }
        }
        rv_country_list?.addOnItemTouchListener(object : OnItemClickListener() {
            override fun onSimpleItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
                val intent = Intent(this@CountryListActivity, LoginActivity::class.java)
                val bun = Bundle()
                bun?.putParcelable("bean", CountryList?.getOrNull(position))
                intent.putExtras(bun)
                setResult(200, intent)
                finish()
            }
        })
        //
    }

    override fun createPresenter(): CountryListPresenter<CountryListView> = CountryListImpl()

    override fun getLayoutId(): Int = R.layout.activity_country_list

    override fun requestPermission() {
    }


}