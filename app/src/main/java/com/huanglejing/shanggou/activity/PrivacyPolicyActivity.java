package com.huanglejing.shanggou.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.common.BaseActivity;
import com.huanglejing.shanggou.utils.AndroidUtil;


public class PrivacyPolicyActivity extends BaseActivity {
    private TextView mTvTitle;
    private WebView webView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        initView();
        initWebViewSetting();
        webView.loadUrl("http://www.dadisoft.cn/sgxy.html");
    }

    private void initView() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        ImageView mIvExit = (ImageView) findViewById(R.id.iv_login_exit);
        mIvExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTvTitle.setText(getResources().getString(R.string.str_license_protocol));
        webView = (WebView) findViewById(R.id.webview);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
    }

    private void initWebViewSetting() {
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.removeAllCookie();
        webView.setWebViewClient(null);
        webView.getSettings().setJavaScriptEnabled(false);
        webView.clearCache(true);

        WebSettings webSetting = webView.getSettings();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webSetting.setAllowFileAccessFromFileURLs(true);
            webSetting.setAllowUniversalAccessFromFileURLs(true);
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSetting.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webSetting.setMediaPlaybackRequiresUserGesture(false);
        }

        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            webSetting.setAllowContentAccess(true);
        }
        String ua = webSetting.getUserAgentString() + "_android";
        webSetting.setUserAgentString(ua);
        webSetting.setJavaScriptEnabled(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setDatabaseEnabled(true);
        webSetting.setJavaScriptCanOpenWindowsAutomatically(true);
        //  webSetting.setBlockNetworkImage(true);
        //  String dbPath = FileUtil.createFileInAppFiles(mContext,"database","").getAbsolutePath();
        //  webSetting.setDatabasePath(dbPath);
        webSetting.setUseWideViewPort(true);
        webSetting.setLoadWithOverviewMode(true);

        webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSetting.setAppCacheEnabled(true);
        //    webSetting.setAppCacheMaxSize(20 * 1025 * 1025);
        //    webSetting.setAppCachePath(dbPath);
        webSetting.setTextSize(WebSettings.TextSize.NORMAL);
        if (AndroidUtil.isNetworkAvailable(PrivacyPolicyActivity.this)) { //判断是否联网
            webSetting.setCacheMode(WebSettings.LOAD_DEFAULT); //默认的缓存使用模式
        } else {
            webSetting.setCacheMode(WebSettings.LOAD_CACHE_ONLY); //不从网络加载数据，只从缓存加载数据。
        }

        //  webView.addJavascriptInterface(new WebViewUtil(mContext),"OAModel");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (!TextUtils.isEmpty(title)) {
                    mTvTitle.setText(title);
                }
                // android 6.0 以下通过title获取
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    if (title.contains("404") || title.contains("500") || title.contains("Error")) {
                        view.loadUrl("about:blank");// 避免出现默认的错误界面
                        //view.loadUrl(mErrorUrl);
                    }
                }
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                // TODO 自动生成的方法存根

                if (newProgress == 100) {
                    progressBar.setVisibility(View.GONE);//加载完网页进度条消失
                } else {
                    progressBar.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                    progressBar.setProgress(newProgress);//设置进度值
                }

            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult
                    result) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PrivacyPolicyActivity.this);
                builder.setTitle("Alert");
                builder.setMessage(message);
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener
                        () {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        result.confirm();
                    }
                });
                builder.setCancelable(false);
                builder.create().show();
                return true;
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, final JsResult
                    result) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PrivacyPolicyActivity.this);
                builder.setTitle("Confirm");
                builder.setMessage(message);
                builder.setPositiveButton("确定", new DialogInterface
                        .OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        result.confirm();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        result.cancel();
                    }
                });
                builder.create().show();
                return true;
            }

            @Override
            public boolean onJsPrompt(WebView view, String url, String message, String
                    defaultValue, JsPromptResult result) {
                Log.i("wenwen", "on js prompt");
                return super.onJsPrompt(view, url, message, defaultValue, result);
            }
        });

        //  webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH);
        //   CookieManager cookieManager = CookieManager.getInstance();
        //   cookieManager.setAcceptCookie(true);

    }
}
