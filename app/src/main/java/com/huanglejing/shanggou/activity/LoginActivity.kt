package com.huanglejing.shanggou.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.util.ArrayMap
import android.view.View
import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.bean.CountryBean
import com.huanglejing.shanggou.bean.UpdateInfo
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.data.CountryList
import com.huanglejing.shanggou.interfaces.LoginContract.LoginPresenter
import com.huanglejing.shanggou.interfaces.LoginContract.LoginView
import com.huanglejing.shanggou.presenterimpl.LoginPresenterImpl
import com.huanglejing.shanggou.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import rx.Observable

class LoginActivity : AppBaseActivity<LoginPresenter<LoginView>, LoginView>(), LoginView {

    private var updateIndo: UpdateInfo? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        updateIndo = intent?.extras?.getSerializable(C.BundleKey.KEY1) as? UpdateInfo
        initWidget();
    }



    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_phone_login))
        getmIvExit()?.visibility = View.GONE
//        et_login_username?.setText("lifeng")
//        et_login_pwd?.setText("123456")
        tv_login_country_code?.setText("+${CountryList?.getOrNull(0)?.code}")
        tv_login_country_name?.setText(CountryList?.getOrNull(0)?.name)
        tv_login_forget_pwd.setOnClickListener {
            val bun=Bundle().apply {
                putString("type","forget")
            }
            UIHelper.startActivity(RegisterActivity::class.java,bun)
        }
        tv_login_to_register.setOnClickListener {
            val bun=Bundle().apply {
                putString("type","register")
            }
            UIHelper.startActivity(RegisterActivity::class.java)
        }
        tv_login_language.setOnClickListener {
            UIHelper.startActivity(LanguageListActivity::class.java)
        }
        ll_login_country.setOnClickListener {
            val intent = Intent(this@LoginActivity, CountryListActivity::class.java)
            startActivityForResult(intent, 100)
        }
        tv_agree?.setOnClickListener {
            startActivity(Intent(this@LoginActivity, PrivacyPolicyActivity::class.java))
        }
        bt_login.setOnClickListener {
            if (et_login_username.checkEmpty()) {

                ToastUtils.toastLong(resources.getString(R.string.str_usename_not_null))
                return@setOnClickListener
            }
            if (et_login_pwd.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_pwd_not_null))
                return@setOnClickListener
            }
            mBasePresenter?.login()
            //
        }

    }

    override fun getRequestParams(): Observable<Map<String, Any>> {
        var countryCode=""
        val code=tv_login_country_code?.text?.toString()?.replace("+","")?.trim()?:""
       if (code?.length==0){
           countryCode="0000"
       }else if (code?.length==1){
           countryCode="000${code}"
       }else if (code?.length==2){
           countryCode="00${code}"
       }else if (code?.length==3){
           countryCode="00${code}"
       }else{
           countryCode=code
       }
        val userName="${countryCode}${et_login_username?.text?.toString()}"

        val map = ArrayMap<String, String>().apply {
            put("customerName", userName)
            put("password", et_login_pwd?.text?.toString() ?: "")
        }
        cacheLoginParams("${code}${et_login_username?.text?.toString()?:""}",userName, et_login_pwd?.text?.toString()
                ?: "")
        return Observable.just(map)
    }

    override fun <T> showContent(data: T?) {
        if (data is JsonObject) {
            if (updateIndo != null) {
                val bun = Bundle(1).apply { putSerializable(C.BundleKey.KEY1, updateIndo) }
                UIHelper.startActivity(MainActivity::class.java,bun)
            }else{
                UIHelper.startActivity(MainActivity::class.java)
            }


            finish()
        }
    }

    override fun showError(e: Throwable?) {
        KLog.e(e?.message)
    }

    override fun createPresenter(): LoginPresenter<LoginView> = LoginPresenterImpl()

    override fun getLayoutId(): Int = R.layout.activity_login

    override fun requestPermission() {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == 200 && data != null) {
            tv_login_country_code?.setText("+${data?.getParcelableExtra<CountryBean>("bean")?.code
                    ?: ""}")
            tv_login_country_name?.setText(data?.getParcelableExtra<CountryBean>("bean")?.name
                    ?: "")
        }

    }

}
