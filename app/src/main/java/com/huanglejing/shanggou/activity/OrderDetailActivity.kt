package com.huanglejing.shanggou.activity

import android.os.Bundle
import android.support.v4.util.ArrayMap
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.bean.OrderDetailBean
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.data.CacheCustomerId
import com.huanglejing.shanggou.interfaces.OrderDetailContract.OrderDetailPresenter
import com.huanglejing.shanggou.interfaces.OrderDetailContract.OrderDetailView
import com.huanglejing.shanggou.presenterimpl.OrderDetailImpl
import com.huanglejing.shanggou.view.EmptyLayout
import kotlinx.android.synthetic.main.activity_order_detail.*
import kotlinx.android.synthetic.main.activity_recomend_pro.*
import rx.Observable

class OrderDetailActivity : AppBaseActivity<OrderDetailPresenter<OrderDetailView>, OrderDetailView>(), OrderDetailView {
    protected var mEmptyLayout: EmptyLayout? = null
    var companyId: String? = ""
    var orderGuid: String? = ""
    var mAdapter: BaseQuickAdapter<OrderDetailBean, BaseViewHolder>? = null
    var mData: ArrayList<OrderDetailBean>? = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        companyId = intent?.extras?.getString("companyId")
        orderGuid = intent?.extras?.getString("orderGuid")
        initWidget()
    }

    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_order_detail1))
        val layoutManager = LinearLayoutManager(this@OrderDetailActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_order_detail?.layoutManager = layoutManager
        mAdapter = object : BaseQuickAdapter<OrderDetailBean, BaseViewHolder>(R.layout.layout_order_detail_item, mData) {
            override fun convert(helper: BaseViewHolder?, item: OrderDetailBean?) {
                helper?.setText(R.id.tv_order_detail_type, item?.no)
                        ?.setText(R.id.tv_order_detail_name, item?.name)
                        ?.setText(R.id.tv_order_detail_num, "x${item?.count}")
                        ?.setText(R.id.tv_order_detail_price, item?.totalPrice)

                Glide.with(this@OrderDetailActivity).load(item?.url)
                        .placeholder(R.drawable.ic_img_error)
                        .error(R.drawable.ic_img_error)
                        .centerCrop()
                        .thumbnail(0.1f)
                        .into(helper?.getView<ImageView>(R.id.iv_order_detail_icon))
            }
        }
        rv_order_detail?.adapter = mAdapter
    }

    override fun getRequestParams(): Observable<Map<String, Any>> {
        val map = ArrayMap<String, String>().apply {
            put("customerId", CacheCustomerId ?: "")
            put("companyId", companyId)
            put("orderGuid", orderGuid)
        }
        return Observable.just(map)
    }
    override fun <T> showContent(data: T?) {
        try {
            val mData = data as? ArrayList<OrderDetailBean>
            if (mData != null) {
                mAdapter?.setNewData(mData ?: arrayListOf())
            }
            loadComplete(true)
        } catch (e: Exception) {
            KLog.e(e?.message)
        }

    }

    override fun showError(e: Throwable?) {
        KLog.e(e?.message)
        mAdapter?.setNewData(arrayListOf())
        mEmptyLayout=EmptyLayout.getEmptyLayout(EmptyLayout.NETWORK_ERROR, rv_order_detail)
        mEmptyLayout?.setOnLayoutClickListener {
            mBasePresenter?.getOrderDetail()
        }
        mAdapter?.setHeaderAndEmpty(mAdapter?.headerLayoutCount ?: 0 > 0)
        mAdapter?.loadMoreComplete()
        mAdapter?.loadMoreEnd()
        mAdapter?.setEmptyView(mEmptyLayout)
    }
    fun loadComplete(end: Boolean = false) {
        mEmptyLayout=EmptyLayout.getEmptyLayout(EmptyLayout.COMPLETE, rv_order_detail)
        mAdapter?.setHeaderAndEmpty(mAdapter?.headerLayoutCount ?: 0 > 0)
        mAdapter?.loadMoreComplete()
        if (end) {
            mAdapter?.loadMoreEnd()
            if (mAdapter?.data?.size ?: 0 <= 0) {
                mEmptyLayout?.setErrorType(EmptyLayout.NODATA)
                mAdapter?.setEmptyView(mEmptyLayout)
            } else {
                val foot = EmptyLayout.getEmptyLayout(EmptyLayout.COMPLETE, rv_order_detail)
                mAdapter?.removeAllFooterView()
                mAdapter?.setFooterView(foot)
            }
        }
    }
    override fun createPresenter(): OrderDetailPresenter<OrderDetailView> = OrderDetailImpl()

    override fun getLayoutId(): Int = R.layout.activity_order_detail

    override fun requestPermission() {
    }
}
