package com.huanglejing.shanggou.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.listener.OnItemChildClickListener
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.bean.LanguageBean
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.interfaces.LanguageListContract.LanguageListPresenter
import com.huanglejing.shanggou.interfaces.LanguageListContract.LanguageListView
import com.huanglejing.shanggou.presenterimpl.LanguageListImpl
import com.huanglejing.shanggou.utils.ToastUtils
import kotlinx.android.synthetic.main.activity_language_list.*
import com.umeng.socialize.utils.DeviceConfig.context
import java.util.*
import android.content.Intent
import android.util.DisplayMetrics
import android.widget.ImageView
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.data.Locale_Spanish
import com.huanglejing.shanggou.utils.C


class LanguageListActivity : AppBaseActivity<LanguageListPresenter<LanguageListView>, LanguageListView>(), LanguageListView {
    var mData: ArrayList<LanguageBean>? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        initWidget()
    }

    private fun initData() {
        mData = arrayListOf()

        mData?.add(LanguageBean(unicode2String("1F1E8") + unicode2String("1F1F3"), resources.getString(R.string.str_language_zh)))
        mData?.add(LanguageBean(unicode2String("1F1FA") + unicode2String("1F1F8"), resources.getString(R.string.str_language_en)))
        mData?.add(LanguageBean(unicode2String("1F1EA") + unicode2String("1F1F8"), resources.getString(R.string.str_language_es)))
        mData?.add(LanguageBean(unicode2String("1F1EE") + unicode2String("1F1F9"), resources.getString(R.string.str_language_it)))
    }

    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_language))
        val share = AppContext.get().getSharedPreferences(C.Preference.SAHNGOU_DATA, Context.MODE_PRIVATE)
        var language = share.getString(C.Preference.CACHE_LANGUAGE, resources.getString(R.string.str_language_zh))
        // getmIvExit()?.visibility = View.GONE
        rv_language_list.layoutManager = LinearLayoutManager(this@LanguageListActivity)

        rv_language_list.adapter = object : BaseQuickAdapter<LanguageBean, BaseViewHolder>(R.layout.layout_language_list_item, mData) {
            override fun convert(helper: BaseViewHolder?, item: LanguageBean?) {
                helper?.setText(R.id.tv_country_list_name, item?.name)
                        ?.setText(R.id.tv_country_list_icon, item?.img)
                if (item?.name?.equals(language)?:false){
                    helper?.getView<ImageView>(R.id.iv_is_select)?.visibility=View.VISIBLE
                }else{
                    helper?.getView<ImageView>(R.id.iv_is_select)?.visibility=View.GONE
                }



            } }
        rv_language_list?.addOnItemTouchListener(object : OnItemClickListener() {
            override fun onSimpleItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
                mData?.getOrNull(position)?.let {
                    val resources = resources// 获得res资源对象
                    val config = resources.configuration// 获得设置对象
                    val dm = resources.displayMetrics// 获得屏幕参数：主要是分辨率，像素等。
                    var language=resources.getString(R.string.str_language_zh)
                    when (it?.name) {
                        resources.getString(R.string.str_language_zh) -> {
                            language=resources.getString(R.string.str_language_zh)
                            config.locale = Locale.CHINA // 中文
                        }
                        resources.getString(R.string.str_language_en) -> {
                            language=resources.getString(R.string.str_language_en)
                            config.locale = Locale.ENGLISH // 英文
                        }
                        resources.getString(R.string.str_language_es) -> {
                            language=resources.getString(R.string.str_language_es)
                            config.locale = Locale_Spanish // 英文
                        }
                        resources.getString(R.string.str_language_it) -> {
                            language=resources.getString(R.string.str_language_it)
                            config.locale = Locale.ITALY // 意大利语
                        }

                    }
                    resources.updateConfiguration(config, dm)
                    val share = AppContext.get().getSharedPreferences(C.Preference.SAHNGOU_DATA, Context.MODE_PRIVATE)
                    val edit=share.edit()
                     edit.putString(C.Preference.CACHE_LANGUAGE,language)
                    edit.apply()
                    finish()//如果不重启当前界面，是不会立马修改的
                    startActivity(Intent(this@LanguageListActivity, SplashActivity::class.java))
                }
            }
        })
        //
    }

    override fun createPresenter(): LanguageListPresenter<LanguageListView> = LanguageListImpl()

    override fun getLayoutId(): Int = R.layout.activity_language_list

    override fun requestPermission() {
    }

    public fun unicode2String(unicode: String): String {
        var string = "";
        try {
            val num = Integer.parseInt(unicode, 16)
            string = String(Character.toChars(num))
        } catch (e: Exception) {
        }
        return string;
    }

}
