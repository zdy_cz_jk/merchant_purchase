package com.huanglejing.shanggou.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.util.ArrayMap
import com.afollestad.materialdialogs.GravityEnum
import com.afollestad.materialdialogs.MaterialDialog
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.bean.CountryBean
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.data.CountryList
import com.huanglejing.shanggou.interfaces.RegisterContract.RegisterPresenter
import com.huanglejing.shanggou.interfaces.RegisterContract.RegisterView
import com.huanglejing.shanggou.presenterimpl.RegisterPresenterImpl
import com.huanglejing.shanggou.utils.UIHelper
import com.huanglejing.shanggou.utils.checkEmpty
import kotlinx.android.synthetic.main.activity_register.*
import rx.Observable

class RegisterActivity : AppBaseActivity<RegisterPresenter<RegisterView>, RegisterView>(), RegisterView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initWidget()
    }

    private fun initWidget() {
        if ("forget".equals(intent?.extras?.getString("type"))){
            setAPPTitle(resources.getString(R.string.str_forget_pwd))
        }else{
            setAPPTitle(resources.getString(R.string.str_register))
        }

        tv_register_country_name?.text= CountryList?.getOrNull(0)?.name
        tv_register_country_code?.text= "+${CountryList?.getOrNull(0)?.code}"
        ll_register_country?.setOnClickListener {
            val intent = Intent(this@RegisterActivity, CountryListActivity::class.java)
            startActivityForResult(intent, 100)
        }
        bt_next_step?.setOnClickListener {

            if (et_register_username.checkEmpty()) {
                MaterialDialog.Builder(this@RegisterActivity)
                        .content(resources.getString(R.string.str_input_username))
                        .buttonsGravity(GravityEnum.CENTER)
                        .titleGravity(GravityEnum.CENTER)
                        .contentGravity(GravityEnum.CENTER)
                        .neutralText(resources.getString(R.string.str_confirm1))
                        .onNeutral { dialog, which ->
                            dialog.dismiss()
                        }
                        .neutralColor(Color.parseColor("#367bb9"))
                        .title(resources.getString(R.string.str_tips))
                        .build().show()
                return@setOnClickListener
            } else {
                var countryCode=""
                val code=tv_register_country_code?.text?.toString()?.replace("+","")?.trim()?:""
                if (code?.length==0){
                    countryCode="0000"
                }else if (code?.length==1){
                    countryCode="000${code}"
                }else if (code?.length==2){
                    countryCode="00${code}"
                }else if (code?.length==3){
                    countryCode="00${code}"
                }else{
                    countryCode=code
                }
                val userName="${countryCode}${et_register_username?.text?.toString()}"

                MaterialDialog.Builder(this@RegisterActivity)
                        .content("${resources.getString(R.string.str_send_code_to_phone)}：" + "\r\n"
                        + "${tv_register_country_code.text.toString()}  "
                        + et_register_username?.text?.toString() ?: "")
                        .title(resources.getString(R.string.str_confirm_phone_num))
                        .positiveText(resources.getString(R.string.str_confirm1))
                        .buttonsGravity(GravityEnum.CENTER)
                        .titleGravity(GravityEnum.CENTER)
                        .contentGravity(GravityEnum.CENTER)
                        .positiveColor(Color.parseColor("#367bb9"))
                        .negativeColor(Color.parseColor("#111111"))
                        .onPositive { dialog, which ->
                            val bun = Bundle().apply {
                                putString("phone", "${tv_register_country_code.text.toString()} ${et_register_username?.text?.toString()}")
                                putString("userName", userName)
                                if ("forget".equals(intent?.extras?.getString("type"))){
                                    putString("type", "forget")
                                }else{
                                    putString("type", "register")
                                }

                            }
                            UIHelper.startActivity(ConfirmValidateActivity::class.java,bun)
                            dialog.dismiss()
                            finish()
                        }.negativeText(resources.getString(R.string.str_cancle))
                        .onNegative { dialog, which ->
                            dialog.dismiss()
                        }
                        .build().show()
            }

        }
    }

    override fun getRequestParams(): Observable<Map<String, Any>> {
        var countryCode=""
        val code=tv_register_country_code?.text?.toString()?.replace("+","")?.trim()?:""
        if (code?.length==0){
            countryCode="0000"
        }else if (code?.length==1){
            countryCode="000${code}"
        }else if (code?.length==2){
            countryCode="00${code}"
        }else if (code?.length==3){
            countryCode="00${code}"
        }else{
            countryCode=code
        }
        val userName="${countryCode}${et_register_username?.text?.toString()}"
        val map = ArrayMap<String, String>().apply {
            put("username", userName)
        }
        return Observable.just(map)
    }


    override fun showError(e: Throwable?) {
        KLog.e(e?.message)
    }

    override fun createPresenter(): RegisterPresenter<RegisterView> = RegisterPresenterImpl()

    override fun getLayoutId(): Int = R.layout.activity_register
    override fun requestPermission() {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == 200 && data != null) {
            tv_register_country_code?.setText("+${data?.getParcelableExtra<CountryBean>("bean")?.code
                    ?: ""}")
            tv_register_country_name?.setText(data?.getParcelableExtra<CountryBean>("bean")?.name
                    ?: "")
        }

    }
}
