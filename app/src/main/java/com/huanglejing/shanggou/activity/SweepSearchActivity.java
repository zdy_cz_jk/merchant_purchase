package com.huanglejing.shanggou.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dtr.zxing.activity.CaptureActivity;
import com.google.zxing.Result;
import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.utils.ToastUtils;
import com.huanglejing.shanggou.utils.UIHelper;


public class SweepSearchActivity extends CaptureActivity {
    Context mContext;

    //TextView mTvInvalidCode;


    @Override
    protected int getLayoutId() {
        //去掉状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_sweep_login;
    }

    @Override
    protected void initView() {
        mContext = this;
        ((TextView) findViewById(R.id.tv_title)).setText(getResources().getString(R.string.str_scan));
        findViewById(R.id.iv_login_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
      //  mTvInvalidCode = (TextView) findViewById(R.id.tv_invalid_code);
        scanPreview = (SurfaceView) findViewById(R.id.capture_preview);
        scanContainer = (RelativeLayout) findViewById(R.id.capture_container);
        scanCropView = (RelativeLayout) findViewById(R.id.capture_crop_view);
        scanLine = (ImageView) findViewById(R.id.capture_scan_line);
    }



    @Override
    protected void initData() {

    }


    @Override
    public Handler getHandler() {
        return super.getHandler();
    }

    @Override
    public void handleDecode(Result rawResult, Bundle bundle) {
        super.handleDecode(rawResult, bundle);
        /**
         * 判断扫描的二维码返回的结果是否为空
         */
        if (rawResult == null || TextUtils.isEmpty(rawResult.getText())) {
//            mTvInvalidCode.setVisibility(View.VISIBLE);
//            mTvInvalidCode.setText(R.string.unrecognized_code);
            restartScan();
            return;
        } else {
          //  mTvInvalidCode.setVisibility(View.INVISIBLE);
            //扫描二维码获取的标示
            String scanResult = rawResult.getText();
          //  Intent intent = new Intent(SweepSearchActivity.this, SearchActivity.class);
            Bundle bun = new Bundle();
            bun.putString("searchWords",scanResult);
            UIHelper.startActivity(SearchActivity.class,bun);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
























