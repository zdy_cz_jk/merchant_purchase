package com.huanglejing.shanggou.activity

import android.os.Bundle
import android.support.v4.util.ArrayMap
import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.interfaces.SetPwdContract.SetPwdPresenter
import com.huanglejing.shanggou.interfaces.SetPwdContract.SetPwdView
import com.huanglejing.shanggou.presenterimpl.SetPwdImpl
import com.huanglejing.shanggou.utils.ToastUtils
import com.huanglejing.shanggou.utils.checkEmpty
import kotlinx.android.synthetic.main.activity_set_pwd.*
import rx.Observable

class SetPwdActivity : AppBaseActivity<SetPwdPresenter<SetPwdView>, SetPwdView>(), SetPwdView {
    private var username: String = ""
    private  var type=""
    private  var yzm=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        username = intent?.extras?.getString("username", "") ?: ""
        type = intent?.extras?.getString("type", "forget")?:"forget"
        yzm = intent?.extras?.getString("yzm", "")?:""
        initWidget()
    }

    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_sms))
        bt_next_step?.setOnClickListener {
            if (et_set_pwd.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_input_pwd))
                return@setOnClickListener
            }
            if (et_set_pwd_again.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_input_pwd_again))
                return@setOnClickListener
            }
            if (et_set_pwd?.text?.toString()?.equals(et_set_pwd_again?.text?.toString()
                            ?: "") == false) {
                ToastUtils.toastLong(resources.getString(R.string.str_input_pwd_not_same))
                return@setOnClickListener
            }
            if ("forget".equals(intent?.extras?.getString("type"))){
                mBasePresenter?.resetPwd()
            }else{
                mBasePresenter?.register()
            }

        }
    }

    override fun createPresenter(): SetPwdPresenter<SetPwdView> = SetPwdImpl()

    override fun getLayoutId(): Int = R.layout.activity_set_pwd
    override fun requestPermission() {
    }

    override fun getRequestParams(): Observable<Map<String, Any>> {
        val map = ArrayMap<String, String>().apply {
            put("username", username)
            put("password", et_set_pwd?.text?.toString() ?: "")
        }
        return Observable.just(map)
    }

    override fun getRegisterParams(): Observable<Map<String, Any>> {
        val map = ArrayMap<String, String>().apply {
            put("username", username)
            put("password", et_set_pwd?.text?.toString() ?: "")
            put("yzm", yzm ?: "")
        }
        return Observable.just(map)
    }

    override fun <T> showContent(data: T?) {
        if (data is JsonObject) {
            if (data?.get("status")?.asString?.equals("00") ?: false) {
                ToastUtils.toastLong(data?.get("message")?.asString)
                finish()
            } else {
                ToastUtils.toastLong(data?.get("message")?.asString)
            }
        }
    }

    override fun showError(e: Throwable?) {
        KLog.e(e?.message)
    }
}
