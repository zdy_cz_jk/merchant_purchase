package com.huanglejing.shanggou.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.util.ArrayMap
import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.data.CacheName
import com.huanglejing.shanggou.data.CacheNameNoZero
import com.huanglejing.shanggou.interfaces.ModifyPwdContract.ModifyPwdPresenter
import com.huanglejing.shanggou.interfaces.ModifyPwdContract.ModifyPwdView
import com.huanglejing.shanggou.presenterimpl.ModifyPwdImpl
import com.huanglejing.shanggou.utils.AppManager
import com.huanglejing.shanggou.utils.ToastUtils
import com.huanglejing.shanggou.utils.cacheLoginParams
import com.huanglejing.shanggou.utils.checkEmpty
import kotlinx.android.synthetic.main.activity_modify_pwd.*
import rx.Observable

class ModifyPwdActivity : AppBaseActivity<ModifyPwdPresenter<ModifyPwdView>, ModifyPwdView>(), ModifyPwdView {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initWidget()
    }

    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_change_pwd))
        bt_modify_commit?.setOnClickListener {
            if (et_modify_pwd_origin.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_origin_pwd_not_null))
                return@setOnClickListener
            }
            if (et_modify_pwd_new.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_new_pwd_not_null))
                return@setOnClickListener
            }
            if (et_modify_pwd_confirm.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_again_new_pwd))
                return@setOnClickListener
            }
            if (!(et_modify_pwd_confirm.text?.toString()?.equals(et_modify_pwd_new?.text?.toString()?:"")?:false)) {
                ToastUtils.toastLong(resources.getString(R.string.str_again_not_same))
                return@setOnClickListener
            }
            mBasePresenter?.updatePwd()
        }

    }

    override fun createPresenter(): ModifyPwdPresenter<ModifyPwdView> = ModifyPwdImpl()

    override fun getLayoutId(): Int = R.layout.activity_modify_pwd

    override fun requestPermission() {
    }

    override fun getRequestParams(): Observable<Map<String, Any>> {
        val map = ArrayMap<String, String>().apply {
            put("password", et_modify_pwd_origin?.text?.toString())
            put("newpassword", et_modify_pwd_new?.text?.toString())
            put("username", CacheName)
        }
        return Observable.just(map)
    }

    override fun <T> showContent(data: T?) {
        try {
            if (data is JsonObject) {
                if (data?.get("data")?.isJsonObject == true
                        && data?.get("status")?.asString?.equals("00") ?: false) {
                    cacheLoginParams(CacheNameNoZero,CacheName
                            ?: "", et_modify_pwd_origin?.text?.toString() ?: "")
                    ToastUtils.toastLong(resources.getString(R.string.str_update_pwd_success))
                    AppManager.finishAllActivity()
                    startActivity(Intent(this@ModifyPwdActivity, LoginActivity::class.java))
                } else {
                    ToastUtils.toastLong(resources.getString(R.string.str_update_pwd_failure))
                }
            }
        } catch (e: Exception) {
            KLog.e(e?.message)
        }

    }

    override fun showError(e: Throwable?) {
        ToastUtils.toastLong(resources.getString(R.string.str_update_pwd_failure))
        KLog.e(e?.message)
    }
}
