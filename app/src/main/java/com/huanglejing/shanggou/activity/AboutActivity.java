package com.huanglejing.shanggou.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.huanglejing.shanggou.R;
import com.huanglejing.shanggou.common.BaseActivity;
import com.huanglejing.shanggou.utils.PackageHelper;


public class AboutActivity extends BaseActivity {
    private String text = "    “商购”APP 是由西班牙大地网络科技有限公司开发的一款移动端叫货订购产品。   "+"\r\n" +
            "    西班牙大地网络科技有限公司主要经营零售批发业，信息化系统开发，整体解决方案提供，以及相关软件服务。";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ((TextView) findViewById(R.id.tv_title)).setText(getResources().getString(R.string.str_about_us));
        ((ImageView) findViewById(R.id.iv_login_exit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((TextView) findViewById(R.id.tv_about)).setText(text);
        ((TextView) findViewById(R.id.tv_version_name)).setText("V"+PackageHelper.getPackageInfo().versionName);
        findViewById(R.id.ll_privacy_policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutActivity.this, PrivacyPolicyActivity.class));
            }
        });
    }


}
