package com.huanglejing.shanggou.activity

import android.os.Bundle
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.interfaces.FeedBackContract.FeedBackPresenter
import com.huanglejing.shanggou.interfaces.FeedBackContract.FeedBackView
import com.huanglejing.shanggou.presenterimpl.FeedBackImpl

class FeedBackActivity : AppBaseActivity<FeedBackPresenter<FeedBackView>, FeedBackView>(), FeedBackView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initWidget()
    }

    override fun createPresenter(): FeedBackPresenter<FeedBackView> = FeedBackImpl()

    override fun getLayoutId(): Int = R.layout.activity_feed_back

    override fun requestPermission() {
    }

    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_feed_back))
    }
}
