package com.huanglejing.shanggou.activity

import android.os.Bundle
import android.support.v4.util.ArrayMap
import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.R
import com.huanglejing.shanggou.bean.AddressListBean
import com.huanglejing.shanggou.common.AppBaseActivity
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.data.CacheCustomerId
import com.huanglejing.shanggou.interfaces.AddAddressContract.AddAddressPresenter
import com.huanglejing.shanggou.interfaces.AddAddressContract.AddAddressView
import com.huanglejing.shanggou.presenterimpl.AddAddressImpl
import com.huanglejing.shanggou.utils.ToastUtils
import com.huanglejing.shanggou.utils.checkEmpty
import kotlinx.android.synthetic.main.activity_add_addresss.*
import rx.Observable

class AddAddresssActivity : AppBaseActivity<AddAddressPresenter<AddAddressView>, AddAddressView>(), AddAddressView {
    var mBean: AddressListBean? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initWidget()
    }

    override fun getRequestParams(): Observable<Map<String, Any>> {
        return Observable.just(ArrayMap<String, Any>().apply {
            put("city", mBean?.city)
            put("companyAddress", mBean?.companyAddress)
            put("companyName", mBean?.companyName)
            put("companySh", mBean?.companySh)
            put("country", mBean?.country)
            put("customerId", mBean?.customerId)
            if (!mBean?.id.checkEmpty()) {
                put("id", mBean?.id)
            }
            put("linkEmail", mBean?.linkEmail)
            put("linkMan", mBean?.linkMan)
            put("linkTelephone", mBean?.linkTelephone)
            put("postalCode", mBean?.postalCode)
            put("province", mBean?.province)
        })
    }

    private fun initWidget() {
        setAPPTitle(resources.getString(R.string.str_add_aaddress))
        if ((intent?.extras?.getSerializable("AddressListBean") as? AddressListBean) != null) { //不为空就是地址编辑修改 为空就是添加地址
            mBean = intent?.extras?.getSerializable("AddressListBean") as? AddressListBean
            et_addrss_company_name?.setText(mBean?.companyName)
            et_addrss_name?.setText(mBean?.linkMan)
            et_addrss_duty_paragraph?.setText(mBean?.companySh)
            et_addrss_address?.setText(mBean?.companyAddress)
            et_addrss_city?.setText(mBean?.city)
            et_addrss_province?.setText(mBean?.province)
            et_addrss_country?.setText(mBean?.country)
            et_addrss_code?.setText(mBean?.postalCode)
            et_addrss_phone?.setText(mBean?.linkTelephone)
            et_addrss_email?.setText(mBean?.linkEmail)
        } else {
            mBean = AddressListBean()
        }



        tv_save_address.setOnClickListener {
            mBean?.customerId = CacheCustomerId ?: ""
            if (et_addrss_company_name.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_company_name_not_null))
                return@setOnClickListener
            } else {
                mBean?.companyName = et_addrss_company_name?.text?.toString() ?: ""
            }
            if (et_addrss_name.checkEmpty()) {

                ToastUtils.toastLong(resources.getString(R.string.str_contact_not_null))
                return@setOnClickListener
            } else {
                mBean?.linkMan = et_addrss_name?.text?.toString() ?: ""
            }
            if (et_addrss_duty_paragraph.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_tax_number_not_null))
                return@setOnClickListener
            } else {
                mBean?.companySh = et_addrss_duty_paragraph?.text?.toString() ?: ""
            }
            if (et_addrss_address.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_address_not_null))
                return@setOnClickListener
            } else {
                mBean?.companyAddress = et_addrss_address?.text?.toString() ?: ""
            }
            if (et_addrss_city.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_city_not_null))
                return@setOnClickListener
            } else {
                mBean?.city = et_addrss_city?.text?.toString() ?: ""
            }
            if (et_addrss_province.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_province_not_null))
                return@setOnClickListener
            } else {
                mBean?.province = et_addrss_province?.text?.toString() ?: ""
            }
            if (et_addrss_country.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_country_not_null))
                return@setOnClickListener
            } else {
                mBean?.country = et_addrss_country?.text?.toString() ?: ""
            }
            if (et_addrss_code.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_zip_code_not_null))
                return@setOnClickListener
            } else {
                mBean?.postalCode = et_addrss_code?.text?.toString() ?: ""
            }
            if (et_addrss_phone.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_phone_num_not_null))
                return@setOnClickListener
            } else {
                mBean?.linkTelephone = et_addrss_phone?.text?.toString() ?: ""
            }
            if (et_addrss_email.checkEmpty()) {
                ToastUtils.toastLong(resources.getString(R.string.str_email_not_null))
                return@setOnClickListener
            } else {
                mBean?.linkEmail = et_addrss_email?.text?.toString() ?: ""
            }
            if ((intent?.extras?.getSerializable("AddressListBean") as? AddressListBean) != null) {
                mBasePresenter?.updateAddress()
            } else {
                mBasePresenter?.addAddress()
            }

        }

    }

    override fun <T> showContent(data: T?) {
        if (data is JsonObject) {
            if (data?.get("data")?.isJsonObject == true
                    && data?.get("status")?.asString?.equals("00") ?: false) {
                ToastUtils.toastLong(resources.getString(R.string.str_update_address_success))
                finish()
            } else if (
                    data?.get("data")?.asString?.equals("1") ?: false
                    && data?.get("status")?.asString?.equals("00") ?: false) {
                ToastUtils.toastLong(resources.getString(R.string.str_add_address_success))
                finish()
            }
        }

    }

    override fun showError(e: Throwable?) {
        KLog.e(e?.message)
    }

    override fun createPresenter(): AddAddressPresenter<AddAddressView> = AddAddressImpl()

    override fun getLayoutId(): Int = R.layout.activity_add_addresss

    override fun requestPermission() {
    }
}
