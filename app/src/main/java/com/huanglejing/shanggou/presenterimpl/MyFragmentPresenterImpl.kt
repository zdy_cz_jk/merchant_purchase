package com.huanglejing.shanggou.presenterimpl

import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.CompanyListBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.MyFragmentContract.MyFragmentPresenter
import com.huanglejing.shanggou.interfaces.MyFragmentContract.MyFragmentView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class MyFragmentPresenterImpl : BasePresenterImpl<MyFragmentView>(), MyFragmentPresenter<MyFragmentView> {

}