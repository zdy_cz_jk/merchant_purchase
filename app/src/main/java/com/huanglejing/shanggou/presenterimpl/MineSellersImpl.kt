package com.huanglejing.shanggou.presenterimpl

import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.CompanyListBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.MineSellersContract.MineSellersPresenter
import com.huanglejing.shanggou.interfaces.MineSellersContract.MineSellersView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class MineSellersImpl : BasePresenterImpl<MineSellersView>(), MineSellersPresenter<MineSellersView> {

    private val model by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.COMPANY_LIST) }
    private val deleteModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.DELETE_SHOP) }
    override fun onFirstLoad() {
        super.onFirstLoad()
    //    getCompanyList()
    }

    override fun getCompanyList() {
        mView?.getRequestParams()?.flatMap {
            model?.requestForGet(it)
        }?.map {
            KLog.d("BasePresenterImpl", "BasePresenterImpl    ${it.toString()}")
            AppContext.gson().fromJson<ArrayList<CompanyListBean>>(it?.get("data"))
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<ArrayList<CompanyListBean>>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: ArrayList<CompanyListBean>) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }

    override fun deleteSellers() {
        mView?.getDeleteParams()?.flatMap {
            deleteModel?.requestForGet(it)
        }?.doOnNext {
            KLog.d("MineSellersImpl", "MineSellersImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showDeleteSuccess(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}