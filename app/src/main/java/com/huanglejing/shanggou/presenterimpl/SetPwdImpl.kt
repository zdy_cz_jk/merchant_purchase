package com.huanglejing.shanggou.presenterimpl

import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.NewComeProBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.SetPwdContract.SetPwdPresenter
import com.huanglejing.shanggou.interfaces.SetPwdContract.SetPwdView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class SetPwdImpl : BasePresenterImpl<SetPwdView>(), SetPwdPresenter<SetPwdView> {
    private val model by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.RESET_PWD) }
    private val registerModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.REGISTER, BaseCacheRequestImpl.UrlStrategy.NEW) }

    override fun resetPwd() {
        mView?.getRequestParams()?.flatMap {
            model?.requestForGet(it)
        }?.doOnNext {
            KLog.d("SetPwdImpl", "SetPwdImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }

                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }

    override fun register() {
        mView?.getRegisterParams()?.flatMap {
            registerModel?.requestFor(it)
        }?.doOnNext {
            KLog.d("SetPwdImpl", "SetPwdImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }

                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}