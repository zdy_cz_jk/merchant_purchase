package com.huanglejing.shanggou.presenterimpl

import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.GoodsListBean
import com.huanglejing.shanggou.bean.NewComeProBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.data.getCacheGoodsList
import com.huanglejing.shanggou.interfaces.ProductListContract.ProductListPresenter
import com.huanglejing.shanggou.interfaces.ProductListContract.ProductListView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class ProductListImpl : BasePresenterImpl<ProductListView>(), ProductListPresenter<ProductListView> {
   // private val model by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.GOODS_CATEGORY) }
    private val productModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.GOODS_LIST) }

    override fun onFirstLoad() {
        super.onFirstLoad()
        mView?. showGoodsList(getCacheGoodsList() ?: arrayListOf())
     //   getGoodsCagory()
    }
//    override fun getGoodsCagory() {
//        mView?.getRequestParams()?.flatMap {
//            model?.requestForGet(it)
//        }?.map {
//            KLog.d("BasePresenterImpl", "BasePresenterImpl    ${it.toString()}")
//            AppContext.gson().fromJson<ArrayList<GoodsListBean>>(it?.get("data"))
//        }?.compose(SchedulersCompat.io<ArrayList<GoodsListBean>>())
//                ?.subscribe(object : SimpleSubscriber<ArrayList<GoodsListBean>>() {
//                    override fun onStart() {
//                        super.onStart()
//                     //   mView?.showLoading()
//                    }
//                    override fun onNext(t: ArrayList<GoodsListBean>) {
//                        super.onNext(t)
//                   //     mView?.dismissLoading()
//                        mView?.showGoodsList(t)
//                    }
//
//                    override fun onError(e: Throwable) {
//                        super.onError(e)
//                   //     mView?.dismissLoading()
//                        mView?.showError(e)
//                    }
//
//                })?.addTo(rxManager)
//    }

    override fun getProductList() {
        mView?.getProductListParams()?.flatMap {
            productModel?.requestForGet(it)
        }?.map {
            KLog.d("BasePresenterImpl", "BasePresenterImpl    ${it.toString()}")
            AppContext.gson().fromJson<ArrayList<NewComeProBean>>(it?.get("data"))
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<ArrayList<NewComeProBean>>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: ArrayList<NewComeProBean>) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}