package com.huanglejing.shanggou.presenterimpl

import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.interfaces.AddSellerContract.AddSellerPresenter
import com.huanglejing.shanggou.interfaces.AddSellerContract.AddSellerView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo


/**
 * Created by Administrator on 2017/7/17.
 */
class AddSellersImpl : BasePresenterImpl<AddSellerView>(), AddSellerPresenter<AddSellerView> {
    private val addmodel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.BIND_CUSTOMER) }
    override fun addSeller() {
        mView?.getRequestParams()?.flatMap {
            addmodel?.requestForGet(it)
        }?.doOnNext {
            KLog.d("AddSellersImpl", "AddSellersImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}