package com.huanglejing.shanggou.presenterimpl


import com.huanglejing.shanggou.interfaces.RegisterContract.RegisterPresenter
import com.huanglejing.shanggou.interfaces.RegisterContract.RegisterView

/**
 * Created by Administrator on 2016/8/16 0016.
 * 负责地址CityEvent的工作,例如请求定位,地址接收并存储.
 */
class RegisterPresenterImpl : BasePresenterImpl<RegisterView>(), RegisterPresenter<RegisterView> {

}
