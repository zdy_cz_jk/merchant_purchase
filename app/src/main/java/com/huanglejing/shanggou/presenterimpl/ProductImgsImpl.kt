package com.huanglejing.shanggou.presenterimpl


import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.GoodsListBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.ProductImgsContract.ProductImgsPresenter
import com.huanglejing.shanggou.interfaces.ProductImgsContract.ProductImgsView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


class ProductImgsImpl : BasePresenterImpl<ProductImgsView>(), ProductImgsPresenter<ProductImgsView> {

    private val model by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.GOODS_CATEGORY) }
    override fun onFirstLoad() {
        super.onFirstLoad()
        getNetData()
    }

    override fun getNetData() {
        mView?.getRequestParams()?.flatMap {
            model?.requestForGet(it)
        }?.map {
            KLog.d("BasePresenterImpl", "BasePresenterImpl    ${it.toString()}")
            AppContext.gson().fromJson<ArrayList<GoodsListBean>>(it?.get("data"))
        }?.compose(SchedulersCompat.io<ArrayList<GoodsListBean>>())
                ?.subscribe(object : SimpleSubscriber<ArrayList<GoodsListBean>>() {
                    override fun onStart() {
                        super.onStart()
                        if (mView?.isShowLoading ?: false) {
                            mView?.showLoading()
                        }
                    }

                    override fun onNext(t: ArrayList<GoodsListBean>) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)

    }
}
