package com.huanglejing.shanggou.presenterimpl


import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.interfaces.ConfirmValiContract.ConfirmValiPresenter
import com.huanglejing.shanggou.interfaces.ConfirmValiContract.ConfirmValiView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo

/**
 * Created by Administrator on 2016/8/16 0016.
 * 负责地址CityEvent的工作,例如请求定位,地址接收并存储.
 */
class ConfirmValidImpl : BasePresenterImpl<ConfirmValiView>(), ConfirmValiPresenter<ConfirmValiView> {
     private val confirmModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.VALILD_YZM, BaseCacheRequestImpl.UrlStrategy.NEW) }

    override fun confirmCode() {
        mView?.getRequestParams()?.flatMap {
            confirmModel?.requestFor(it)
        }?.doOnNext {
            KLog.d("ConfirmValiPresenterImpl", "ConfirmValiPresenterImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }

                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }

}
