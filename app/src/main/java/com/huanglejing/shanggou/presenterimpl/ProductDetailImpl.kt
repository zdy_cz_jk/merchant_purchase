package com.huanglejing.shanggou.presenterimpl

import com.huanglejing.shanggou.interfaces.ProductDetailContract.ProductDetailPresenter
import com.huanglejing.shanggou.interfaces.ProductDetailContract.ProductDetailView


/**
 * Created by Administrator on 2017/7/17.
 */
class ProductDetailImpl : BasePresenterImpl<ProductDetailView>(), ProductDetailPresenter<ProductDetailView> {
}