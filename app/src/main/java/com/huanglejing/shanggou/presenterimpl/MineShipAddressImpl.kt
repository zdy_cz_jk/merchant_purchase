package com.huanglejing.shanggou.presenterimpl

import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.AddressListBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.MineShipAddressContract.MineShipAddressPresenter
import com.huanglejing.shanggou.interfaces.MineShipAddressContract.MineShipAddressView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class MineShipAddressImpl : BasePresenterImpl<MineShipAddressView>(), MineShipAddressPresenter<MineShipAddressView> {
    private val listModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.ADDRESS_LIST) }
    private val deleteModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.DELETE_ADDRESS) }
    override fun onFirstLoad() {
        super.onFirstLoad()
        getAddressList()
    }
    override fun getAddressList() {
        mView?.getRequestParams()?.flatMap {
            listModel?.requestForGet(it)
        }?.map {
            KLog.d("LoginPresenterImpl", "LoginPresenterImpl    ${it.toString()}")
            AppContext.gson().fromJson<ArrayList<AddressListBean>>(it?.get("data"))
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<ArrayList<AddressListBean>>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: ArrayList<AddressListBean>) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }

    override fun deleteAddress() {
        mView?.getDeleteParams()?.flatMap {
            deleteModel?.requestForGet(it)
        }?.doOnNext {
            KLog.d("LoginPresenterImpl", "LoginPresenterImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.showDeleteSuccess(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}