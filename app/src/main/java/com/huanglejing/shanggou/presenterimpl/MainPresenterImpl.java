package com.huanglejing.shanggou.presenterimpl;


import com.huanglejing.shanggou.interfaces.MainContract.*;

/**
 * Created by Administrator on 2016/8/16 0016.
 * 负责地址CityEvent的工作,例如请求定位,地址接收并存储.
 */
public class MainPresenterImpl<T> extends BasePresenterImpl<MainView> implements MainPresenter<MainView> {

    @Override
    public void onFirstLoad() {
        super.onFirstLoad();
        //获取配置信息
        //initAccount();
    }
}
