package com.huanglejing.shanggou.presenterimpl


import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.common.BasePresenter
import com.huanglejing.shanggou.common.BaseView

import com.huanglejing.shanggou.rx.RxManager

import kotlin.properties.Delegates

/**
 * Created by Administrator on 2016/11/25 0025.
 */
open class BasePresenterImpl<V : BaseView> : BasePresenter<V> {

    @JvmField protected var mView: V? = null
    protected var isFirstStart: Boolean by Delegates.notNull()
    protected val rxManager by lazy { RxManager() }
    protected val TAG: String by lazy { this.javaClass.simpleName }


    override fun onViewAttached(view: V) {
        KLog.e(TAG, "$this:onViewAttached")
        mView = view
        assert(mView != null)
    }


    override fun onStart(firstStart: Boolean) {
        isFirstStart = firstStart
        KLog.e(TAG, "onStart", firstStart)
        if (firstStart && this !is BasePresenter.LazyLoaderPresenter) {
            onFirstLoad()
        }
    }


    //可以初始化加载数据
    override fun onFirstLoad() {
        KLog.e(TAG, "onFirstLoad")
    }

    override fun onResume() {
        KLog.e(TAG, "onResume")
    }

    override fun onPause() {
        KLog.e(TAG, "onPause")
    }

    override fun onStop() {
        KLog.e(TAG, "onStop")
    }


    override fun onViewDetached() {
        KLog.e(TAG, "onViewDetached")
        rxManager.clear()
        try {
            mView?.dismissLoading()
            mView = null
        } catch(e: Exception) {
        }

    }

    override fun onPresenterDestroyed() {
        KLog.e(TAG, "$this:onPresenterDestroyed")
        rxManager.clear()
        rxManager.destory()
    }

}