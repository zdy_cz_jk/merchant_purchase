package com.huanglejing.shanggou.presenterimpl

import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.CompanyListBean
import com.huanglejing.shanggou.bean.NowOrderBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.NowOrderContract.NowOrderPresenter
import com.huanglejing.shanggou.interfaces.NowOrderContract.NowOrderView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class NowOrderImpl : BasePresenterImpl<NowOrderView>(), NowOrderPresenter<NowOrderView> {
   // private val model by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.COMPANY_LIST) }
    override fun onFirstLoad() {
        super.onFirstLoad()
      //  getCompanyList()
    }
//    override fun getCompanyList() {
//        mView?.getCompanyListParams()?.flatMap {
//            model?.requestForGet(it)
//        }?.map {
//            KLog.d("BasePresenterImpl", "BasePresenterImpl    ${it.toString()}")
//            AppContext.gson().fromJson<ArrayList<CompanyListBean>>(it?.get("data"))
//        }?.compose(SchedulersCompat.io())
//                ?.subscribe(object : SimpleSubscriber<ArrayList<CompanyListBean>>() {
//                    override fun onStart() {
//                        super.onStart()
//                        mView?.showLoading()
//                    }
//                    override fun onNext(t: ArrayList<CompanyListBean>) {
//                        super.onNext(t)
//                        mView?.dismissLoading()
//                        mView?.showCompanyList(t)
//                    }
//
//                    override fun onError(e: Throwable) {
//                        super.onError(e)
//                        mView?.dismissLoading()
//                        mView?.showError(e)
//                    }
//
//                })?.addTo(rxManager)
//    }


//    override fun getNowOrder() {
//        mView?.getRequestParams()?.flatMap {
//            orderModel?.requestForGet(it)
//        }?.map {
//            KLog.d("BasePresenterImpl", "BasePresenterImpl    ${it.toString()}")
//            AppContext.gson().fromJson<ArrayList<NowOrderBean>>(it?.get("data"))
//        }?.compose(SchedulersCompat.io())
//                ?.subscribe(object : SimpleSubscriber<ArrayList<NowOrderBean>>() {
//                    override fun onNext(t: ArrayList<NowOrderBean>) {
//                        super.onNext(t)
//                        mView?.showContent(t)
//                    }
//
//                    override fun onError(e: Throwable) {
//                        super.onError(e)
//                        mView?.showError(e)
//                    }
//
//                })?.addTo(rxManager)
//    }
}