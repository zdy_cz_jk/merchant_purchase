package com.huanglejing.shanggou.presenterimpl

import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.NewComeProBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.ModifyPwdContract.ModifyPwdPresenter
import com.huanglejing.shanggou.interfaces.ModifyPwdContract.ModifyPwdView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class ModifyPwdImpl : BasePresenterImpl<ModifyPwdView>(), ModifyPwdPresenter<ModifyPwdView> {
    private val updatwModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.UPDATE_PWD) }

    override fun onFirstLoad() {
        super.onFirstLoad()
    }

    override fun updatePwd() {
        mView?.getRequestParams()?.flatMap {
            updatwModel?.requestForGet(it)
        }?.doOnNext {
            KLog.d("ModifyPwdImpl", "ModifyPwdImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io<JsonObject>())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}