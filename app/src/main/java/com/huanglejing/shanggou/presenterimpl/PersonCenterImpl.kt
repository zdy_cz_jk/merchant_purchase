package com.huanglejing.shanggou.presenterimpl


import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.interfaces.PersonalCenterContract.PersonalCenterPresenter
import com.huanglejing.shanggou.interfaces.PersonalCenterContract.PersonalCenterView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo


class PersonCenterImpl : BasePresenterImpl<PersonalCenterView>(), PersonalCenterPresenter<PersonalCenterView> {
    private val logoutModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.LOGOUT) }
    override fun logout() {
        mView?.getRequestParams()?.flatMap {
            logoutModel?.requestFor(it)
        }?.doOnNext {
            KLog.d("PersonalPresenterImpl", "PersonalPresenterImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.dismissLoading()
                       mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}
