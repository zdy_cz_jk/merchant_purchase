package com.huanglejing.shanggou.presenterimpl

import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.OrderDetailBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.OrderDetailContract.OrderDetailPresenter
import com.huanglejing.shanggou.interfaces.OrderDetailContract.OrderDetailView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class OrderDetailImpl : BasePresenterImpl<OrderDetailView>(), OrderDetailPresenter<OrderDetailView> {
    private val model by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.ORDER_GOODS) }

    override fun onFirstLoad() {
        super.onFirstLoad()
        getOrderDetail()
    }
    override fun getOrderDetail() {
        mView?.getRequestParams()?.flatMap {
            model?.requestForGet(it)
        }?.map {
            KLog.d("BasePresenterImpl", "BasePresenterImpl    ${it.toString()}")
            AppContext.gson().fromJson<ArrayList<OrderDetailBean>>(it?.get("data"))
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<ArrayList<OrderDetailBean>>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: ArrayList<OrderDetailBean>) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}