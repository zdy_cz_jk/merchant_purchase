package com.huanglejing.shanggou.presenterimpl

import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.NowOrderBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.MineOrderContract.*
import com.huanglejing.shanggou.presenterimpl.BasePresenterImpl
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class MineOrderImpl : BasePresenterImpl<MineOrderView>(), MineOrderPresenter<MineOrderView> {
    private val orderModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.ORDER_ALL) }
    override fun onFirstLoad() {
        super.onFirstLoad()
       // getNowOrder()
    }
    override fun getNowOrder() {
        mView?.getRequestParams()?.flatMap {
            orderModel?.requestForGet(it)
        }?.map {
            KLog.d("BasePresenterImpl", "BasePresenterImpl    ${it.toString()}")
            AppContext.gson().fromJson<ArrayList<NowOrderBean>>(it?.get("data"))
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<ArrayList<NowOrderBean>>() {
                    override fun onStart() {
                        super.onStart()
                        if (mView?.isShowLoading?:false){
                            mView?.showLoading()
                        }
                    }
                    override fun onNext(t: ArrayList<NowOrderBean>) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}