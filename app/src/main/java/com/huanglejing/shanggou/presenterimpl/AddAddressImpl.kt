package com.huanglejing.shanggou.presenterimpl

import com.google.gson.JsonObject
import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.AddressListBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.AddAddressContract.AddAddressPresenter
import com.huanglejing.shanggou.interfaces.AddAddressContract.AddAddressView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson


/**
 * Created by Administrator on 2017/7/17.
 */
class AddAddressImpl : BasePresenterImpl<AddAddressView>(), AddAddressPresenter<AddAddressView> {
    private val addmodel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.ADD_ADDRESS) }
    private val updateModel by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.UPDATE_ADDRESS) }

    override fun addAddress() {
        mView?.getRequestParams()?.flatMap {
            addmodel?.requestFor(it)
        }?.doOnNext {
            KLog.d("LoginPresenterImpl", "LoginPresenterImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }

    override fun updateAddress() {
        mView?.getRequestParams()?.flatMap {
            updateModel?.requestFor(it)
        }?.doOnNext {
            KLog.d("LoginPresenterImpl", "LoginPresenterImpl    ${it.toString()}")
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<JsonObject>() {
                    override fun onNext(t: JsonObject) {
                        super.onNext(t)
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }
}