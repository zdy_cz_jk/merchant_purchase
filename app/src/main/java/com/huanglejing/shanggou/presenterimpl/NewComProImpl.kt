package com.huanglejing.shanggou.presenterimpl

import com.huanglejing.mylibrary.KLog
import com.huanglejing.shanggou.bean.NewComeProBean
import com.huanglejing.shanggou.common.AppContext
import com.huanglejing.shanggou.interfaces.NewComeProContract.NewComeProPresenter
import com.huanglejing.shanggou.interfaces.NewComeProContract.NewComeProView
import com.huanglejing.shanggou.rx.SchedulersCompat
import com.huanglejing.shanggou.rx.SimpleSubscriber
import com.huanglejing.shanggou.utils.C
import com.huanglejing.shanggou.utils.addTo
import com.huanglejing.shanggou.utils.fromJson

/**
 * Created by Administrator on 2017/7/17.
 */
class NewComProImpl : BasePresenterImpl<NewComeProView>(), NewComeProPresenter<NewComeProView> {
    private val model by lazy { BaseCacheRequestImpl.getInstance(C.APIV1.GOODS_NEW) }
    
    override fun onFirstLoad() {
        super.onFirstLoad()
        getNewComProList()
    }

    override fun getNewComProList() {
        mView?.getRequestParams()?.flatMap {
            model?.requestForGet(it)
        }?.map {
            KLog.d("BasePresenterImpl", "BasePresenterImpl    ${it.toString()}")
            AppContext.gson().fromJson<ArrayList<NewComeProBean>>(it?.get("data"))
        }?.compose(SchedulersCompat.io())
                ?.subscribe(object : SimpleSubscriber<ArrayList<NewComeProBean>>() {
                    override fun onStart() {
                        super.onStart()
                        mView?.showLoading()
                    }
                    override fun onNext(t: ArrayList<NewComeProBean>) {
                        super.onNext(t)
                        mView?.dismissLoading()
                        mView?.showContent(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        mView?.dismissLoading()
                        mView?.showError(e)
                    }

                })?.addTo(rxManager)
    }

}